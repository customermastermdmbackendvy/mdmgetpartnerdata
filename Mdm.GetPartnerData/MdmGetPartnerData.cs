using Amazon.Lambda.Core;
using Mdm.Infrastructure;
using Mdm.Services.Helpers;
using Mdm.Services.Interfaces;
using Mdm.Services.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Mdm.GetPartnerData
{
    public class MdmGetPartnerData
    {
        public MdmGetPartnerData()
        {
            LambdaLogger.Log("Initiating GetPartnerData Lambda.");
            ApplicationSetup.Instance.BuildMappers().SetupDependencies().BuildStaticData();
        }

        public async Task<OperationResult<List<PartnerDataResult>>> GetPartnerData(GetPartnerDataRequest getPartnerDataRequest, ILambdaContext context)
        {
            LambdaLogger.Log("Initiating GetPartnerData operation.");
            var partnerDataService = ApplicationSetup.Instance.ServiceProvider.GetService<IPartnerDataService>();
            try
            {
                if (!string.IsNullOrEmpty(getPartnerDataRequest.UserId) && getPartnerDataRequest.UserId.ToLower() == "test.user")
                {
                    await partnerDataService.RunColdStart();
                    return null;
                }
                return await partnerDataService.GetPartnerData(getPartnerDataRequest);
            }
            catch (Exception ex)
            {
                partnerDataService.ProcessOperationError("Error with  GetPartnerData : " + ex.Message, "GetPartnerData");
            }
            return partnerDataService.OperationResult;
        }

        public async Task<OperationResult<SapPartnersResponse>> GetApolloPartners(SapPartnersRequest sapPartnersRequest, ILambdaContext context)
        {
            LambdaLogger.Log("Initiating GetApolloPartners operation.");
            var partnerDataService = ApplicationSetup.Instance.ServiceProvider.GetService<IPartnerDataService>();
            try
            {
                if (!string.IsNullOrEmpty(sapPartnersRequest.UserId) && sapPartnersRequest.UserId.ToLower() == "test.user")
                {
                    await partnerDataService.RunColdStart();
                    return null;
                }
                return await partnerDataService.GetSapPartners(SystemType.APOLLO, "GetApolloPartners", sapPartnersRequest);
            }
            catch (Exception ex)
            {
                partnerDataService.ProcessOperationError("Error with  GetApolloPartners : " + ex.Message, "GetApolloPartners");
            }
            return partnerDataService.SapPartnersOperationResult;
        }

        public async Task<OperationResult<SapPartnersResponse>> GetOlympusPartners(SapPartnersRequest sapPartnersRequest, ILambdaContext context)
        {
            LambdaLogger.Log("Initiating GetOlympusPartners operation.");
            var partnerDataService = ApplicationSetup.Instance.ServiceProvider.GetService<IPartnerDataService>();
            try
            {
                if (!string.IsNullOrEmpty(sapPartnersRequest.UserId) && sapPartnersRequest.UserId.ToLower() == "test.user")
                {
                    await partnerDataService.RunColdStart();
                    return null;
                }
                return await partnerDataService.GetSapPartners(SystemType.OLYMPUS, "GetOlympusPartners", sapPartnersRequest);
            }
            catch (Exception ex)
            {
                partnerDataService.ProcessOperationError("Error with  GetOlympusPartners : " + ex.Message, "GetOlympusPartners");
            }
            return partnerDataService.SapPartnersOperationResult;
        }

        public async Task<OperationResult<SapPartnersResponse>> GetJDEPartners(SapPartnersRequest sapPartnersRequest, ILambdaContext context)
        {
            LambdaLogger.Log("Initiating GetJDEPartners operation.");
            var partnerDataService = ApplicationSetup.Instance.ServiceProvider.GetService<IPartnerDataService>();
            try
            {
                if (!string.IsNullOrEmpty(sapPartnersRequest.UserId) && sapPartnersRequest.UserId.ToLower() == "test.user")
                {
                    await partnerDataService.RunColdStart();
                    return null;
                }
                return await partnerDataService.GetSapPartners(SystemType.JDE, "GetJDEPartners", sapPartnersRequest);
            }
            catch (Exception ex)
            {
                partnerDataService.ProcessOperationError("Error with  GetJDEPartners : " + ex.Message, "GetJDEPartners");
            }
            return partnerDataService.SapPartnersOperationResult;
        }

        public async Task<OperationResult<SearchSapPartnerResponse>> SearchApolloPartner(SapPartnerRequest searchSapPartnerRequest, ILambdaContext context)
        {
            LambdaLogger.Log("Initiating SearchApolloPartner operation.");
            var partnerDataService = ApplicationSetup.Instance.ServiceProvider.GetService<IPartnerDataService>();
            try
            {
                if (!string.IsNullOrEmpty(searchSapPartnerRequest.UserId) && searchSapPartnerRequest.UserId.ToLower() == "test.user")
                {
                    await partnerDataService.RunColdStart();
                    return null;
                }
                return await partnerDataService.SearchSapPartner(SystemType.APOLLO, "SearchApolloPartner", searchSapPartnerRequest);
            }
            catch (Exception ex)
            {
                partnerDataService.ProcessSearchSapPartnerOperationError("Error with  SearchApolloPartner : " + ex.Message, "SearchApolloPartner");
            }
            return partnerDataService.SearchSapPartnerOperationResult;
        }

        public async Task<OperationResult<SearchSapPartnerResponse>> SearchOlympusPartner(SapPartnerRequest searchSapPartnerRequest, ILambdaContext context)
        {
            LambdaLogger.Log("Initiating SearchOlympusPartner operation.");
            var partnerDataService = ApplicationSetup.Instance.ServiceProvider.GetService<IPartnerDataService>();
            try
            {
                if (!string.IsNullOrEmpty(searchSapPartnerRequest.UserId) && searchSapPartnerRequest.UserId.ToLower() == "test.user")
                {
                    await partnerDataService.RunColdStart();
                    return null;
                }
                return await partnerDataService.SearchSapPartner(SystemType.OLYMPUS, "SearchOlympusPartner", searchSapPartnerRequest);
            }
            catch (Exception ex)
            {
                partnerDataService.ProcessSearchSapPartnerOperationError("Error with  SearchOlympusPartner : " + ex.Message, "SearchOlympusPartner");
            }
            return partnerDataService.SearchSapPartnerOperationResult;
        }

        public async Task<OperationResult<SearchSapPartnerResponse>> SearchJDEPartner(SapPartnerRequest searchSapPartnerRequest, ILambdaContext context)
        {
            LambdaLogger.Log("Initiating SearchJDEPartner operation.");
            var partnerDataService = ApplicationSetup.Instance.ServiceProvider.GetService<IPartnerDataService>();
            try
            {
                if (!string.IsNullOrEmpty(searchSapPartnerRequest.UserId) && searchSapPartnerRequest.UserId.ToLower() == "test.user")
                {
                    await partnerDataService.RunColdStart();
                    return null;
                }
                return await partnerDataService.SearchSapPartner(SystemType.JDE, "SearchJDEPartner", searchSapPartnerRequest);
            }
            catch (Exception ex)
            {
                partnerDataService.ProcessSearchSapPartnerOperationError("Error with  SearchJDEPartner : " + ex.Message, "SearchJDEPartner");
            }
            return partnerDataService.SearchSapPartnerOperationResult;
        }

        public async Task<OperationResult<SapPartnerWorkflowDataResponse>> GetApolloPartnerWorkflowData(SearchSapPartnerWorkflowRequest apolloPartnerWorkflowRequest, ILambdaContext context)
        {
            LambdaLogger.Log("Initiating GetApolloPartnerWorkflowData operation.");
            var partnerDataService = ApplicationSetup.Instance.ServiceProvider.GetService<IPartnerDataService>();
            try
            {
                if (!string.IsNullOrEmpty(apolloPartnerWorkflowRequest.UserId) && apolloPartnerWorkflowRequest.UserId.ToLower() == "test.user")
                {
                    await partnerDataService.RunColdStart();
                    return null;
                }
                return await partnerDataService.GetSapPartnerWorkflowData(SystemType.APOLLO, "GetApolloPartnerWorkflowData", apolloPartnerWorkflowRequest);
            }
            catch (Exception ex)
            {
                partnerDataService.ProcessSearchSapPartnerOperationError("Error with  GetApolloPartnerWorkflowData : " + ex.Message, "GetApolloPartnerWorkflowData");
            }
            return partnerDataService.SapPartnerWorkflowDataOperationResult;
        }

        public async Task<OperationResult<SapPartnerWorkflowDataResponse>> GetOlympusPartnerWorkflowData(SearchSapPartnerWorkflowRequest olympusPartnerWorkflowRequest, ILambdaContext context)
        {
            LambdaLogger.Log("Initiating GetOlympusPartnerWorkflowData operation.");
            var partnerDataService = ApplicationSetup.Instance.ServiceProvider.GetService<IPartnerDataService>();
            try
            {
                if (!string.IsNullOrEmpty(olympusPartnerWorkflowRequest.UserId) && olympusPartnerWorkflowRequest.UserId.ToLower() == "test.user")
                {
                    await partnerDataService.RunColdStart();
                    return null;
                }
                return await partnerDataService.GetSapPartnerWorkflowData(SystemType.OLYMPUS, "GetOlympusPartnerWorkflowData", olympusPartnerWorkflowRequest);
            }
            catch (Exception ex)
            {
                partnerDataService.ProcessSearchSapPartnerOperationError("Error with  GetOlympusPartnerWorkflowData : " + ex.Message, "GetOlympusPartnerWorkflowData");
            }
            return partnerDataService.SapPartnerWorkflowDataOperationResult;
        }

        public async Task<OperationResult<SapPartnerWorkflowDataResponse>> GetJdePartnerWorkflowData(SearchSapPartnerWorkflowRequest jdePartnerWorkflowRequest, ILambdaContext context)
        {
            LambdaLogger.Log("Initiating GetJdePartnerWorkflowData operation.");
            var partnerDataService = ApplicationSetup.Instance.ServiceProvider.GetService<IPartnerDataService>();
            try
            {
                if (!string.IsNullOrEmpty(jdePartnerWorkflowRequest.UserId) && jdePartnerWorkflowRequest.UserId.ToLower() == "test.user")
                {
                    await partnerDataService.RunColdStart();
                    return null;
                }
                return await partnerDataService.GetSapPartnerWorkflowData(SystemType.JDE, "GetJdePartnerWorkflowData", jdePartnerWorkflowRequest);
            }
            catch (Exception ex)
            {
                partnerDataService.ProcessSearchSapPartnerOperationError("Error with  GetJdePartnerWorkflowData : " + ex.Message, "GetJdePartnerWorkflowData");
            }
            return partnerDataService.SapPartnerWorkflowDataOperationResult;
        }

        public async Task<OperationResult<GetCustomerFiltersResponse>> GetCustomerFilterData(GetCustomerFiltersRequest getCustomerFiltersRequest, ILambdaContext context)
        {
            LambdaLogger.Log("Initiating GetCustomerFiltersData operation.");
            var partnerDataService = ApplicationSetup.Instance.ServiceProvider.GetService<IPartnerDataService>();
            try
            {
                if (!string.IsNullOrEmpty(getCustomerFiltersRequest.UserId) && getCustomerFiltersRequest.UserId.ToLower() == "test.user")
                {
                    await partnerDataService.RunColdStart();
                    return null;
                }
                return await partnerDataService.GetCustomerFilterData(getCustomerFiltersRequest, (SystemType)getCustomerFiltersRequest.SystemTypeId);
            }
            catch (Exception ex)
            {
                partnerDataService.ProcessOperationError("Error with  GetPartnerData : " + ex.Message, "GetPartnerData");
            }
            return partnerDataService.GetCustomerFiltersResponseOperationResult;
        }
    }
}
