﻿using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Managers
{
    public interface IConnectionManager
    {
        string ConnectionString { get; }
        string RedshiftConnectionString { get; }
        Task<string> GetConnectionString();
        Task<string> GetRedshiftConnectionString();
    }
}
