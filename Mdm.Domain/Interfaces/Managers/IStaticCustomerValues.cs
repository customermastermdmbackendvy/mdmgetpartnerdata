﻿namespace Mdm.Domain.Interfaces.Managers
{
    public interface IStaticCustomerValues
    {
        string RoleType { get; set; }
        string SalesOrgType { get; set; }
        string CustomerClassType { get; set; }
        string IndustryCodeType { get; set; }
        string CompanyCodeType { get; set; }
        string IndustryType { get; set; }
        string ReconAccountType { get; set; }
        string SalesOfficeType { get; set; }
        string CustomerGroupType { get; set; }
        string PPCustProcType { get; set; }
        string CustomerPriceProcType { get; set; }
        string PriceListType { get; set; }
        string DeliveryPriorityType { get; set; }
        string IncoTerms1Type { get; set; }
        string AcctAssignmentGroupType { get; set; }
        string AccountType { get; set; }
        string ShippingCustomerType { get; set; }
        string CreditRepGroupType { get; set; }
        string RiskCategoryType { get; set; }
        string ShippingConditionsType { get; set; }
        string DistributionChannelType { get; set; }
        string DivisionType { get; set; }
        string SpecialPriceType { get; set; }
        string DistPriceType { get; set; }

    }
}
