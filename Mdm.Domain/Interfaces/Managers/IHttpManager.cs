﻿
using Mdm.Domain.Entities;
using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Managers
{
    public interface IHttpManager
    {
        Task<SapOlympusCustomerDomain> GetSapOlympusCustomer(ErpPartnerDataRequest erpPartnerDataRequest);

        Task<SapApolloCustomerDomain> GetSapApolloCustomer(ErpPartnerDataRequest erpPartnerDataRequest);
    }
}
