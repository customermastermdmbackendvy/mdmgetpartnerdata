﻿
namespace Mdm.Domain.Interfaces.Managers
{
    public interface IApiConnectionManager
    {
        string SapApolloBasicAuthData { get; }
        string SapOlympusBasicAuthData { get; }
    }
}
