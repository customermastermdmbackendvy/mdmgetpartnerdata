﻿
using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Managers
{
    public interface ISecretsManager
    {
        Task<string> GetRdsSecret();
        Task<string> GetRedshiftSecret();
        Task<string> GetErpApisSecret();
    }
}
