﻿using System;

namespace Mdm.Domain.Interfaces.Repositories
{
    public interface IBaseRepository : IDisposable
    {
        void Rollback();
        void Commit();
    }
}
