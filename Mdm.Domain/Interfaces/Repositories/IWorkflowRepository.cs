﻿using Mdm.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Repositories
{
    public interface IWorkflowRepository
    {
        Task<WorkflowTaskDomain> GetWorkflowTaskByTeamAsync(int taskId);
        Task<List<WorkflowTaskDomain>> GetWorkflowTaskAsync(string WorkflowId);
        Task<IEnumerable<WorkflowDomain>> GetWorkflowsAsync(IEnumerable<string> workflowIds);
        Task<WorkflowTypeDomain> GetWorkflowTypeAsync(int workflowTypeId);
        Task<List<MdmTeamDomain>> GetAllTeamsAsync();
        Task<List<WorkflowDocumentDomain>> GetWorkflowDocumentByWorkflowID(string workFlowId);
    }
}
