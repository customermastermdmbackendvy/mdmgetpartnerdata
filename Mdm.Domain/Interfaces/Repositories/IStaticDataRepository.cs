﻿

using Mdm.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Repositories
{
    public interface IStaticDataRepository : IBaseRepository
    {
        Task<List<StaticDataTypeDomain>> GetOlympusPartnerFunctionTypes();
        Task<List<StaticDataTypeDomain>> GetOlympusDivisionTypes();
        Task<List<StaticDataTypeDomain>> GetOlympusDistributionChannelTypes();
        Task<List<StaticDataTypeDomain>> GetOlympusSalesOrgTypes();
        Task<List<StaticDataTypeDomain>> GetJdePartnerFunctionTypes();

        Task<List<WorkflowTypeDomain>> GetWorkflowTypeAsync();
        //Task<List<RoleTypeDomain>> GetApolloRoles();
        Task<List<StaticDataTypeDomain>> GetApolloRoleTypes();
        Task<List<StaticDataTypeDomain>> GetOlympusRoleTypes();
        Task<List<StaticDataTypeDomain>> GetJdeRoleTypes();
        Task<List<StaticDataTypeDomain>> GetApolloPartnerFunctionTypes();
        Task<List<StaticDataTypeDomain>> GetSalesOrgs();

        Task<List<ApolloCustomerClassTypeDomain>> GetApolloCustomerClassTypes();

        Task<List<ApolloIndustryCodeTypeDomain>> GetApolloIndustryCodeTypes();
        Task<List<ApolloCompanyCodeTypeDomain>> GetApolloCompanyCodeTypes();

        //Task<List<ApolloIndustryTypeDomain>> GetApolloIndustryTypes();
        Task<List<ApolloMarketingSegmentationTypeDomain>> GetApolloMarketingSegmentationTypes();
        Task<List<ApolloReconAccountTypeDomain>> GetApolloReconAccountTypes();
        Task<List<ApolloSalesOfficeTypeDomain>> GetApolloSalesOfficeTypes();

        Task<List<ApolloCustomerGroupTypeDomain>> GetApolloCustomerGroupTypes();

        Task<List<ApolloPpcustProcTypeDomain>> GetApolloPpcustProcTypes();

        Task<List<ApolloCustomerPriceProcTypeDomain>> GetApolloCustomerPriceProcTypes();

        Task<List<ApolloPriceListTypeDomain>> GetApolloPriceListTypes();

        Task<List<ApolloDeliveryPriorityTypeDomain>> GetApolloDeliveryPriorityTypes();

        Task<List<ApolloIncoTermsTypeDomain>> GetApolloIncoTermsTypes();

        Task<List<ApolloAcctAssignmentGroupTypeDomain>> GetApolloAcctAssignmentGroupTypes();

        Task<List<ApolloAccountTypeDomain>> GetApolloAccountTypes();

        Task<List<ApolloShippingCustomerTypeDomain>> GetApolloShippingCustomerTypes();

        Task<List<ApolloPaymentTermsTypeDomain>> GetApolloPaymentTermsTypes();

        Task<List<WorkflowStateTypeDomain>> GetWorkflowStateTypes();

        Task<List<ApolloRiskCategoryTypeDomain>> GetApolloRiskCategoryTypes();

        Task<List<ApolloShippingConditionsTypeDomain>> GetApolloShippingConditionsTypes();

        Task<List<ApolloCreditRepGroupTypeDomain>> GetApolloCreditRepGroupTypes();

        Task<List<StaticDataTypeDomain>> GetApolloDistributionChannelTypes();

        Task<List<StaticDataTypeDomain>> GetApolloDivisionTypes();

        Task<List<ApolloDistLevelTypeDomain>> GetApolloDistLevelTypes();

        Task<List<ApolloSpecialPricingTypeDomain>> GetApolloSpecialPricingTypes();

        Task<List<ApolloDAPAPriceTypeDomain>> GetApolloDAPAPriceTypes();

        Task<List<ApolloDAPA2PriceTypeDomain>> GetApolloDapa2priceTypes();

        Task<List<ApolloFssPriceTypeDomain>> GetApolloFssPriceTypes();

        Task<List<StaticDataTypeDomain>> GetApolloBlockBillingTypes();

        Task<List<StaticDataTypeDomain>> GetApolloBlockDeliveryTypes();

        Task<List<StaticDataTypeDomain>> GetApolloBlockOrderTypes();
    }
}
