﻿

using Mdm.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Repositories
{
    public interface IUserRepository : IBaseRepository
    {
        Task<MdmUserDomain> GetmdmUserByRequestorId(int RequestorId);
        Task<List<MdmUserDomain>> GetMdmUser(int? teamId, int? UserId);
    }
}
