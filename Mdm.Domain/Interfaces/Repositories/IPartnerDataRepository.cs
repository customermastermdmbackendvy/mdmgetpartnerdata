﻿using Mdm.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Repositories
{
    public interface IPartnerDataRepository : IBaseRepository
    {
        Task<RedshiftCustomerMasterDomain> ReadMdmRepoCustomerByCustomerDetail(string customerNumber, string source, string salesOrg, string division, string distributionChannelType);
        Task<List<RedshiftCustomerMasterDomain>> ReadRedShiftByCustomerNumberSource(string customerNumber, string source);
        Task<RedshiftCustomerMasterDomain> ReadRedShiftByCustomerNumberAndSource(string customerNumber, string source);
        Task<List<RedshiftSapPartner>> ReadSapPartners(string source, string customerNumber, string salesOrg, string division, string distributionChannelType);
        Task<List<RedshiftSapPartner>> ReadJdePartners(string source, string customerNumber);
        Task<List<WorkflowPartnersDomain>> GetWorkflowPartnersByWorkflowId(string workflowId);
        Task<List<WorkflowPartnersDomain>> GetWorkflowPartnersByPartnerOneWorkflowId(string partnerOneWorkflowId);
        Task<List<WorkflowPartnersDomain>> GetWorkflowPartnersByPartnerOneCustomerId(string partnerOneCustomerId);
        Task<MdmMappingDomain> RetrieveMdmMappingDomainByOldMdmNumber(string oldMdmNumber);
        Task<List<RedshiftAggregateDomain>> ReadAggregateDataFromRedShift(string mdmNumber);
        Task<WorkflowUpdatesDomain> GetUpdatesWorkflow(string workflowId, int systemTypeId);
        Task<WorkflowExtendToNewSalesOrgDomain> GetExtendToSalesOrgWorkflow(string workflowId, int systemTypeId);
        Task<WorkflowCreateCustomerDomain> GetWorkflowCreateCustomer(string workflowId, int systemTypeId);
        Task<WorkflowBlockCustomerDomain> GetWorkflowBlockCustomer(string workflowId, int systemTypeId);
        Task<List<WorkflowFunctionCustomerDomain>> RetrieveWorkflowFunctionCustomerData(string workflowId);
        Task<List<RedshiftCustomerMasterDomain>> ReadRedShift(string MdmNumber);
        Task<WorkflowBlockCustomerDomain> GetBlockUnblockWorkflow(string workflowId, int systemTypeId);
        Task<WorkflowDomain> GetWorkflow(string workflowId);
        Task<WorkflowTypeDomain> GetWorkflowTypeAsync(int workflowTypeId);
        Task<string> GetMdmNumberfromCustomerNumber(string customerNumber);
        Task<List<RedshiftPartnerDataDomain>> ReadRedShiftPointmanBillTo(string customerNumber, String customerName = "", string address = "", string city = "",string role="");
        Task<List<RedshiftPartnerDataDomain>> ReadRedShiftPointmanShipTo(string mdmNumber, string customerNumber);
        Task<bool> ReadRedShiftForColdStart();
    }
}
