﻿using System;

namespace Mdm.Domain.Entities
{
    public partial class WorkflowDomain : BaseDomainEntity
    {
        public string WorkflowId { get; set; }
        public int WorkflowRequestorId { get; set; }
        public int WorkflowTypeId { get; set; }
        public int WorkflowStateTypeId { get; set; }
        public DateTime WorkflowCreatedOn { get; set; }
    }
}
