﻿using System;

namespace Mdm.Domain.Entities
{
    public class MdmMappingDomain
    {
        public string OldMdmNumber { get; set; }
        public string NewMdmNumber { get; set; }
        public DateTime ExpiredDate { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}
