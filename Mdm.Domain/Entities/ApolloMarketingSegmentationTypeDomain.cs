﻿namespace Mdm.Domain.Entities
{
    public partial class ApolloMarketingSegmentationTypeDomain
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
