﻿namespace Mdm.Domain.Entities
{
    public partial class CategoryTypeDomain
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
