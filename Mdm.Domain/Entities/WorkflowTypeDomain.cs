﻿namespace Mdm.Domain.Entities
{
    public class WorkflowTypeDomain
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string WorkflowStartRuleName { get; set; }
        public bool CanAutoComplete { get; set; }
    }
}
