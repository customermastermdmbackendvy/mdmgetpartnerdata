﻿namespace Mdm.Domain.Entities
{
    public partial class ApolloDAPA2PriceTypeDomain
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
