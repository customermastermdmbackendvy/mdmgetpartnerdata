﻿namespace Mdm.Domain.Entities
{
    public partial class ApolloPriceListTypeDomain
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
