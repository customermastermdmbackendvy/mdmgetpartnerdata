﻿
namespace Mdm.Domain.Entities
{
    public class ErpPartnerDataRequest
    {
        public int SystemTypeId { get; set; }
        public string WorkflowId { get; set; }
        public string CustomerNumber { get; set; }
        public int RoleTypeId { get; set; }
        public string MdmNumber { get; set; }
    }
}