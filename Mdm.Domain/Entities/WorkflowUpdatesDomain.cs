﻿namespace Mdm.Domain.Entities
{
    public partial class WorkflowUpdatesDomain
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public string WorkflowTitle { get; set; }
        public string MdmCustomerId { get; set; }
        public string SystemRecordId { get; set; }
        public int SystemTypeId { get; set; }
        public int? CompanyCodeTypeId { get; set; }
        public int? DistributionChannelTypeId { get; set; }
        public int? DivisionTypeId { get; set; }
        public string CustomerName { get; set; }
        public int? RoleTypeId { get; set; }
        public int? SalesOrgTypeId { get; set; }
        public string Deltas { get; set; }
    }
}
