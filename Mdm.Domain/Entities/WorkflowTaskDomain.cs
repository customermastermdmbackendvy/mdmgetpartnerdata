﻿using System;

namespace Mdm.Domain.Entities
{
    public class WorkflowTaskDomain : BaseDomainEntity
    {
        public int WorkflowTaskTypeId { get; set; }
        public string WorkflowId { get; set; }
        public int TeamId { get; set; }
        public int WorkflowTaskStateTypeId { get; set; }
        public string WorkflowTaskNote { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
