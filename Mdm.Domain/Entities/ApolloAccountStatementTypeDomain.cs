﻿namespace Mdm.Domain.Entities
{
    public class ApolloAccountStatementTypeDomain
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
