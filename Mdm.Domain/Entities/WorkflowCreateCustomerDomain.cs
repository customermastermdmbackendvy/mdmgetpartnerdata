﻿namespace Mdm.Domain.Entities
{
    public partial class WorkflowCreateCustomerDomain : BaseDomainEntity
    {
        private string _data;
        public string WorkflowId { get; set; }
        public int SystemTypeId { get; set; }
        public string MdmCustomerId { get; set; }
        public string SystemRecordId { get; set; }
        public string Data { get; set; }
       
        public string WorkflowTitle { get; set; }
    }
}
