﻿
namespace Mdm.Domain.Entities
{
    public partial class WorkflowApolloGlobalTradeDomain : BaseDomainEntity
    {
        public string WorkflowId { get; set; }
        public string AdditionalNotes { get; set; }
    }
}
