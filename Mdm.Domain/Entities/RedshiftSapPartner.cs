﻿namespace Mdm.Domain.Entities
{
    public class RedshiftSapPartner
    {
        public string CustomerNumber { get; set; }
        public string MdmNumber { get; set; }
        public string PartnerNumber { get; set; }
        public string PartnerFunctionRole { get; set; }
        public string PartnerCustomerName { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string Postal { get; set; }
        public string CustomerRole { get; set; }
        public bool? IsDefaultPartner { get; set; }
    }
}
