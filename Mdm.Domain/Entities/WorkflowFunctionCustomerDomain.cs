﻿using System;

namespace Mdm.Domain.Entities
{
    public class WorkflowFunctionCustomerDomain
    {
        public string WorkflowId { get; set; }
        public string FunctionName { get; set; }
        public string Data { get; set; }
        public string DataJson { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
