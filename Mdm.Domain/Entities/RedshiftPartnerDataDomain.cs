﻿using Newtonsoft.Json;
using System;

namespace Mdm.Domain.Entities
{
    public class RedshiftPartnerDataDomain
    {
        public string AssignedMdm { get; set; }
        public string BillToCustomerNumber { get; set; }
        public string ShipToCustomerNumber { get; set; }
        public string PartnerFunc { get; set; }
        public string CustomerName { get; set; }

        public string Address1 { get; set; }


        public string City { get; set; }


        public string State { get; set; }


        public string Country { get; set; }

        public string Postal { get; set; }

        [JsonIgnore]
        public string Address
        {
            get
            {
                return (
                (string.IsNullOrEmpty(Address1) ? string.Empty : Address1) + " " +
                (string.IsNullOrEmpty(City) ? string.Empty : City) + " " +
                (string.IsNullOrEmpty(State) ? string.Empty : State) + " " +
                (string.IsNullOrEmpty(Postal) ? string.Empty : Postal) + " " +
                (string.IsNullOrEmpty(Country) ? string.Empty : Country)
                ).Trim();
            }
        }

    }
}
