﻿using System;
using System.Collections.Generic;

namespace Mdm.Domain.Entities
{
    public partial class ApolloPartnerFunctionTypeDomain
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public int? SortOrder { get; set; }
        public bool? IsDefault { get; set; }
    }
}
