﻿using Mdm.Domain.Interfaces.Managers;
using Newtonsoft.Json;

namespace Mdm.Domain.Entities
{
    public partial class SapOlympusCustomerDomain : IStaticCustomerValues
    {

        #region Properties
        [JsonProperty("TITLE")]
        public string Title { get; set; }

        [JsonProperty("NAME1")]

        public string Name1 { get; set; }

        [JsonProperty("NAME2")]

        public string Name2 { get; set; }

        [JsonProperty("NAME3")]

        public string Name3 { get; set; }

        [JsonProperty("NAME4")]

        public string Name4 { get; set; }

        [JsonProperty("STREET")]

        public string Street { get; set; }

        [JsonProperty("STREET2")]

        public string Street2 { get; set; }

        [JsonProperty("CITY")]

        public string City { get; set; }

        [JsonProperty("REGION")]
        public string Region { get; set; }

        [JsonProperty("POSTALCODE")]
        public string PostalCode { get; set; }

        [JsonProperty("COUNTRY")]
        public string Country { get; set; }

        [JsonProperty("TELEPHONE")]
        public string Telephone { get; set; }

        [JsonProperty("FAX")]
        public string Fax { get; set; }

        [JsonProperty("EMAIL")]
        public string Email { get; set; }

        [JsonProperty("TAXNUMBER")]
        public string Taxnumber { get; set; }

        [JsonProperty("VAT_REG_NO")]
        public string VatRegNo { get; set; }

        [JsonProperty("SIC_CODE4")]
        public string SicCode4 { get; set; }

        [JsonProperty("SIC_CODE6")]
        public string SicCode6 { get; set; }

        [JsonProperty("SIC_CODE8")]
        public string SicCode8 { get; set; }

        [JsonProperty("NAICS_CODE")]
        public string NaicsCode { get; set; }

        [JsonProperty("DUNS_NUMBER")]
        public string DunsNumber { get; set; }

        [JsonProperty("ROLE")]
        public string RoleType { get; set; }

        [JsonProperty("SALESORG")]
        public string SalesOrgType { get; set; }

        [JsonProperty("LICENSE_NO")]
        public string License { get; set; }

        [JsonProperty("EXP_DATE")]
        public string LicenseExpDate { get; set; }

        [JsonProperty("SEARCH_TERM1")]
        public string SearchTerm1 { get; set; }

        [JsonProperty("SEARCH_TERM2")]
        public string SearchTerm2 { get; set; }

        [JsonProperty("CUS_CLASS")]
        public string CustomerClassType { get; set; }

        [JsonProperty("INDUSTRY_CODE")]
        public string IndustryCodeType { get; set; }

        [JsonProperty("COMPANY_CODE")]
        public string CompanyCodeType { get; set; }

        [JsonProperty("DISTR_CHAN")]
        public string DistributionChannelType { get; set; }

        [JsonProperty("DIVISION")]
        public string DivisionType { get; set; }

        [JsonProperty("SPL_PRICE")]
        public string SpecialPriceType { get; set; }

        [JsonProperty("DIST_PRICE")]
        public string DistPriceType { get; set; }

        [JsonProperty("TRANS_ZONE")]
        public string TransporationZone { get; set; }

        [JsonProperty("INDUSTRY")]
        public string IndustryType { get; set; }

        [JsonProperty("TAX_NUMBER2")]
        public string TaxNumber2 { get; set; }

        [JsonProperty("RECON_ACCOUNT")]
        public string ReconAccountType { get; set; }

        [JsonProperty("SORT_KEY")]
        public string SortKey { get; set; }

        [JsonProperty("PAYM_HISTORY")]
        public string PaymentHistoryRecord { get; set; }

        [JsonProperty("PAYM_METHOD")]
        public string PaymentMethods { get; set; }

        [JsonProperty("ACC_CLERK")]
        public string AcctgClerk { get; set; }

        [JsonProperty("ACC_STATEMENT")]
        public string AccountStatement { get; set; }

        [JsonProperty("SALES_OFFICE")]
        public string SalesOfficeType { get; set; }

        [JsonProperty("CUST_GROUP")]
        public string CustomerGroupType { get; set; }

        [JsonProperty("PP_CUST_PROC")]
        public string PPCustProcType { get; set; }

        [JsonProperty("CUST_PRICE_PROC")]
        public string CustomerPriceProcType { get; set; }

        [JsonProperty("PRICE_LIST")]
        public string PriceListType { get; set; }

        [JsonProperty("DEL_PRIORITY")]
        public string DeliveryPriorityType { get; set; }

        [JsonProperty("SHIP_COND")]
        public string ShippingConditionsType { get; set; }

        [JsonProperty("ORDER_COMBN")]
        public string OrderCombination { get; set; }

        [JsonProperty("INCOTERMS1")]
        public string IncoTerms1Type { get; set; }

        [JsonProperty("INCOTERMS2")]
        public string IncoTerms2 { get; set; }

        [JsonProperty("ACC_ASGN_GROUP")]
        public string AcctAssignmentGroupType { get; set; }

        [JsonProperty("TAX_CLASS")]
        public string TaxClassification { get; set; }

        [JsonProperty("ACCOUNT_TYPE")]
        public string AccountType { get; set; }

        [JsonProperty("SHIPPING_TYPE")]
        public string ShippingCustomerType { get; set; }

        [JsonProperty("PAYM_TERMS")]
        public string PaymentTermsType { get; set; }

        [JsonProperty("CREDIT_LIMIT")]
        public string CreditLimit { get; set; }

        [JsonProperty("RISK_CATEGORY")]
        public string RiskCategoryType { get; set; }

        [JsonProperty("CREDIT_GROUP")]
        public string CreditRepGroupType { get; set; }

        [JsonProperty("CREDIT_INFO_NO")]
        public string CredInfoNumber { get; set; }

        [JsonProperty("LAST_EXT_REVIEW")]
        public string LastExtReview { get; set; }

        [JsonProperty("RATING")]
        public string Rating { get; set; }

        [JsonProperty("PAYMENT_INDEX")]
        public string PaymentIndex { get; set; }

        [JsonProperty("FIRST_NAME")]
        public string ContactFirstName { get; set; }

        [JsonProperty("LAST_NAME")]
        public string ContactLastName { get; set; }

        [JsonProperty("C_TELEPHONE")]
        public string ContactPhone { get; set; }

        [JsonProperty("C_FAX")]
        public string ContactFax { get; set; }

        [JsonProperty("C_EMAIL")]
        public string ContactEmail { get; set; }

        [JsonProperty("TYPE")]
        public string Type { get; set; }

        [JsonProperty("MESSAGE")]
        public string Message { get; set; }
        #endregion
    }
}
