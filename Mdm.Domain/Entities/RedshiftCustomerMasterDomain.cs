﻿using System;

namespace Mdm.Domain.Entities
{
    public class RedshiftCustomerMasterDomain
    {
        public string source_id { get; set; }
        public string customer_number { get; set; }

        public string globalmdm { get; set; }
        public bool? gold_rec_ind { get; set; }
        public string title { get; set; }
        public string name1 { get; set; }
        public string name2 { get; set; }
        public string name3 { get; set; }
        public string name4 { get; set; }
        public string street { get; set; }
        public string street2 { get; set; }
        public string city { get; set; }
        public string region { get; set; }
        public string postal_code { get; set; }
        public string country { get; set; }
        public string telephone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string category { get; set; }
        public string tax_number { get; set; }
        public string vat_reg_no { get; set; }
        public string dunsnumber { get; set; }
        public string sic_code4 { get; set; }
        public string sic_code6 { get; set; }
        public string sic_code8 { get; set; }
        public string naics_code { get; set; }
        public string tax_juris { get; set; }
        public string role { get; set; }
        public string sales_org { get; set; }
        public DateTime? effective_date { get; set; }
        public string license_number { get; set; }
        public DateTime? license_exp_date { get; set; }
        public string search_term_1 { get; set; }
        public string search_term_2 { get; set; }
        public string customer_class { get; set; }
        public string industry_code_1 { get; set; }
        public string company_code { get; set; }
        public string distribution_channel { get; set; }
        public string division { get; set; }
        public string transportation_zone { get; set; }
        public string tax_number_2 { get; set; }
        public string recon_account { get; set; }
        public string sort_key { get; set; }
        public string payment_history_record { get; set; }
        public string payment_methods { get; set; }
        public string acctg_clerk { get; set; }
        public string account_statement { get; set; }
        public string sales_office { get; set; }
        public string customer_group { get; set; }
        public string pp_cust_proc { get; set; }
        public string cust_pric_proc { get; set; }
        public string price_list { get; set; }
        public string delivery_priority { get; set; }
        public string shipping_conditions { get; set; }
        public string order_combination { get; set; }
        public string incoterms_1 { get; set; }
        public string incoterms_2 { get; set; }
        public string acct_assgmt_group { get; set; }
        public string tax_classification { get; set; }
        public string account_type { get; set; }
        public string shipping_customer_type { get; set; }
        public string payment_terms { get; set; }
        public double? credit_limit { get; set; }
        public string risk_category { get; set; }
        public string credit_rep_group { get; set; }
        public string cred_info_number { get; set; }
        public string payment_index { get; set; }
        public string last_ext_review { get; set; }
        public string rating { get; set; }
        public string special_pricing { get; set; }
        public string dist_level_pricing { get; set; }
        public DateTime mdm_load_ts { get; set; }
        public DateTime mdm_updt_ts { get; set; }
    }
}
