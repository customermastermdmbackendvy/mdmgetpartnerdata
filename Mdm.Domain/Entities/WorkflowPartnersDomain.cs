﻿using System;
using System.Collections.Generic;

namespace Mdm.Domain.Entities
{
    public partial class WorkflowPartnersDomain : BaseDomainEntity
    {
        public string WorkflowId { get; set; }
        public string WorkflowTitle { get; set; }
        public string PurposeOfRequest { get; set; }
        public string PartnerOneWorkflowId { get; set; }
        public string PartnerOneCustomerNumber { get; set; }
        public string PartnerTwoWorkflowId { get; set; }
        public string PartnerOneUniqueID { get; set; }
        public string PartnerTwoUniqueID { get; set; }
        public string PartnerTwoCustomerNumber { get; set; }
        public int SystemTypeId { get; set; }
        public int? RoleTypeId { get; set; }
        public bool IsAddPartnerRequest { get; set; }
        public bool IsDefaultPartner { get; set; }
        public bool IsDeleteRequest { get; set; }
        public int? PartnerOneDistributionChannelTypeId { get; set; }
        public int? PartnerOneDivisionTypeId { get; set; }
        public int? PartnerOneRoleTypeId { get; set; }
        public int? PartnerOneSalesOrgTypeId { get; set; }
        public int PartnerTypeId { get; set; }
    }
}
