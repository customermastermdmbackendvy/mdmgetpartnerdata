﻿namespace Mdm.Domain.Entities
{
    public class WorkflowDocumentDomain
    {
        public int DocumentTypeId { get; set; }
        public string DocumentName { get; set; }
        public string S3objectKey { get; set; }
    }
}
