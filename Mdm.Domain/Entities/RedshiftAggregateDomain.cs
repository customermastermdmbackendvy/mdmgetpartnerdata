﻿namespace Mdm.Domain.Entities
{
    public class RedshiftAggregateDomain
    {
        public string matched_duns { get; set; }
        public string assignedmdm { get; set; }
        public string name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string postal1 { get; set; }
        public string postal2 { get; set; }
        public string telephone { get; set; }
        public string glb_duns { get; set; }
        public string glb_duns_name { get; set; }

    }
}
