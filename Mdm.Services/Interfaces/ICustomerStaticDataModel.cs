﻿using Mdm.Services.Models;

namespace Mdm.Services.Interfaces
{
    interface ICustomerStaticDataModel
    {
        string RoleType { get; set; }
        string SalesOrgType { get; set; }
        string CustomerClassType { get; set; }
        string IndustryCodeType { get; set; }
        string CompanyCodeType { get; set; }
        string IndustryType { get; set; }
        string MarketingSegmentation { get; set; }
        string ReconAccountType { get; set; }
        string SalesOfficeType { get; set; }
        string CustomerGroupType { get; set; }
        string PPCustProcType { get; set; }
        string DistributionChannelType { get; set; }
        string DivisionType { get; set; }
        string CustomerPriceProcType { get; set; }
        string PriceListType { get; set; }
        string DeliveryPriorityType { get; set; }
        string IncoTerms1Type { get; set; }
        string AcctAssignmentGroupType { get; set; }
        string AccountType { get; set; }
        string SpecialPriceType { get; set; }
        string DistPriceType { get; set; }
        string ShippingCustomerType { get; set; }
        string PaymentTermsType { get; set; }

        string CreditRepGroupType { get; set; }
        string RiskCategoryType { get; set; }
        string ShippingConditionsType { get; set; }

        string DAPAPriceType { get; set; }
        string DAPA2PriceType { get; set; }
        string FSSPriceType { get; set; }

        int RoleTypeId { get; set; }
        int SalesOrgTypeId { get; set; }
        int CustomerClassTypeId { get; set; }
        int IndustryCodeTypeId { get; set; }
        int CompanyCodeTypeId { get; set; }
        //int IndustryTypeId { get; set; }
        int MarketingSegmentationTypeId { get; set; }
        int ReconAccountTypeId { get; set; }
        int SalesOfficeTypeId { get; set; }
        int CustomerGroupTypeId { get; set; }
        int PPCustProcTypeId { get; set; }
        int CustomerPriceProcTypeId { get; set; }
        int PriceListTypeId { get; set; }
        int DeliveryPriorityTypeId { get; set; }
        int IncoTerms1TypeId { get; set; }
        int AcctAssignmentGroupTypeId { get; set; }
        int AccountTypeId { get; set; }
        int ShippingCustomerTypeId { get; set; }
        int PaymentTermsTypeId { get; set; }
        int CreditRepGroupTypeId { get; set; }
        int RiskCategoryTypeId { get; set; }
        int ShippingConditionsTypeId { get; set; }
        int DivisionTypeId { get; set; }
        int DistributionChannelTypeId { get; set; }
        int SpecialPricingTypeId { get; set; }
        int DistPriceTypeId { get; set; }

        int DAPAPricingTypeId { get; set; }
        int DAPA2PricingTypeId { get; set; }
        int FSSPricingTypeId { get; set; }
        BlockUnblockData BlockUnblockData { get; set; }
        string BillingBlock_AllSalesArea { get; set; }
        string BillingBlock_SalectedSalesArea { get; set; }
        string DeliveryBlock_AllSalesArea { get; set; }
        string DeliveryBlock_SalectedSalesArea { get; set; }
        string OrderBlock_AllSalesArea { get; set; }
        string OrderBlock_SalectedSalesArea { get; set; }
        string PostingBlock_AllCompanyCodes { get; set; }
        string PostingBlock_SelectedCompanyCode { get; set; }
    }
}
