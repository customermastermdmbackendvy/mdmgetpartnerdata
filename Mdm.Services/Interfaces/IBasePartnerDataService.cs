﻿using AutoMapper;

using Mdm.Services.Models;
using System.Collections.Generic;

namespace Mdm.Services.Interfaces
{
    public interface IBasePartnerDataService
    {
        IMapper Mapper { get; }
        OperationResult<List<PartnerDataResult>> OperationResult { get; set; }
        OperationResult<SapPartnersResponse> SapPartnersOperationResult { get; set; }
        OperationResult<SearchSapPartnerResponse> SearchSapPartnerOperationResult { get; set; }
        OperationResult<SapPartnerWorkflowDataResponse> SapPartnerWorkflowDataOperationResult { get; set; }
        OperationResult<GetCustomerFiltersResponse> GetCustomerFiltersResponseOperationResult { get; set; }
        void ProcessOperationError(string message, string operationName);
        void ProcessSapPartnersOperationError(string message, string operationName);
        void ProcessSearchSapPartnerOperationError(string message, string operationName);
        void ProcessSapPartnerWorkflowDataOperationError(string message, string operationName);
    }
}
