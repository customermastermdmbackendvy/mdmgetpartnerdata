﻿using Mdm.Services.Helpers;
using Mdm.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mdm.Services.Interfaces
{
    public interface IPartnerDataService : IBasePartnerDataService
    {
        Task<OperationResult<SearchSapPartnerResponse>> SearchSapPartner(SystemType systemType, string operationName, SapPartnerRequest searchSapPartnerRequest);
        Task<OperationResult<List<PartnerDataResult>>> GetPartnerData(GetPartnerDataRequest getPartnerDataRequest);
        Task<OperationResult<SapPartnersResponse>> GetSapPartners(SystemType systemType, string operationName, SapPartnersRequest sapPartnersRequest);
        Task<OperationResult<SapPartnerWorkflowDataResponse>> GetSapPartnerWorkflowData(SystemType systemType, string operationName, SearchSapPartnerWorkflowRequest sapPartnerWorkflowRequest);
        Task<OperationResult<GetCustomerFiltersResponse>> GetCustomerFilterData(GetCustomerFiltersRequest getCustomerFiltersRequest, SystemType systemType);
        Task<bool> RunColdStart();
    }
}
