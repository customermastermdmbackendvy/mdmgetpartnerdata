﻿using AutoMapper;
using Mdm.Domain.Entities;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Domain.Interfaces.Repositories;
using Mdm.Services.Helpers;
using Mdm.Services.Interfaces;
using Mdm.Services.Models;
using Mdm.WorkflowEngine.Helpers.Constants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Mdm.Services
{
    public class PartnerDataService : BasePartnerDataService, IPartnerDataService
    {
        private readonly IPartnerDataRepository _partnerDataRepository;
        private readonly IWorkflowRepository _workflowRepository;
        private readonly IUserRepository _userRepository;
        private readonly IStaticDataRepository _staticDataRepository;
        private readonly IHttpManager _httpManager;

        public PartnerDataService(
            IPartnerDataRepository partnerDataRepository,
            IWorkflowRepository workflowRepository,
            IUserRepository userRepository,
            IStaticDataRepository staticDataRepository,
            IHttpManager httpManager,
            IMapper mapper) : base(mapper, partnerDataRepository)
        {
            _partnerDataRepository = partnerDataRepository;
            _workflowRepository = workflowRepository;
            _userRepository = userRepository;
            _staticDataRepository = staticDataRepository;
            _httpManager = httpManager;
        }

        public async Task<bool> RunColdStart()
        {
            var workflows = await _workflowRepository.GetWorkflowsAsync(new string[] { "wftest" });
            var redshiftData = await _partnerDataRepository.ReadRedShiftForColdStart();
            return true;
        }

        public async Task<OperationResult<List<PartnerDataResult>>> GetPartnerData(GetPartnerDataRequest getPartnerDataRequest)
        {
            InitOperationResult();

            if ((SystemType)getPartnerDataRequest.SystemTypeId == SystemType.Pointman)
            {
                ValidatePointmanRequest(getPartnerDataRequest);
            }
            else
            {
                ValidateRequest(getPartnerDataRequest);
            }

            if (!OperationResult.IsSuccess)
            {
                return OperationResult;
            }

            if ((SystemType)getPartnerDataRequest.SystemTypeId == SystemType.Pointman)
            {
                var billToCustomerNumber = getPartnerDataRequest.CustomerNumber;
                if (!string.IsNullOrEmpty(getPartnerDataRequest.WorkflowId))
                {
                    var workflowDetails = await GetWorkflowDetails(getPartnerDataRequest, SystemType.Pointman);

                    if (workflowDetails == null)
                    {
                        ProcessOperationError("No Pointman Workflows Exist");
                        OperationResult.IsSuccess = false;
                        return OperationResult;
                    }

                    billToCustomerNumber = workflowDetails.CustomerNumber;
                }
                // Initialize partners response list
                var partnerDataResults = new List<PartnerDataResult>();

                // If Bill To Customer is Found, then read Mdm Repo Partner Data first
                if (!string.IsNullOrEmpty(billToCustomerNumber))
                {
                    // Find Mdm Repo Pointman Partners
                    var mdmPartnerData = await _partnerDataRepository.ReadRedShiftPointmanBillTo(billToCustomerNumber,getPartnerDataRequest.CustomerName, getPartnerDataRequest.Address, getPartnerDataRequest.City, getPartnerDataRequest.Role);
                    var mdmPartnerDataResults = Mapper.Map<List<PartnerDataResult>>(mdmPartnerData);
                    if (mdmPartnerDataResults.Any())
                    {
                        partnerDataResults.AddRange(mdmPartnerDataResults);
                    }
                }

                // Find Workflow Pointman Partners with either the Bill To Customer or Workflow Number
                var workflowPartnerData = string.IsNullOrEmpty(billToCustomerNumber) ?
                        await _partnerDataRepository.GetWorkflowPartnersByPartnerOneWorkflowId(getPartnerDataRequest.WorkflowId)
                     : await _partnerDataRepository.GetWorkflowPartnersByPartnerOneCustomerId(billToCustomerNumber);

                var customerNumberOrWorkflowNumber = string.IsNullOrEmpty(billToCustomerNumber) ? getPartnerDataRequest.WorkflowId : billToCustomerNumber;
                workflowPartnerData?.ForEach(async workflowPartner =>
                {
                    // If the ShipTo Partner is only just a workflow
                    if (string.IsNullOrEmpty(workflowPartner.PartnerTwoCustomerNumber) && !string.IsNullOrEmpty(workflowPartner.PartnerTwoWorkflowId))
                    {
                        var workflow = await _partnerDataRepository.GetWorkflow(workflowPartner.PartnerTwoWorkflowId);

                        if (workflow != null &&
                            workflow.WorkflowStateTypeId != (int)WorkflowStateType.Rejected &&
                            workflow.WorkflowStateTypeId != (int)WorkflowStateType.Withdrawn)
                        {
                            var createCustomer = await _partnerDataRepository.GetWorkflowCreateCustomer(workflowPartner.PartnerTwoWorkflowId, (int)SystemType.Pointman);
                            var customerData = JsonConvert.DeserializeObject<CustomerDataModel>(createCustomer.Data);
                            partnerDataResults.Add(ConvertToPartnerDataResult(createCustomer, customerNumberOrWorkflowNumber));
                        }
                    }
                });

                OperationResult.ResultData = partnerDataResults;

            }
            else
            {
                ProcessOperationError("Invalid System Type in request");
                OperationResult.IsSuccess = false;
                return OperationResult;
            }
            return OperationResult;
        }

        public async Task<OperationResult<SapPartnersResponse>> GetSapPartners(SystemType systemType, string operationName, SapPartnersRequest sapPartnersRequest)
        {
            InitSapPartnersOperationResult(operationName);

            if (!ValidateSapPartnersRequest(systemType, sapPartnersRequest))
            {
                return SapPartnersOperationResult;
            }

            SapPartnersOperationResult.ResultData.WorkflowOrCustomerNumber = sapPartnersRequest.WorkflowOrCustomerNumber;
            var workflowPartners = new List<WorkflowPartnersDomain>();
            var customerPartners = new List<WorkflowPartnersDomain>();

            // Check if Workflow or Customer number
            if (sapPartnersRequest.WorkflowOrCustomerNumber.ToLower().StartsWith("wf"))
            {
                // If Workflow, then check if completed and turned into a customer number
                var workflow = (await _workflowRepository.GetWorkflowsAsync(new string[] { sapPartnersRequest.WorkflowOrCustomerNumber }.ToList())).FirstOrDefault();
                if (workflow == null)
                {
                    ProcessSapPartnersOperationError("Missing Workflow Data for WorkflowId: " + workflow.WorkflowId);
                    return SapPartnersOperationResult;
                }
                var workflowCreateCustomer = await _partnerDataRepository.GetWorkflowCreateCustomer(workflow.WorkflowId, (int)systemType);
                if (workflowCreateCustomer == null)
                {
                    ProcessSapPartnersOperationError("Missing Create Customer Workflow Data for WorkflowId: " + workflow.WorkflowId);
                    return SapPartnersOperationResult;
                }

                if (!string.IsNullOrEmpty(workflowCreateCustomer.SystemRecordId))
                { 
                    SapPartnersOperationResult.ResultData.WorkflowOrCustomerNumber = workflowCreateCustomer.SystemRecordId;
                    sapPartnersRequest.WorkflowOrCustomerNumber= workflowCreateCustomer.SystemRecordId; 
                }
                else
                {
                    if (string.IsNullOrEmpty(workflowCreateCustomer.Data))
                    {
                        return SapPartnersOperationResult;
                    }

                    var workflowCreateCustomerData = JsonConvert.DeserializeObject<CustomerDataModel>(workflowCreateCustomer.Data);
                    if (workflowCreateCustomerData == null || workflowCreateCustomerData.CustomerElements == null || !workflowCreateCustomerData.CustomerElements.Any())
                    {
                        return SapPartnersOperationResult;
                    }

                    var roleTypeId = workflowCreateCustomerData.CustomerElements.GetValue("RoleTypeId");

                    if (systemType == SystemType.JDE)
                    {
                        if (Convert.ToInt32(roleTypeId) != 1 && Convert.ToInt32(roleTypeId) != 3) // 1 - Sold To, 2 - Bill To, 3 - Ship To | Parent cannot be Bill To
                        {
                            return SapPartnersOperationResult;
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(roleTypeId) != (int)SapFunctionPartnerType.SP)
                        {
                            return SapPartnersOperationResult;
                        }

                        var salesOrgTypeId = workflowCreateCustomerData.CustomerElements.GetValue("SalesOrgTypeId");
                        if (salesOrgTypeId == null || (Convert.ToInt32(salesOrgTypeId)) != (Convert.ToInt32(sapPartnersRequest.SalesOrgTypeId)))
                        {
                            return SapPartnersOperationResult;
                        }

                        var distributionChannelTypeId = workflowCreateCustomerData.CustomerElements.GetValue("DistributionChannelTypeId");
                        if (distributionChannelTypeId == null || (Convert.ToInt32(distributionChannelTypeId)) != (Convert.ToInt32(sapPartnersRequest.DistributionChannelTypeId)))
                        {
                            return SapPartnersOperationResult;
                        }

                        var divisionTypeId = workflowCreateCustomerData.CustomerElements.GetValue("DivisionTypeId");
                        if (divisionTypeId == null || (Convert.ToInt32(divisionTypeId)) != (Convert.ToInt32(sapPartnersRequest.DivisionTypeId)))
                        {
                            return SapPartnersOperationResult;
                        }
                    }
                    if (workflow.WorkflowStateTypeId == (int)WorkflowStateType.Completed)
                    {
                        if (!string.IsNullOrEmpty(workflowCreateCustomer.SystemRecordId))
                        {
                            // Add to result-set the partners coming from the MDM Repository
                            if(systemType == SystemType.JDE)
                            {
                                SapPartnersOperationResult.ResultData.SapPartners.AddRange(await GetSapPartnersByCustomerNumber(systemType, workflowCreateCustomer.SystemRecordId));

                                // Perform sap parnters validation
                                if (!AreJdePartnersValid(SapPartnersOperationResult.ResultData.SapPartners, workflowCreateCustomer.SystemRecordId, workflow.WorkflowId))
                                {
                                    SapPartnersOperationResult.ResultData.SapPartners.Clear();
                                    return SapPartnersOperationResult;
                                }
                            }
                            else
                            {
                                SapPartnersOperationResult.ResultData.SapPartners.AddRange(await GetSapPartnersByCustomerNumber(systemType, workflowCreateCustomer.SystemRecordId, Convert.ToInt32(sapPartnersRequest.DivisionTypeId), Convert.ToInt32(sapPartnersRequest.DistributionChannelTypeId), Convert.ToInt32(sapPartnersRequest.SalesOrgTypeId)));

                                // Perform sap parnters validation
                                if (!AreSapPartnersValid(SapPartnersOperationResult.ResultData.SapPartners, workflowCreateCustomer.SystemRecordId, workflow.WorkflowId))
                                {
                                    SapPartnersOperationResult.ResultData.SapPartners.Clear();
                                    return SapPartnersOperationResult;
                                }
                            }
                        }
                        else
                        {
                            ProcessSapPartnersOperationError("Workflow is completed but there is no customer number saved for WorkflowId: " + workflow.WorkflowId);
                            return SapPartnersOperationResult;
                        }

                        customerPartners.AddRange(await _partnerDataRepository.GetWorkflowPartnersByPartnerOneCustomerId(workflowCreateCustomer.SystemRecordId));
                    }
                    else if (workflow.WorkflowStateTypeId == (int)WorkflowStateType.InProgress)
                    {
                        var sapPartners = MapSapPartnersCustomerDataElements(systemType, workflowCreateCustomerData.CustomerElements, sapPartnersRequest.WorkflowOrCustomerNumber);
                        if (sapPartners == null)
                            return SapPartnersOperationResult;
                        SapPartnersOperationResult.ResultData.SapPartners.AddRange(sapPartners);
                    }
                    else
                    {
                        ProcessSapPartnersOperationError("Workflow is not in-progress or completion state for WorkflowId: " + workflow.WorkflowId);
                        return SapPartnersOperationResult;
                    }

                    // Get any existing Partners Records that may be in progress.  If in progress, then do not allow edit.
                    workflowPartners.AddRange(await _partnerDataRepository.GetWorkflowPartnersByPartnerOneWorkflowId(workflow.WorkflowId));
                }
            }

            if (!sapPartnersRequest.WorkflowOrCustomerNumber.ToLower().StartsWith("wf"))
            {
                // If searching by Customer Number, check the MDM Database for partners
                // Add to result-set the partners coming from the MDM Repository
                var customerNumber = systemType != SystemType.JDE ? sapPartnersRequest.WorkflowOrCustomerNumber.PadLeft(10, '0') : sapPartnersRequest.WorkflowOrCustomerNumber?.TrimStart(new Char[] { '0' }).Trim();
                if (systemType == SystemType.JDE)
                {
                    SapPartnersOperationResult.ResultData.WorkflowOrCustomerNumber = customerNumber;
                    SapPartnersOperationResult.ResultData.SapPartners.AddRange(await GetSapPartnersByCustomerNumber(systemType, customerNumber));

                    // Perform sap parnters validation
                    if (!AreJdePartnersValid(SapPartnersOperationResult.ResultData.SapPartners, customerNumber))
                    {
                        SapPartnersOperationResult.ResultData.SapPartners.Clear();
                        return SapPartnersOperationResult;
                    }
                }
                else
                {
                    SapPartnersOperationResult.ResultData.SapPartners.AddRange(await GetSapPartnersByCustomerNumber(systemType, customerNumber, Convert.ToInt32(sapPartnersRequest.DivisionTypeId), Convert.ToInt32(sapPartnersRequest.DistributionChannelTypeId), Convert.ToInt32(sapPartnersRequest.SalesOrgTypeId)));

                    // Perform sap parnters validation
                    if (!AreSapPartnersValid(SapPartnersOperationResult.ResultData.SapPartners, customerNumber))
                    {
                        SapPartnersOperationResult.ResultData.SapPartners.Clear();
                        return SapPartnersOperationResult;
                    }
                }

                customerPartners.AddRange(await _partnerDataRepository.GetWorkflowPartnersByPartnerOneCustomerId(customerNumber));
            }

            var existingPartners = new List<WorkflowPartnersDomain>();
            if (workflowPartners.Any())
                existingPartners.AddRange(workflowPartners);

            if (customerPartners.Any())
                existingPartners.AddRange(customerPartners);

            existingPartners = existingPartners.Distinct().ToList();

            // Check if the main account is currently in the middle of a partner workflow that is in progress
            if (existingPartners.Any())
                SapPartnersOperationResult.ResultData.AllowSapPartnerEdit = await AllowEditOfPartners(existingPartners);

            SetPartnerFlags(SapPartnersOperationResult.ResultData.SapPartners, systemType);
            return SapPartnersOperationResult;
        }

        public async Task<OperationResult<SearchSapPartnerResponse>> SearchSapPartner(SystemType systemType, string operationName, SapPartnerRequest searchSapPartnerRequest)
        {
            InitSearchSapPartnerOperationResult(operationName);

            if (!ValidateSearchSapPartnerRequest(systemType, searchSapPartnerRequest))
            {
                return SearchSapPartnerOperationResult;
            }

            SearchSapPartnerOperationResult.ResultData.WorkflowOrCustomerNumber = searchSapPartnerRequest.WorkflowOrCustomerNumber;

            // Check if Workflow or Customer number
            if (searchSapPartnerRequest.WorkflowOrCustomerNumber.ToLower().StartsWith("wf"))
            {
                // If Workflow, then check if completed and turned into a customer number
                var workflow = (await _workflowRepository.GetWorkflowsAsync(new string[] { searchSapPartnerRequest.WorkflowOrCustomerNumber }.ToList())).FirstOrDefault();
                if (workflow == null)
                {
                    ProcessSearchSapPartnerOperationValidation("Missing Workflow Data for WorkflowId: " + searchSapPartnerRequest.WorkflowOrCustomerNumber);
                    return SearchSapPartnerOperationResult;
                }
                var workflowCreateCustomer = await _partnerDataRepository.GetWorkflowCreateCustomer(workflow.WorkflowId, (int)systemType);
                if (workflowCreateCustomer == null)
                {
                    ProcessSearchSapPartnerOperationValidation("Missing Customer Workflow Data for WorkflowId: " + workflow.WorkflowId);
                    return SearchSapPartnerOperationResult;
                }

                if (string.IsNullOrEmpty(workflowCreateCustomer.Data))
                {
                    return SearchSapPartnerOperationResult;
                }

                var workflowCreateCustomerData = JsonConvert.DeserializeObject<CustomerDataModel>(workflowCreateCustomer.Data);
                if (workflowCreateCustomerData == null || workflowCreateCustomerData.CustomerElements == null || !workflowCreateCustomerData.CustomerElements.Any())
                {
                    return SearchSapPartnerOperationResult;
                }

                if (workflow.WorkflowStateTypeId == (int)WorkflowStateType.Completed)
                {
                    if (!string.IsNullOrEmpty(workflowCreateCustomer.SystemRecordId))
                    {
                        var searchSapPartnerResponse = await PerformSearchSapPartnerResponse(workflowCreateCustomer.SystemRecordId, systemType.ToString(), Convert.ToInt32(searchSapPartnerRequest.DivisionTypeId), Convert.ToInt32(searchSapPartnerRequest.DistributionChannelTypeId), Convert.ToInt32(searchSapPartnerRequest.SalesOrgTypeId));
                        if (searchSapPartnerResponse == null)
                            return SearchSapPartnerOperationResult;

                        SetSearchSapPartnerOperationResult(searchSapPartnerResponse);
                    }
                    else
                    {
                        ProcessSearchSapPartnerOperationError("Workflow is completed but there is no customer number saved for WorkflowId: " + workflow.WorkflowId);
                        return SearchSapPartnerOperationResult;
                    }
                }
                else if (workflow.WorkflowStateTypeId == (int)WorkflowStateType.InProgress)
                {
                    // Find the workflow details in the Workflow Table if in-progress workflow
                    var searchSapPartnerResponse = PerformWorkflowSearchSapPartnerResponse(workflowCreateCustomerData.CustomerElements, systemType.ToString(), Convert.ToInt32(searchSapPartnerRequest.DivisionTypeId), Convert.ToInt32(searchSapPartnerRequest.DistributionChannelTypeId), Convert.ToInt32(searchSapPartnerRequest.SalesOrgTypeId), workflow.WorkflowId);
                    if (searchSapPartnerResponse == null)
                    {
                        var salesOrgTypes = systemType == SystemType.APOLLO ? StaticTypeData.SalesOrgTypes : StaticTypeData.OlympusSalesOrgTypes;
                        var distChannelTypes = systemType == SystemType.APOLLO ? StaticTypeData.ApolloDistributionChannelTypes : StaticTypeData.OlympusDistributionChannelTypes;
                        var divisionTypes = systemType == SystemType.APOLLO ? StaticTypeData.ApolloDivisionTypes : StaticTypeData.OlympusDivisionTypes;

                        var divisionType = divisionTypes?.Where(x => x.Id == Convert.ToInt32(searchSapPartnerRequest.DivisionTypeId)).FirstOrDefault();
                        var distributionChannelType = distChannelTypes?.Where(x => x.Id == Convert.ToInt32(searchSapPartnerRequest.DistributionChannelTypeId)).FirstOrDefault();
                        var salesOrgType = salesOrgTypes?.Where(x => x.Id == Convert.ToInt32(searchSapPartnerRequest.SalesOrgTypeId)).FirstOrDefault();

                        ProcessSearchSapPartnerOperationError("Workflow " + workflow.WorkflowId + " has not been created for sales area " + salesOrgType?.Value + " " + distributionChannelType?.Value + " " + divisionType?.Value);
                        return SearchSapPartnerOperationResult;
                    }

                    SetSearchSapPartnerOperationResult(searchSapPartnerResponse);
                }
                else
                {
                    ProcessSearchSapPartnerOperationError("Workflow is not in-progress or completion state for WorkflowId: " + workflow.WorkflowId);
                    return SearchSapPartnerOperationResult;
                }
            }
            else
            {
                var customerNumber = systemType != SystemType.JDE ? searchSapPartnerRequest.WorkflowOrCustomerNumber.PadLeft(10, '0') : searchSapPartnerRequest.WorkflowOrCustomerNumber?.TrimStart(new Char[] { '0' }).Trim();
                var searchSapPartnerResponse = await PerformSearchSapPartnerResponse(customerNumber, systemType.ToString(), Convert.ToInt32(searchSapPartnerRequest.DivisionTypeId), Convert.ToInt32(searchSapPartnerRequest.DistributionChannelTypeId), Convert.ToInt32(searchSapPartnerRequest.SalesOrgTypeId));
                if (searchSapPartnerResponse == null)
                {
                    if (SearchSapPartnerOperationResult != null && SearchSapPartnerOperationResult.ResultData != null)
                    {
                        SearchSapPartnerOperationResult.ResultData.WorkflowOrCustomerNumber = searchSapPartnerRequest.RawWorkflowOrCustomerNumber;
                    }
                    return SearchSapPartnerOperationResult;
                }

                SetSearchSapPartnerOperationResult(searchSapPartnerResponse);
            }
            return SearchSapPartnerOperationResult;
        }

        public async Task<OperationResult<SapPartnerWorkflowDataResponse>> GetSapPartnerWorkflowData(SystemType systemType, string operationName, SearchSapPartnerWorkflowRequest sapPartnerWorkflowRequest)
        {
            InitSapPartnerWorkflowDataOperationResult(operationName);
            if (!ValidateSapPartnerWorkflowRequest(sapPartnerWorkflowRequest))
            {
                return SapPartnerWorkflowDataOperationResult;
            }

            // Find all partners and create error if none found.
            var allPartners = await _partnerDataRepository.GetWorkflowPartnersByWorkflowId(sapPartnerWorkflowRequest.WorkflowId);
            if (allPartners == null || allPartners.Count == 0)
            {
                ProcessSapPartnerWorkflowDataOperationError("No partners found for Partner Workflow ID: " + sapPartnerWorkflowRequest.WorkflowId);
                return SapPartnerWorkflowDataOperationResult;
            }

            if (!await BuildSapPartnersFromWorkflowRequest(systemType, allPartners[0]))
                return SapPartnerWorkflowDataOperationResult;

            if (!await BuildPartnerEditsForCustomer(systemType, allPartners))
                return SapPartnerWorkflowDataOperationResult;

            SapPartnerWorkflowDataOperationResult.ResultData.AllowSapPartnerEdit = await AllowEditOfPartners(allPartners);
            var workflowTasks = await _workflowRepository.GetWorkflowTaskAsync(sapPartnerWorkflowRequest.WorkflowId);
            var taskNoteData = new List<TaskNoteData>();
            List<string> Approve_Rejected_Users = new List<string>();
            if (workflowTasks != null)
            {
                if (workflowTasks?.Count() != 0)
                {
                    var workflowTask = workflowTasks.FirstOrDefault();
                    if (workflowTask != null)
                    {
                        SapPartnerWorkflowDataOperationResult.ResultData.TaskId = workflowTask.Id;
                        SapPartnerWorkflowDataOperationResult.ResultData.WorkflowTaskStateTypeId = workflowTask.WorkflowTaskStateTypeId;
                        SapPartnerWorkflowDataOperationResult.ResultData.WorkflowTaskNote = workflowTask.WorkflowTaskNote;
                    }
                    workflowTasks.ForEach(async x =>
                    {
                        if (x.WorkflowTaskStateTypeId == 4 || x.WorkflowTaskStateTypeId == 5)
                        {
                            Approve_Rejected_Users = ((await _userRepository.GetMdmUser(null, x.ModifiedUserId)).Select(z => z.Value).ToList());
                        }
                        else
                        {
                            Approve_Rejected_Users = new List<string>();
                        }
                        taskNoteData.Add(new TaskNoteData
                        {
                            TaskNote = x.WorkflowTaskNote,
                            TeamName = ((WorkflowTeamType)x.TeamId).ToString(),
                            WorkflowTaskState = (WorkflowTaskStateType)x.WorkflowTaskStateTypeId,
                            ModifiedDate = x.ModifiedOn,
                            ApprovedRejectedUser = Approve_Rejected_Users
                        });
                    });
                    taskNoteData.OrderByDescending(x => x.ModifiedDate);
                }
            }
            SapPartnerWorkflowDataOperationResult.ResultData.TaskNoteData = taskNoteData;

            var workflow = await _partnerDataRepository.GetWorkflow(sapPartnerWorkflowRequest.WorkflowId);

            if (workflow != null)
            {
                var mdmUser = await _userRepository.GetmdmUserByRequestorId(workflow.WorkflowRequestorId);
                SapPartnerWorkflowDataOperationResult.ResultData.RequestorName = mdmUser?.Value != null ? mdmUser.Value : null;
                SapPartnerWorkflowDataOperationResult.ResultData.DateOfCreation = workflow.CreatedOn;
                SapPartnerWorkflowDataOperationResult.ResultData.WorkflowStateTypeId = workflow.WorkflowStateTypeId;
            }

            SapPartnerWorkflowDataOperationResult.ResultData.UserId = sapPartnerWorkflowRequest.UserId;
            SapPartnerWorkflowDataOperationResult.ResultData.WorkflowId = sapPartnerWorkflowRequest.WorkflowId;
            SapPartnerWorkflowDataOperationResult.ResultData.WorkflowTitle = allPartners[0].WorkflowTitle;
            SapPartnerWorkflowDataOperationResult.ResultData.PurposeOfRequest = allPartners[0].PurposeOfRequest;

            SapPartnerWorkflowDataOperationResult.ResultData.SystemTypeId = allPartners[0].SystemTypeId;

            SapPartnerWorkflowDataOperationResult.ResultData.DistributionChannelTypeId = allPartners[0].PartnerOneDistributionChannelTypeId;
            SapPartnerWorkflowDataOperationResult.ResultData.DivisionTypeId = allPartners[0].PartnerOneDivisionTypeId;
            SapPartnerWorkflowDataOperationResult.ResultData.SalesOrgTypeId = allPartners[0].PartnerOneSalesOrgTypeId;
            SapPartnerWorkflowDataOperationResult.ResultData.RoleTypeId = allPartners[0].PartnerOneRoleTypeId;

            SapPartnerWorkflowDataOperationResult.ResultData.WorkflowOrCustomerNumber = !string.IsNullOrEmpty(allPartners[0].PartnerOneCustomerNumber) ? allPartners[0].PartnerOneCustomerNumber : allPartners[0].PartnerOneWorkflowId;

            SetPartnerFlags(SapPartnerWorkflowDataOperationResult.ResultData.SapPartners, systemType);

            var documentLocationList = await _workflowRepository.GetWorkflowDocumentByWorkflowID(sapPartnerWorkflowRequest.WorkflowId);
            if (documentLocationList != null && documentLocationList.Count != 0)
            {
                SapPartnerWorkflowDataOperationResult.ResultData.DocumentLocation = Mapper.Map<List<WorkflowDocumentDomain>, List<DocumentModel>>(documentLocationList);
            }

            SapPartnerWorkflowDataOperationResult.IsSuccess = true;
            SapPartnerWorkflowDataOperationResult.OperationResultMessages.Add
            (
                new OperationResultMessage { Message = "Successful read", OperationalResultType = OperationalResultType.Success }
            );

            return SapPartnerWorkflowDataOperationResult;
        }

        public async Task<OperationResult<GetCustomerFiltersResponse>> GetCustomerFilterData(GetCustomerFiltersRequest getCustomerFiltersRequest, SystemType systemType)
        {
            InitGetCustomerFiltersResponseOperationResult();

            if (!ValidateGetCustomerFiltersRequest(getCustomerFiltersRequest))
            {
                return GetCustomerFiltersResponseOperationResult;
            }

            GetCustomerFiltersResponseOperationResult.ResultData.CustomerNumber = getCustomerFiltersRequest.CustomerNumber;
            GetCustomerFiltersResponseOperationResult.ResultData.SystemTypeId = systemType.ToString();
            GetCustomerFiltersResponseOperationResult.IsSuccess = false;

            // Check if Workflow or Customer number
            if (getCustomerFiltersRequest.CustomerNumber.ToLower().StartsWith("wf"))
            {
                // If Workflow, then check if completed and turned into a customer number
                var workflow = (await _workflowRepository.GetWorkflowsAsync(new string[] { getCustomerFiltersRequest.CustomerNumber }.ToList())).FirstOrDefault();
                if (workflow == null)
                {
                    ProcessSearchSapPartnerOperationValidation("Missing Workflow Data for WorkflowId: " + workflow.WorkflowId);
                    return GetCustomerFiltersResponseOperationResult;
                }
                var workflowCreateCustomer = await _partnerDataRepository.GetWorkflowCreateCustomer(workflow.WorkflowId, (int)systemType);
                if (workflowCreateCustomer == null)
                {
                    ProcessSearchSapPartnerOperationValidation("Missing Customer Workflow Data for WorkflowId: " + workflow.WorkflowId);
                    return GetCustomerFiltersResponseOperationResult;
                }

                if (string.IsNullOrEmpty(workflowCreateCustomer.Data))
                {
                    return GetCustomerFiltersResponseOperationResult;
                }

                var workflowCreateCustomerData = JsonConvert.DeserializeObject<CustomerDataModel>(workflowCreateCustomer.Data);
                if (workflowCreateCustomerData == null || workflowCreateCustomerData.CustomerElements == null || !workflowCreateCustomerData.CustomerElements.Any())
                {
                    return GetCustomerFiltersResponseOperationResult;
                }

                var salesOrgTypeId = workflowCreateCustomerData.CustomerElements.GetValue("SalesOrgTypeId");
                if (salesOrgTypeId == null)
                {
                    return GetCustomerFiltersResponseOperationResult;
                }

                var distributionChannelTypeId = workflowCreateCustomerData.CustomerElements.GetValue("DistributionChannelTypeId");
                if (distributionChannelTypeId == null)
                {
                    return GetCustomerFiltersResponseOperationResult;
                }

                var divisionTypeId = workflowCreateCustomerData.CustomerElements.GetValue("DivisionTypeId");
                if (divisionTypeId == null)
                {
                    return GetCustomerFiltersResponseOperationResult;
                }

                GetCustomerFiltersResponseOperationResult.ResultData.SalesOrgs = new List<string>() { salesOrgTypeId };
                GetCustomerFiltersResponseOperationResult.ResultData.DistChannels = new List<string>() { distributionChannelTypeId };
                GetCustomerFiltersResponseOperationResult.ResultData.Divisions = new List<string>() { divisionTypeId };
                GetCustomerFiltersResponseOperationResult.IsSuccess = true;

            }
            else
            {
                var customerNumber = getCustomerFiltersRequest.CustomerNumber.PadLeft(10, '0');
                var searchApolloPartnerResponse = await PerformGetCustomerFiltersResponse(customerNumber, ((SystemType)getCustomerFiltersRequest.SystemTypeId).ToString());
                if (searchApolloPartnerResponse == null)
                    return GetCustomerFiltersResponseOperationResult;

                //if ((SystemType)getCustomerFiltersRequest.SystemTypeId == SystemType.APOLLO)
                //{
                //}
                //else if ((SystemType)getCustomerFiltersRequest.SystemTypeId == SystemType.OLYMPUS)
                //{
                //}
                //else if ((SystemType)getCustomerFiltersRequest.SystemTypeId == SystemType.Pointman)
                //{
                //}
                //else if ((SystemType)getCustomerFiltersRequest.SystemTypeId == SystemType.Made2Manage)
                //{
                //}
                //else if ((SystemType)getCustomerFiltersRequest.SystemTypeId == SystemType.JDE)
                //{
                //}
                //else if ((SystemType)getCustomerFiltersRequest.SystemTypeId == SystemType.Salesforce)
                //{
                //}
                //else
                //{
                //    ProcessGetCustomerFiltersResponseOperationError("Invalid System Type in request");
                //    GetCustomerFiltersResponseOperationResult.IsSuccess = false;
                //    return GetCustomerFiltersResponseOperationResult;
                //}

                GetCustomerFiltersResponseOperationResult.ResultData.SalesOrgs = searchApolloPartnerResponse.SalesOrgs;
                GetCustomerFiltersResponseOperationResult.ResultData.DistChannels = searchApolloPartnerResponse.DistChannels;
                GetCustomerFiltersResponseOperationResult.ResultData.Divisions = searchApolloPartnerResponse.Divisions;
                GetCustomerFiltersResponseOperationResult.IsSuccess = true;
            }

            if (!GetCustomerFiltersResponseOperationResult.IsSuccess)
            {
                return GetCustomerFiltersResponseOperationResult;
            }

            return GetCustomerFiltersResponseOperationResult;
        }

        #region Private Helpers


        private bool AreSapPartnersValid(List<SapPartner> sapPartners, string customerNumber, string workflowId=null )
        {
            if (!string.IsNullOrEmpty(workflowId))
            {
                if (!sapPartners.Any())
                {
                    ProcessSapPartnersOperationError("There are no Partners found in the MDM Repository for customer id: " + customerNumber + " and Workflow Id: " + workflowId);
                    return false;
                }
            }

            // If non-sold to role then show validation message
            var nonSoldToCustomerRecords = sapPartners.Where(x => !string.IsNullOrEmpty(x.CustomerRole) && x.CustomerRole != PartnerConstants.SapSoldToRole && x.PartnerNumber == customerNumber).ToList();
            if (nonSoldToCustomerRecords != null && nonSoldToCustomerRecords.Any())
            {
                ProcessSapPartnersOperationError("Customer number " + customerNumber + " is not a Sold To account.  Only accounts with the SoldTo role can be partnered with.");
                return false;
            }

            return true;
        }


        private bool AreJdePartnersValid(List<SapPartner> sapPartners, string customerNumber, string workflowId = null)
        {
            if (!string.IsNullOrEmpty(workflowId))
            {
                if (!sapPartners.Any())
                {
                    ProcessSapPartnersOperationError("There are no Partners found in the MDM Repository for customer id: " + customerNumber + " and Workflow Id: " + workflowId);
                    return false;
                }
            }

            // If non-ship to / sold to role then show validation message
            var nonSoldToCustomerRecords = sapPartners.Where(x => !string.IsNullOrEmpty(x.CustomerRole) && (x.CustomerRole.Trim() == "B") && x.PartnerNumber == customerNumber).ToList();
            if (nonSoldToCustomerRecords != null && nonSoldToCustomerRecords.Any())
            {
                ProcessSapPartnersOperationError("Customer number " + customerNumber + " is not a Ship To / Sold To account.  Only accounts with the Ship To / SoldTo role can be partnered with.");
                return false;
            }

            return true;
        }

        private void SetPartnerFlags(List<SapPartner> sapPartners, SystemType systemType)
        {
            if (sapPartners == null)
                return;
            if (systemType == SystemType.JDE)
            {
                foreach (var sapPartner in sapPartners)
                {
                    sapPartner.UnPartner = false;

                    if (int.Parse(sapPartner.PartnerFunctionTypeId) == (int)JdePartnerFuncType.BILLTO)
                    {
                        sapPartner.IsEditable = true;
                    }
                    else
                    {
                        sapPartner.IsDefaultPartner = false;
                        sapPartner.IsEditable = false;
                    }
                }
            }
            else
            {
                foreach (var sapPartner in sapPartners)
                {
                    sapPartner.UnPartner = false;

                    if (sapPartner.PartnerFunctionRole == SapFunctionPartnerType.SP.ToString() ||
                        sapPartner.PartnerFunctionRole == SapFunctionPartnerType.Z1.ToString() ||
                        sapPartner.PartnerFunctionRole == SapFunctionPartnerType.Z2.ToString())
                    {
                        sapPartner.IsDefaultPartner = false;
                        sapPartner.IsEditable = false;
                    }
                    else
                    {
                        sapPartner.IsEditable = true;
                    }
                }
            }
        }

        private void SetSearchSapPartnerOperationResult(SearchSapPartnerResponse searchOlympusPartnerResponse)
        {
            SearchSapPartnerOperationResult.ResultData.MdmNumber = searchOlympusPartnerResponse.MdmNumber;
            SearchSapPartnerOperationResult.ResultData.PartnerCustomerAddress = searchOlympusPartnerResponse.PartnerCustomerAddress;
            SearchSapPartnerOperationResult.ResultData.PartnerCustomerName = searchOlympusPartnerResponse.PartnerCustomerName;
            SearchSapPartnerOperationResult.ResultData.PartnerCustomerRoleTypeId = searchOlympusPartnerResponse.PartnerCustomerRoleTypeId;
        }
        private async Task<bool> AllowEditOfPartners(List<WorkflowPartnersDomain> existingPartners)
        {
            // Find unique partner wf ids and see if any are not completed. If that is the case, return false.
            var distinctWorkflowIds = existingPartners.Where(x => !string.IsNullOrEmpty(x.WorkflowId)).Select(x => x.WorkflowId).Distinct();
            if ((await _workflowRepository.GetWorkflowsAsync(distinctWorkflowIds)).Any(x => x.WorkflowStateTypeId == (int)WorkflowStateType.InProgress || x.WorkflowStateTypeId == (int)WorkflowStateType.New))
                return false;

            return true;
        }
        private async Task<bool> BuildSapPartnersFromWorkflowRequest(SystemType systemType, WorkflowPartnersDomain partner)
        {
            if (!string.IsNullOrEmpty(partner.PartnerOneWorkflowId))
            {
                var workflow = (await _workflowRepository.GetWorkflowsAsync(new string[] { partner.PartnerOneWorkflowId }.ToList())).FirstOrDefault();
                if (workflow == null)
                {
                    ProcessSapPartnerWorkflowDataOperationError("Missing Workflow Data for WorkflowId: " + workflow.WorkflowId);
                    return false;
                }
                var workflowCreateCustomer = await _partnerDataRepository.GetWorkflowCreateCustomer(workflow.WorkflowId, partner.SystemTypeId);
                if (workflowCreateCustomer == null)
                {
                    ProcessSapPartnerWorkflowDataOperationError("Missing Create Customer Workflow Data for WorkflowId: " + workflow.WorkflowId);
                    return false;
                }

                //if (workflow.WorkflowStateTypeId != (int)WorkflowStateType.Completed && workflow.WorkflowStateTypeId != (int)WorkflowStateType.InProgress)
                //{
                //    ProcessSapPartnerWorkflowDataOperationError("Workflow is not in-progress or completion state for WorkflowId: " + workflow.WorkflowId);
                //    return false;
                //}

                if (!string.IsNullOrEmpty(workflowCreateCustomer.SystemRecordId))
                {
                    // Add to result-set the partners coming from the MDM Repository
                    if (systemType == SystemType.JDE)
                    {
                        SapPartnerWorkflowDataOperationResult.ResultData.SapPartners.AddRange(await GetSapPartnersByCustomerNumber(systemType, workflowCreateCustomer.SystemRecordId));
                    }
                    else
                    {
                        SapPartnerWorkflowDataOperationResult.ResultData.SapPartners.AddRange(await GetSapPartnersByCustomerNumber(systemType, workflowCreateCustomer.SystemRecordId, Convert.ToInt32(partner.PartnerOneDivisionTypeId), Convert.ToInt32(partner.PartnerOneDistributionChannelTypeId), Convert.ToInt32(partner.PartnerOneSalesOrgTypeId)));
                    }
                }
                else
                {
                    if (workflow.WorkflowStateTypeId == (int)WorkflowStateType.Completed)
                    {
                        ProcessSapPartnerWorkflowDataOperationError("Workflow is completed but there is no customer number saved for WorkflowId: " + workflow.WorkflowId);
                        return false;
                    }

                    // Find the workflow details in the Workflow Table if in-progress workflow
                    var workflowCreateCustomerData = JsonConvert.DeserializeObject<CustomerDataModel>(workflowCreateCustomer.Data);
                    var sapPartners = MapSapPartnersCustomerDataElements(systemType, workflowCreateCustomerData.CustomerElements, workflowCreateCustomer.WorkflowId);
                    if (sapPartners == null)
                        return false;

                    SapPartnerWorkflowDataOperationResult.ResultData.SapPartners.AddRange(sapPartners);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(partner.PartnerOneCustomerNumber))
                {
                    ProcessSapPartnerWorkflowDataOperationError("Partner one workflow and customer are null for partner wf id ");
                    return false;
                }

                if (systemType == SystemType.JDE)
                {
                    SapPartnerWorkflowDataOperationResult.ResultData.SapPartners.AddRange(await GetSapPartnersByCustomerNumber(systemType, partner.PartnerOneCustomerNumber));
                }
                else
                {
                    SapPartnerWorkflowDataOperationResult.ResultData.SapPartners.AddRange(await GetSapPartnersByCustomerNumber(systemType, partner.PartnerOneCustomerNumber, Convert.ToInt32(partner.PartnerOneDivisionTypeId), Convert.ToInt32(partner.PartnerOneDistributionChannelTypeId), Convert.ToInt32(partner.PartnerOneSalesOrgTypeId)));
                }
            }
            return true;
        }
        private async Task<bool> BuildPartnerEditsForCustomer(SystemType systemType, List<WorkflowPartnersDomain> allPartners)
        {
            foreach (var workflowPartner in allPartners)
            {
                // Add Sap Partners Edits
                var partnerEdit = await GetSapPartnerEdit(systemType, workflowPartner, SapPartnerWorkflowDataOperationResult.ResultData.SapPartners);
                if (partnerEdit != null)
                    SapPartnerWorkflowDataOperationResult.ResultData.PartnerEdits.Add(partnerEdit);
            }
            return true;
        }
        private bool ValidateSapPartnerWorkflowRequest(SearchSapPartnerWorkflowRequest apolloPartnerWorkflowRequest)
        {
            SapPartnerWorkflowDataOperationResult.IsSuccess = true;
            if (string.IsNullOrEmpty(apolloPartnerWorkflowRequest.UserId) || string.IsNullOrEmpty(apolloPartnerWorkflowRequest.WorkflowId))
            {
                var msg = "Invalid Apollo Partners Workflow Request";
                ProcessSapPartnerWorkflowDataOperationError(msg);
                SapPartnerWorkflowDataOperationResult.IsSuccess = false;
            }
            return SapPartnerWorkflowDataOperationResult.IsSuccess;
        }

        private bool ValidateSapPartnersRequest(SystemType systemType, SapPartnersRequest sapPartnersRequest)
        {
            var salesOrgTypes = systemType == SystemType.APOLLO ? StaticTypeData.SalesOrgTypes : StaticTypeData.OlympusSalesOrgTypes;
            var distChannelTypes = systemType == SystemType.APOLLO ? StaticTypeData.ApolloDistributionChannelTypes : StaticTypeData.OlympusDistributionChannelTypes;
            var divisionTypes = systemType == SystemType.APOLLO ? StaticTypeData.ApolloDivisionTypes : StaticTypeData.OlympusDivisionTypes;

            SapPartnersOperationResult.IsSuccess = true;

            if (systemType == SystemType.JDE)
            {
                if (sapPartnersRequest == null ||
                        string.IsNullOrEmpty(sapPartnersRequest.WorkflowOrCustomerNumber) ||
                        string.IsNullOrEmpty(sapPartnersRequest.UserId)
                    )
                {
                    var msg = "Invalid " + systemType.ToString() + " Partners Request";
                    ProcessSapPartnersOperationError(msg);
                    SapPartnersOperationResult.IsSuccess = false;
                }

            }
            else
            {
                if (sapPartnersRequest == null ||
                        string.IsNullOrEmpty(sapPartnersRequest.WorkflowOrCustomerNumber) ||
                        string.IsNullOrEmpty(sapPartnersRequest.UserId) ||

                        string.IsNullOrEmpty(sapPartnersRequest.SalesOrgTypeId) ||
                        !sapPartnersRequest.SalesOrgTypeId.All(char.IsDigit) ||
                        !salesOrgTypes.Exists(x => x.Id == Convert.ToInt32(sapPartnersRequest.SalesOrgTypeId)) ||

                        string.IsNullOrEmpty(sapPartnersRequest.DistributionChannelTypeId) ||
                        !sapPartnersRequest.DistributionChannelTypeId.All(char.IsDigit) ||
                        !distChannelTypes.Exists(x => x.Id == Convert.ToInt32(sapPartnersRequest.DistributionChannelTypeId)) ||

                        string.IsNullOrEmpty(sapPartnersRequest.DivisionTypeId) ||
                        !sapPartnersRequest.DivisionTypeId.All(char.IsDigit) ||
                        !divisionTypes.Exists(x => x.Id == Convert.ToInt32(sapPartnersRequest.DivisionTypeId))
                    )
                {
                    var msg = "Invalid " + systemType.ToString() + " Partners Request";
                    ProcessSapPartnersOperationError(msg);
                    SapPartnersOperationResult.IsSuccess = false;
                }
            }
            return SapPartnersOperationResult.IsSuccess;
        }

        private bool ValidateSearchSapPartnerRequest(SystemType systemType, SapPartnerRequest searchApolloPartnerRequest)
        {
            var salesOrgTypes = systemType == SystemType.APOLLO ? StaticTypeData.SalesOrgTypes : StaticTypeData.OlympusSalesOrgTypes;
            var distChannelTypes = systemType == SystemType.APOLLO ? StaticTypeData.ApolloDistributionChannelTypes : StaticTypeData.OlympusDistributionChannelTypes;
            var divisionTypes = systemType == SystemType.APOLLO ? StaticTypeData.ApolloDivisionTypes : StaticTypeData.OlympusDivisionTypes;

            SearchSapPartnerOperationResult.IsSuccess = true;

            if (systemType == SystemType.JDE)
            {
                if (searchApolloPartnerRequest == null ||
                        string.IsNullOrEmpty(searchApolloPartnerRequest.WorkflowOrCustomerNumber) ||
                        string.IsNullOrEmpty(searchApolloPartnerRequest.UserId)
                    )
                {
                    var msg = "Invalid " + systemType.ToString() + " Partners Request";
                    ProcessSapPartnersOperationError(msg);
                    SapPartnersOperationResult.IsSuccess = false;
                }

            }
            else
            {
                if (searchApolloPartnerRequest == null ||
                        string.IsNullOrEmpty(searchApolloPartnerRequest.WorkflowOrCustomerNumber) ||
                        string.IsNullOrEmpty(searchApolloPartnerRequest.UserId) ||

                        string.IsNullOrEmpty(searchApolloPartnerRequest.SalesOrgTypeId) ||
                        !searchApolloPartnerRequest.SalesOrgTypeId.All(char.IsDigit) ||
                        !salesOrgTypes.Exists(x => x.Id == Convert.ToInt32(searchApolloPartnerRequest.SalesOrgTypeId)) ||

                        string.IsNullOrEmpty(searchApolloPartnerRequest.DistributionChannelTypeId) ||
                        !searchApolloPartnerRequest.DistributionChannelTypeId.All(char.IsDigit) ||
                        !distChannelTypes.Exists(x => x.Id == Convert.ToInt32(searchApolloPartnerRequest.DistributionChannelTypeId)) ||

                        string.IsNullOrEmpty(searchApolloPartnerRequest.DivisionTypeId) ||
                        !searchApolloPartnerRequest.DivisionTypeId.All(char.IsDigit) ||
                        !divisionTypes.Exists(x => x.Id == Convert.ToInt32(searchApolloPartnerRequest.DivisionTypeId))
                    )
                {
                    var msg = "Invalid Search " + systemType.ToString() + " Partners Request";
                    ProcessSearchSapPartnerOperationError(msg);
                    SearchSapPartnerOperationResult.IsSuccess = false;
                }
            }
            return SearchSapPartnerOperationResult.IsSuccess;
        }

        private bool ValidateSearchOlympusPartnerRequest(SapPartnerRequest searchOlympusPartnerRequest)
        {
            SearchSapPartnerOperationResult.IsSuccess = true;
            if (searchOlympusPartnerRequest == null ||
                    string.IsNullOrEmpty(searchOlympusPartnerRequest.WorkflowOrCustomerNumber) ||
                    string.IsNullOrEmpty(searchOlympusPartnerRequest.UserId) ||

                    string.IsNullOrEmpty(searchOlympusPartnerRequest.SalesOrgTypeId) ||
                    !searchOlympusPartnerRequest.SalesOrgTypeId.All(char.IsDigit) ||
                    !StaticTypeData.OlympusSalesOrgTypes.Exists(x => x.Id == Convert.ToInt32(searchOlympusPartnerRequest.SalesOrgTypeId)) ||

                    string.IsNullOrEmpty(searchOlympusPartnerRequest.DistributionChannelTypeId) ||
                    !searchOlympusPartnerRequest.DistributionChannelTypeId.All(char.IsDigit) ||
                    !StaticTypeData.OlympusDistributionChannelTypes.Exists(x => x.Id == Convert.ToInt32(searchOlympusPartnerRequest.DistributionChannelTypeId)) ||

                    string.IsNullOrEmpty(searchOlympusPartnerRequest.DivisionTypeId) ||
                    !searchOlympusPartnerRequest.DivisionTypeId.All(char.IsDigit) ||
                    !StaticTypeData.OlympusDivisionTypes.Exists(x => x.Id == Convert.ToInt32(searchOlympusPartnerRequest.DivisionTypeId))
                )
            {
                var msg = "Invalid Search Apollo Partner Request";
                ProcessSearchSapPartnerOperationError(msg);
                SearchSapPartnerOperationResult.IsSuccess = false;
            }
            return SearchSapPartnerOperationResult.IsSuccess;
        }

        private bool ValidateGetCustomerFiltersRequest(GetCustomerFiltersRequest getCustomerFiltersRequest)
        {
            GetCustomerFiltersResponseOperationResult.IsSuccess = true;
            if (getCustomerFiltersRequest == null ||
                    string.IsNullOrEmpty(getCustomerFiltersRequest.CustomerNumber) ||
                    string.IsNullOrEmpty(getCustomerFiltersRequest.UserId) ||
                    getCustomerFiltersRequest.SystemTypeId <= 0)
            {
                var msg = "Invalid Get Customer Filters Request";
                ProcessGetCustomerFiltersResponseOperationError(msg);
                GetCustomerFiltersResponseOperationResult.IsSuccess = false;
            }
            return GetCustomerFiltersResponseOperationResult.IsSuccess;
        }

        private async Task<SearchSapPartnerResponse> PerformSearchSapPartnerResponse(string customerNumber, string source, int divisionTypeId, int distributionChannelTypeId, int salesOrgTypeId)
        {
            var divisionType = source == SystemType.APOLLO.ToString() ? StaticTypeData.ApolloDivisionTypes.Where(x => x.Id == divisionTypeId).FirstOrDefault() : StaticTypeData.OlympusDivisionTypes.Where(x => x.Id == divisionTypeId).FirstOrDefault();
            var distributionChannelType = source == SystemType.APOLLO.ToString() ? StaticTypeData.ApolloDistributionChannelTypes.Where(x => x.Id == distributionChannelTypeId).FirstOrDefault() : StaticTypeData.OlympusDistributionChannelTypes.Where(x => x.Id == distributionChannelTypeId).FirstOrDefault();
            var salesOrgType = source == SystemType.APOLLO.ToString() ? StaticTypeData.SalesOrgTypes.Where(x => x.Id == salesOrgTypeId).FirstOrDefault() : StaticTypeData.OlympusSalesOrgTypes.Where(x => x.Id == salesOrgTypeId).FirstOrDefault();
            if (source != SystemType.JDE.ToString())
            {
                if (divisionType == null || distributionChannelType == null || salesOrgType == null)
                {
                    ProcessSearchSapPartnerOperationError("Cannot map distribution, division, or sales org type");
                    return null;
                }
            }
            var foundCustomerRecord = source == SystemType.JDE.ToString() ? await _partnerDataRepository.ReadRedShiftByCustomerNumberAndSource(customerNumber, source) : await _partnerDataRepository.ReadMdmRepoCustomerByCustomerDetail(customerNumber, source, salesOrgType.Value, divisionType.Value, distributionChannelType.Value);
            if (foundCustomerRecord == null)
            {
                var customerSearch = source == SystemType.JDE.ToString() ? "Customer number " + customerNumber + " not found for source : JDE" : "Customer " + customerNumber + " has not been created for sales area " + salesOrgType?.Value + " " + distributionChannelType.Value + " " + divisionType.Value; 
                ProcessSearchSapPartnerOperationValidation(customerSearch);
                return null;
            }

            var address = (foundCustomerRecord.street.EmptyIfNull() + " " + foundCustomerRecord.street2.EmptyIfNull() + " " + foundCustomerRecord.city.EmptyIfNull() + " " + foundCustomerRecord.region.EmptyIfNull() + " " + foundCustomerRecord.postal_code.EmptyIfNull() + " " + foundCustomerRecord.country.EmptyIfNull()).Trim().Replace("  ", " ");
            var roleType = source == SystemType.APOLLO.ToString() ? StaticTypeData.ApolloRoleTypes.Where(x => x.Value == foundCustomerRecord.role).FirstOrDefault() : 
                        source == SystemType.OLYMPUS.ToString() ? StaticTypeData.OlympusRoleTypes.Where(x => x.Value == foundCustomerRecord.role).FirstOrDefault() :
                        StaticTypeData.JDERoleTypes.Where(x => x.Value == foundCustomerRecord.role).FirstOrDefault();

            var partnerableRole = source == SystemType.APOLLO.ToString() ? Enum.IsDefined(typeof(ApolloPartnerableRoleType), roleType.Id) : 
                                source == SystemType.OLYMPUS.ToString() ? Enum.IsDefined(typeof(OlympusPartnerableRoleType), roleType.Id) :
                                Enum.IsDefined(typeof(JdePartnerableRoleType), roleType.Id);

            if (!partnerableRole)
            {
                ProcessSearchSapPartnerOperationValidation("Customer does not belong to a role that can be partnered");
                return null;
            }

            return new SearchSapPartnerResponse
            {
                MdmNumber = foundCustomerRecord.globalmdm,
                PartnerCustomerName = foundCustomerRecord.name1,
                WorkflowOrCustomerNumber = customerNumber,
                PartnerCustomerAddress = address,
                PartnerCustomerRoleTypeId = roleType?.Id.ToString()
            };
        }

        private async Task<GetCustomerFiltersResponse> PerformGetCustomerFiltersResponse(string customerNumber, string source)
        {
            var customerRecords = await _partnerDataRepository.ReadRedShiftByCustomerNumberSource(customerNumber, source);
            if (customerRecords == null)
            {
                var customerSearch = "Cannot find record for Customer: " + customerNumber + " with Source: " + source;
                ProcessGetCustomerFiltersResponseOperationValidation(customerSearch);
                return null;
            }
            if (customerRecords.Count <= 0)
            {
                var customerSearch = "Cannot find record for Customer: " + customerNumber + " with Source: " + source;
                ProcessGetCustomerFiltersResponseOperationValidation(customerSearch);
                return null;
            }

            // If non-sold to role then show validation message
            var nonSoldToCustomerRecords = customerRecords.Where(x => !string.IsNullOrEmpty(x.role) && x.role != PartnerConstants.SapSoldToRole).ToList(); 
            if(nonSoldToCustomerRecords != null && nonSoldToCustomerRecords.Any())
            {
                var customerSearch = "Customer number " + customerNumber +  " is not a Sold To account.  Only accounts with the SoldTo role can be partnered with.";
                ProcessGetCustomerFiltersResponseOperationValidation(customerSearch);
                return null;
            }

            return new GetCustomerFiltersResponse
            {
                CustomerNumber = customerNumber,
                SystemTypeId = source,
                SalesOrgs = customerRecords.Select(x => x.sales_org).Distinct().ToList(),
                DistChannels = customerRecords.Select(x => x.distribution_channel).Distinct().ToList(),
                Divisions = customerRecords.Select(x => x.division).Distinct().ToList()
            };
        }

        private async Task<PartnerEdit> GetSapPartnerEdit(SystemType systemType, WorkflowPartnersDomain workflowPartnersDomain, List<SapPartner> apolloPartners)
        {
            var partnerEdit = new PartnerEdit();
            var partnerFunctionType = systemType == SystemType.APOLLO ? StaticTypeData.ApolloPartnerFunctionTypes.Where(x => x.Id == Convert.ToInt32(workflowPartnersDomain.PartnerTypeId)).FirstOrDefault() :
                                    systemType == SystemType.OLYMPUS ? StaticTypeData.OlympusPartnerFunctionTypes.Where(x => x.Id == Convert.ToInt32(workflowPartnersDomain.PartnerTypeId)).FirstOrDefault() :
                                    StaticTypeData.JdePartnerFunctionTypes.Where(x => x.Id == Convert.ToInt32(workflowPartnersDomain.PartnerTypeId)).FirstOrDefault();
            partnerEdit.ID = workflowPartnersDomain.Id;
            partnerEdit.PartnerFunctionTypeId = partnerFunctionType?.Id.ToString();
            partnerEdit.PartnerFunctionRole = partnerFunctionType?.Description;
            partnerEdit.IsDefault = workflowPartnersDomain.IsDefaultPartner;
            partnerEdit.CustomerNumberOrWorkflowNumber = !string.IsNullOrEmpty(workflowPartnersDomain.PartnerTwoCustomerNumber) ? workflowPartnersDomain.PartnerTwoCustomerNumber : workflowPartnersDomain.PartnerTwoWorkflowId;
            if (workflowPartnersDomain.IsDeleteRequest)
            {
                partnerEdit.PartnersOperation = PartnerEditType.Remove.ToString();
                if (!await BuildPartnerEditAddOperation(systemType, workflowPartnersDomain, partnerEdit))
                    return null;
            }
            else
            {
                var existingPartner = apolloPartners.Where(x =>
                    (x.PartnerNumber == workflowPartnersDomain.PartnerTwoCustomerNumber && Convert.ToInt32(x.PartnerFunctionTypeId) == workflowPartnersDomain.PartnerTypeId)
                    ||
                    (x.PartnerNumber == workflowPartnersDomain.PartnerTwoWorkflowId && Convert.ToInt32(x.PartnerFunctionTypeId) == workflowPartnersDomain.PartnerTypeId)
                ).FirstOrDefault();
                if (existingPartner == null || workflowPartnersDomain.IsAddPartnerRequest) // If adding or has been added
                {
                    partnerEdit.PartnersOperation = PartnerEditType.Add.ToString();
                    if (!await BuildPartnerEditAddOperation(systemType, workflowPartnersDomain, partnerEdit))
                        return null;
                }
                else if (!workflowPartnersDomain.IsAddPartnerRequest) // If editing or has been edited
                {
                    partnerEdit.PartnersOperation = PartnerEditType.Edit.ToString();
                }
                else
                {
                    ProcessSapPartnerWorkflowDataOperationError("Cannot determine Partner Operation for Partner Workflow ID: " + workflowPartnersDomain.WorkflowId);
                    return null;
                }
            }
            return partnerEdit;
        }
        private async Task<bool> BuildPartnerEditAddOperation(SystemType systemType, WorkflowPartnersDomain workflowPartnersDomain, PartnerEdit partnerEdit)
        {

            if (!string.IsNullOrEmpty(workflowPartnersDomain.PartnerTwoCustomerNumber))
            {
                partnerEdit.SearchedSapPartner = await PerformSearchSapPartnerResponse(
                    workflowPartnersDomain.PartnerTwoCustomerNumber,
                    systemType.ToString(),
                    workflowPartnersDomain.PartnerOneDivisionTypeId.Value,
                    workflowPartnersDomain.PartnerOneDistributionChannelTypeId.Value,
                    workflowPartnersDomain.PartnerOneSalesOrgTypeId.Value);
            }
            else if (!string.IsNullOrEmpty(workflowPartnersDomain.PartnerTwoWorkflowId))
            {
                var workflow = (await _workflowRepository.GetWorkflowsAsync(new string[] { workflowPartnersDomain.PartnerTwoWorkflowId }.ToList())).FirstOrDefault();
                if (workflow == null)
                {
                    ProcessSapPartnersOperationError("Missing Workflow Data for WorkflowId: " + workflow.WorkflowId);
                    return false;
                }
                var workflowCreateCustomer = await _partnerDataRepository.GetWorkflowCreateCustomer(workflow.WorkflowId, (int)systemType);
                if (workflowCreateCustomer == null)
                {
                    ProcessSapPartnersOperationError("Missing Create Customer Workflow Data for WorkflowId: " + workflow.WorkflowId);
                    return false;
                }

                if (workflow.WorkflowStateTypeId == (int)WorkflowStateType.Completed)
                {
                    if (!string.IsNullOrEmpty(workflowCreateCustomer.SystemRecordId))
                    {
                        partnerEdit.SearchedSapPartner = await PerformSearchSapPartnerResponse(
                                workflowPartnersDomain.PartnerTwoCustomerNumber,
                                systemType.ToString(),
                                workflowPartnersDomain.PartnerOneDivisionTypeId.Value,
                                workflowPartnersDomain.PartnerOneDistributionChannelTypeId.Value,
                                workflowPartnersDomain.PartnerOneSalesOrgTypeId.Value);
                    }
                    else
                    {
                        ProcessSapPartnerWorkflowDataOperationError("Workflow is completed but there is no customer number saved for WorkflowId: " + workflow.WorkflowId);
                        return false;
                    }
                }
                else //if (workflow.WorkflowStateTypeId == (int)WorkflowStateType.InProgress)
                {
                    var workflowCreateCustomerData = JsonConvert.DeserializeObject<CustomerDataModel>(workflowCreateCustomer.Data);
                    var searchSapPartnerResponse = PerformWorkflowSearchSapPartnerResponse(workflowCreateCustomerData.CustomerElements,
                        systemType.ToString(),
                        workflowPartnersDomain.PartnerOneDivisionTypeId.Value,
                        workflowPartnersDomain.PartnerOneDistributionChannelTypeId.Value,
                        workflowPartnersDomain.PartnerOneSalesOrgTypeId.Value,
                        workflow.WorkflowId);

                    if (searchSapPartnerResponse == null)
                    {
                        var salesOrgTypes = systemType == SystemType.APOLLO ? StaticTypeData.SalesOrgTypes : StaticTypeData.OlympusSalesOrgTypes;
                        var distChannelTypes = systemType == SystemType.APOLLO ? StaticTypeData.ApolloDistributionChannelTypes : StaticTypeData.OlympusDistributionChannelTypes;
                        var divisionTypes = systemType == SystemType.APOLLO ? StaticTypeData.ApolloDivisionTypes : StaticTypeData.OlympusDivisionTypes;

                        var divisionType = divisionTypes.Where(x => x.Id == Convert.ToInt32(workflowPartnersDomain.PartnerOneDivisionTypeId.Value)).FirstOrDefault();
                        var distributionChannelType = distChannelTypes.Where(x => x.Id == Convert.ToInt32(workflowPartnersDomain.PartnerOneDistributionChannelTypeId.Value)).FirstOrDefault();
                        var salesOrgType = salesOrgTypes.Where(x => x.Id == Convert.ToInt32(workflowPartnersDomain.PartnerOneSalesOrgTypeId.Value)).FirstOrDefault();

                        ProcessSapPartnerWorkflowDataOperationError("Workflow " + workflow.WorkflowId + " has not been created for sales area " + salesOrgType.Value + " " + distributionChannelType.Value + " " + divisionType.Value);

                        return false;
                    }
                    partnerEdit.SearchedSapPartner = searchSapPartnerResponse;
                }
                //else
                //{
                //    ProcessSapPartnerWorkflowDataOperationError("Workflow is not in-progress or completion state for WorkflowId: " + workflow.WorkflowId);
                //    return false;
                //}
            }

            return true;
        }

        private async Task<List<SapPartner>> GetSapPartnersByCustomerNumber(SystemType systemType, string customerNumber, int divisionTypeId = 0, int distributionChannelTypeId = 0, int salesOrgTypeId = 0)
        {
            var divisionType = systemType == SystemType.APOLLO ? StaticTypeData.ApolloDivisionTypes.Where(x => x.Id == divisionTypeId).FirstOrDefault() : StaticTypeData.OlympusDivisionTypes.Where(x => x.Id == divisionTypeId).FirstOrDefault();
            var distributionChannelType = systemType == SystemType.APOLLO ? StaticTypeData.ApolloDistributionChannelTypes.Where(x => x.Id == distributionChannelTypeId).FirstOrDefault() : StaticTypeData.OlympusDistributionChannelTypes.Where(x => x.Id == distributionChannelTypeId).FirstOrDefault();
            var salesOrgType = systemType == SystemType.APOLLO ? StaticTypeData.SalesOrgTypes.Where(x => x.Id == salesOrgTypeId).FirstOrDefault() : StaticTypeData.OlympusSalesOrgTypes.Where(x => x.Id == salesOrgTypeId).FirstOrDefault();
            if (systemType != SystemType.JDE) { 
                if (divisionType == null || distributionChannelType == null || salesOrgType == null)
                {
                    ProcessSapPartnersOperationError("Cannot map distribution, division, or sales org type");
                    return null;
                } 
            }
            var mdmSapPartners = systemType == SystemType.JDE ? await _partnerDataRepository.ReadJdePartners(systemType.ToString(), customerNumber) : await _partnerDataRepository.ReadSapPartners(systemType.ToString(), customerNumber, salesOrgType.Value, divisionType.Value, distributionChannelType.Value);
            return MapSapPartners(systemType, mdmSapPartners, customerNumber);
        }


        private SapPartner GetNewSapPartner(SapPartner defaultPartner, StaticDataTypeDomain partnerFunctionType)
        {
            var partnerNumber = defaultPartner.PartnerNumber;
            var partnerCustomerName = defaultPartner.PartnerCustomerName;
            var partnerCustomerAddress = defaultPartner.PartnerCustomerAddress;

            // Parcel
            if (partnerFunctionType.Value == SapFunctionPartnerType.Z1.ToString())
            {
                partnerNumber = PartnerConstants.ParcelPartnerNumber;
                partnerCustomerName = PartnerConstants.ParcelPartnerName;
                partnerCustomerAddress = string.Empty;
            }
            else if (partnerFunctionType.Value == SapFunctionPartnerType.Z2.ToString()) // LTL
            {
                partnerNumber = PartnerConstants.LTLPartnerNumber;
                partnerCustomerName = PartnerConstants.LTLPartnerName;
                partnerCustomerAddress = string.Empty;
            }

            return new SapPartner()
            {
                PartnerFunctionRole = partnerFunctionType.Description,
                PartnerFunctionTypeId = partnerFunctionType.Id.ToString(),
                MdmNumber = defaultPartner.MdmNumber,
                IsDefaultPartner = defaultPartner.IsDefaultPartner,
                PartnerCustomerAddress = partnerCustomerAddress,
                PartnerCustomerName = partnerCustomerName,
                PartnerNumber = partnerNumber,
                WorkflowOrCustomerNumber = defaultPartner.WorkflowOrCustomerNumber,
                CustomerRoleTypeId = defaultPartner.CustomerRoleTypeId
            };
        }

        private SearchSapPartnerResponse PerformWorkflowSearchSapPartnerResponse(Dictionary<string, object> customerElements, string source, int divisionTypeId, int distributionChannelTypeId, int salesOrgTypeId, string workflowId)
        {
            if (customerElements == null || customerElements.Count == 0)
                return null;

            var salesOrgTypeIdFromElements = Convert.ToInt32(customerElements.GetValue("SalesOrgTypeId"));
            var distributionChannelTypeIdFromElements = Convert.ToInt32(customerElements.GetValue("DistributionChannelTypeId"));
            var divisionTypeIdFromElements = Convert.ToInt32(customerElements.GetValue("DivisionTypeId"));
            if (source != SystemType.JDE.ToString())
            {
                if (salesOrgTypeIdFromElements != salesOrgTypeId || distributionChannelTypeIdFromElements != distributionChannelTypeId || divisionTypeIdFromElements != divisionTypeId)
                {
                    return null;
                }
            }
            var street = customerElements.GetValue("Street");
            var city = customerElements.GetValue("City");
            var region = customerElements.GetValue("Region");
            var country = customerElements.GetValue("Country");
            var postalCode = customerElements.GetValue("PostalCode");
            var name = customerElements.GetValue("Name1");
            var address = street + " " + city + " " + region + " " + postalCode + " " + country;
            var roleTypeId = customerElements.GetValue("RoleTypeId");
            
            var partnerableRole = source == SystemType.APOLLO.ToString() ? Enum.IsDefined(typeof(ApolloPartnerableRoleType), Convert.ToInt32(roleTypeId)) :
                                source == SystemType.OLYMPUS.ToString() ? Enum.IsDefined(typeof(OlympusPartnerableRoleType), Convert.ToInt32(roleTypeId)) :
                                Enum.IsDefined(typeof(JdePartnerableRoleType), Convert.ToInt32(roleTypeId));

            if (!partnerableRole)
            {
                ProcessSearchSapPartnerOperationValidation("Workflow does not belong to a role that can be partnered");
                return null;
            }

            return new SearchSapPartnerResponse
            {
                MdmNumber = string.Empty,
                PartnerCustomerAddress = address,
                PartnerCustomerName = name,
                WorkflowOrCustomerNumber = workflowId,
                PartnerCustomerRoleTypeId = roleTypeId
            };
        }

        private List<SapPartner> MapSapPartnersCustomerDataElements(SystemType systemType, Dictionary<string, object> customerElements, string workflowOrCustomerNumber)
        {
            var sapPartners = new List<SapPartner>();
            if (customerElements == null || customerElements.Count == 0)
                return sapPartners;

            var roleTypeId = customerElements.GetValue("RoleTypeId");
            if (string.IsNullOrEmpty(roleTypeId))
                return sapPartners;

            var street = customerElements.GetValue("Street");
            var city = customerElements.GetValue("City");
            var region = customerElements.GetValue("Region");
            var country = customerElements.GetValue("Country");
            var postalCode = customerElements.GetValue("PostalCode");
            var name = customerElements.GetValue("Name1");
            var address = street + " " + city + " " + region + " " + postalCode + " " + country;

            var defaultPartner = new SapPartner
            {
                IsDefaultPartner = false,
                MdmNumber = string.Empty,
                PartnerCustomerAddress = address.Trim().Replace("  ", " "),
                PartnerCustomerName = name,
                PartnerNumber = workflowOrCustomerNumber,
                CustomerRoleTypeId = roleTypeId,
                WorkflowOrCustomerNumber = workflowOrCustomerNumber
            };

            var partnerFunctionTypes = systemType == SystemType.APOLLO ? StaticTypeData.ApolloPartnerFunctionTypes :
                                    systemType == SystemType.OLYMPUS ? StaticTypeData.OlympusPartnerFunctionTypes :
                                    systemType == SystemType.JDE ? StaticTypeData.JdePartnerFunctionTypes : null;

            if (systemType == SystemType.JDE)
            {
                if (int.Parse(roleTypeId) == 1) // Sold To
                {
                    var soldToType = partnerFunctionTypes.Where(x => x.Value == JdePartnerFuncType.SOLDTO.ToString()).FirstOrDefault();
                    sapPartners.Add(GetNewSapPartner(defaultPartner, soldToType));
                }
                else if (int.Parse(roleTypeId) == 3) // Ship To
                {
                    var shipToType = partnerFunctionTypes.Where(x => x.Value == JdePartnerFuncType.SHIPTO.ToString()).FirstOrDefault();
                    sapPartners.Add(GetNewSapPartner(defaultPartner, shipToType));
                }
            }
            else
            {
                // Sold To
                var soldToType = partnerFunctionTypes.Where(x => x.Value == SapFunctionPartnerType.SP.ToString()).FirstOrDefault();
                sapPartners.Add(GetNewSapPartner(defaultPartner, soldToType));

                // Ship To
                var shipToType = partnerFunctionTypes.Where(x => x.Value == SapFunctionPartnerType.SH.ToString()).FirstOrDefault();
                sapPartners.Add(GetNewSapPartner(defaultPartner, shipToType));

                // Bill To
                var billToType = partnerFunctionTypes.Where(x => x.Value == SapFunctionPartnerType.BP.ToString()).FirstOrDefault();
                sapPartners.Add(GetNewSapPartner(defaultPartner, billToType));

                // Payer
                var payerType = partnerFunctionTypes.Where(x => x.Value == SapFunctionPartnerType.PY.ToString()).FirstOrDefault();
                sapPartners.Add(GetNewSapPartner(defaultPartner, payerType));

                if (systemType == SystemType.APOLLO)
                {
                    // Parcel
                    var parcelType = StaticTypeData.ApolloPartnerFunctionTypes.Where(x => x.Value == SapFunctionPartnerType.Z1.ToString()).FirstOrDefault();
                    sapPartners.Add(GetNewSapPartner(defaultPartner, parcelType));

                    // LTL
                    var ltlType = StaticTypeData.ApolloPartnerFunctionTypes.Where(x => x.Value == SapFunctionPartnerType.Z2.ToString()).FirstOrDefault();
                    sapPartners.Add(GetNewSapPartner(defaultPartner, ltlType));
                }
            }
            return sapPartners;
        }

        private List<SapPartner> MapSapPartners(SystemType systemType, List<RedshiftSapPartner> mdmSapPartners, string workflowOrCustomerNumber)
        {
            var sapPartners = new List<SapPartner>();

            foreach (var mdmSapPartner in mdmSapPartners)
            {

                var partnerFunctionType = systemType == SystemType.APOLLO ? StaticTypeData.ApolloPartnerFunctionTypes.Where(x => x.Value == mdmSapPartner.PartnerFunctionRole).FirstOrDefault() :
                                        systemType == SystemType.OLYMPUS ? StaticTypeData.OlympusPartnerFunctionTypes.Where(x => x.Value == mdmSapPartner.PartnerFunctionRole).FirstOrDefault() :
                                        systemType == SystemType.JDE ? StaticTypeData.JdePartnerFunctionTypes.Where(x => x.Value == mdmSapPartner.PartnerFunctionRole).FirstOrDefault() :
                                        null;

                var customerRoleType = systemType == SystemType.APOLLO ? StaticTypeData.ApolloRoleTypes.Where(x => x.Value == mdmSapPartner.CustomerRole).FirstOrDefault() :
                                        systemType == SystemType.OLYMPUS ? StaticTypeData.OlympusRoleTypes.Where(x => x.Value == mdmSapPartner.CustomerRole).FirstOrDefault() :
                                        systemType == SystemType.JDE ? StaticTypeData.JDERoleTypes.Where(x => x.Value == mdmSapPartner.CustomerRole).FirstOrDefault() :
                                        null;
                if (partnerFunctionType == null)
                    continue;

                sapPartners.Add(new SapPartner
                {
                    IsDefaultPartner = mdmSapPartner.IsDefaultPartner.HasValue && mdmSapPartner.IsDefaultPartner.Value,
                    MdmNumber = mdmSapPartner.MdmNumber,
                    PartnerCustomerName = mdmSapPartner.PartnerCustomerName,
                    PartnerCustomerAddress = (mdmSapPartner.Street.EmptyIfNull() + " " + mdmSapPartner.Street2.EmptyIfNull() + " " + mdmSapPartner.City.EmptyIfNull() + " " + mdmSapPartner.Region.EmptyIfNull() + " " + mdmSapPartner.Postal.EmptyIfNull() + " " + mdmSapPartner.Country.EmptyIfNull()).Trim().Replace("  ", " "),
                    PartnerFunctionTypeId = partnerFunctionType?.Id.ToString(),
                    PartnerNumber = mdmSapPartner.PartnerNumber,
                    WorkflowOrCustomerNumber = workflowOrCustomerNumber,
                    PartnerFunctionRole = partnerFunctionType.Description,
                    SortOrder = partnerFunctionType.SortOrder,
                    CustomerRole = mdmSapPartner.CustomerRole,
                    CustomerRoleTypeId = customerRoleType?.Id.ToString()
                });
            }

            sapPartners = sapPartners.OrderBy(x => x.SortOrder).ToList();

            if (systemType == SystemType.APOLLO)
            {
                // Parcel
                var parcelType = StaticTypeData.ApolloPartnerFunctionTypes.Where(x => x.Value == SapFunctionPartnerType.Z1.ToString()).FirstOrDefault();
                if (!sapPartners.Exists(x => Convert.ToInt32(x.PartnerFunctionTypeId) == parcelType.Id))
                {
                    var soldToPartner = sapPartners.Where(x => Convert.ToInt32(x.PartnerFunctionTypeId) == (int)SapFunctionPartnerType.SP).FirstOrDefault();
                    if (soldToPartner != null)
                        sapPartners.Add(GetNewSapPartner(soldToPartner, parcelType));
                }

                // LTL
                var ltlType = StaticTypeData.ApolloPartnerFunctionTypes.Where(x => x.Value == SapFunctionPartnerType.Z2.ToString()).FirstOrDefault();
                if (!sapPartners.Exists(x => Convert.ToInt32(x.PartnerFunctionTypeId) == ltlType.Id))
                {
                    var soldToPartner = sapPartners.Where(x => Convert.ToInt32(x.PartnerFunctionTypeId) == (int)SapFunctionPartnerType.SP).FirstOrDefault();
                    if (soldToPartner != null)
                        sapPartners.Add(GetNewSapPartner(soldToPartner, ltlType));
                }
            }
            return sapPartners;
        }

        private PartnerDataResult ConvertToPartnerDataResult(WorkflowCreateCustomerDomain workflowCreateCustomerDomain, string customerNumberOrWorkflowNumber)
        {
            if (string.IsNullOrEmpty(workflowCreateCustomerDomain.Data))
                return new PartnerDataResult { BillToCustomerNumber = customerNumberOrWorkflowNumber };

            var customerData = JsonConvert.DeserializeObject<CustomerDataModel>(workflowCreateCustomerDomain.Data);
            var partFunction = workflowCreateCustomerDomain.SystemTypeId == (int)SystemType.Pointman ?
                    ((PointmanRoleType)Convert.ToInt32(customerData.CustomerElements["RoleTypeId"])).ToString().ToUpper() :
                    string.Empty;

            var partnerDataResult =
              new PartnerDataResult
              {
                  CustomerName = customerData.CustomerElements.GetValue("Name1"),
                  Address =
                    (
                    customerData.CustomerElements.GetValue("Street") + " " +
                    customerData.CustomerElements.GetValue("City") + " " +
                    customerData.CustomerElements.GetValue("Region") + " " +
                    customerData.CustomerElements.GetValue("PostalCode") + " " +
                    customerData.CustomerElements.GetValue("Country") + " ").Trim(),
                  AssignedMdm = string.Empty,
                  BillToCustomerNumber = customerNumberOrWorkflowNumber,
                  ShipToCustomerNumber = workflowCreateCustomerDomain.WorkflowId,
                  PartnerFunc = partFunction
              };
            partnerDataResult.Address = !string.IsNullOrEmpty(partnerDataResult.Address) ? Regex.Replace(partnerDataResult.Address, @"\s+", " ") : string.Empty;
            return partnerDataResult;
        }

        private void ValidateRequest(GetPartnerDataRequest partnerRequest)
        {
            if (partnerRequest == null ||
                partnerRequest.SystemTypeId == 0 ||
                (string.IsNullOrEmpty(partnerRequest.CustomerNumber) &&
                string.IsNullOrEmpty(partnerRequest.WorkflowId) &&
                string.IsNullOrEmpty(partnerRequest.MdmNumber))
                )
            {
                var msg = "Invalid partnerRequest";
                ProcessOperationError(msg);
                OperationResult.IsSuccess = false;
            }
            OperationResult.IsSuccess = true;
        }

        private void ValidatePointmanRequest(GetPartnerDataRequest partnerRequest)
        {
            if (partnerRequest == null ||
                partnerRequest.SystemTypeId == 0 ||
                (string.IsNullOrEmpty(partnerRequest.CustomerNumber) &&
                string.IsNullOrEmpty(partnerRequest.WorkflowId))
                )
            {
                var msg = "Invalid Pointman PartnerRequest";
                ProcessOperationError(msg);
                OperationResult.IsSuccess = false;
            }
            OperationResult.IsSuccess = true;
        }

        private async Task<WorkflowDetails> GetWorkflowDetails(GetPartnerDataRequest getPartnerDataRequest, SystemType systemType)
        {
            var workflowDetails = new WorkflowDetails();

            var workflow = await _partnerDataRepository.GetWorkflow(getPartnerDataRequest.WorkflowId);

            if (workflow == null ||
                workflow.WorkflowStateTypeId == (int)WorkflowStateType.Rejected ||
                workflow.WorkflowStateTypeId == (int)WorkflowStateType.Withdrawn)
            {
                var msg = "Unable to retrieve active workflow details";
                ProcessOperationError(msg);
                OperationResult.IsSuccess = false;
                return null;
            }

            var workflowType = await _partnerDataRepository.GetWorkflowTypeAsync(workflow.WorkflowTypeId);

            if (workflowType == null || string.IsNullOrEmpty(workflowType.Value))
            {
                var msg = "Unable to retrieve workflow details";
                ProcessOperationError(msg);
                OperationResult.IsSuccess = false;
                return null;
            }

            if (workflowType.Value.ToLower().StartsWith("create"))
            {
                var createCustomer = await _partnerDataRepository.GetWorkflowCreateCustomer(getPartnerDataRequest.WorkflowId, (int)systemType);
                workflowDetails.MdmNumber = createCustomer?.MdmCustomerId;
                workflowDetails.CustomerNumber = createCustomer?.SystemRecordId;
                return workflowDetails;
            }
            else if (workflowType.Value.ToLower().StartsWith("update"))
            {
                var updateCustomer = await _partnerDataRepository.GetUpdatesWorkflow(getPartnerDataRequest.WorkflowId, (int)systemType);
                workflowDetails.MdmNumber = updateCustomer?.MdmCustomerId;
                workflowDetails.CustomerNumber = updateCustomer?.SystemRecordId;
                return workflowDetails;
            }
            else if (workflowType.Value.ToLower().StartsWith("extendtonewsalesorg"))
            {
                var extendToNewSalesOrgCustomer = await _partnerDataRepository.GetExtendToSalesOrgWorkflow(getPartnerDataRequest.WorkflowId, (int)systemType);
                workflowDetails.MdmNumber = extendToNewSalesOrgCustomer?.MdmCustomerId;
                workflowDetails.CustomerNumber = extendToNewSalesOrgCustomer?.SystemRecordId;
                return workflowDetails;
            }
            else if (workflowType.Value.ToLower().StartsWith("block"))
            {
                var blockUnblockCustomer = await _partnerDataRepository.GetBlockUnblockWorkflow(getPartnerDataRequest.WorkflowId, (int)systemType);
                workflowDetails.MdmNumber = blockUnblockCustomer?.MdmCustomerId;
                workflowDetails.CustomerNumber = blockUnblockCustomer?.SystemRecordId;
                return workflowDetails;
            }

            return null;
        }

        #endregion
    }
}
