﻿namespace Mdm.Services.Models
{
    public class WorkflowDetails
    {
        public string CustomerNumber { get; set; }
        public string MdmNumber { get; set; }
    }
}
