﻿

using System;

namespace Mdm.Services.Models
{
    public class SapPartnerRequest
    {
        private string _workflowOrCustomerNumber;
        public string RawWorkflowOrCustomerNumber
        {
            get { return _workflowOrCustomerNumber; }
        }

        public string WorkflowOrCustomerNumber
        {
            get
            {
                if (string.IsNullOrEmpty(_workflowOrCustomerNumber) || _workflowOrCustomerNumber.ToLower().StartsWith("wf"))
                    return _workflowOrCustomerNumber;
                return _workflowOrCustomerNumber.PadLeft(10, Convert.ToChar("0"));
            }
            set
            {
                _workflowOrCustomerNumber = value;
            }
        }

        public string UserId { get; set; }
        public string DistributionChannelTypeId { get; set; }
        public string DivisionTypeId { get; set; }
        public string SalesOrgTypeId { get; set; }
    }
}
