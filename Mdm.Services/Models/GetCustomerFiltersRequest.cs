﻿
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mdm.Services.Models
{
    public class GetCustomerFiltersRequest
    {
        public string CustomerNumber { get; set; }
        public int SystemTypeId { get; set; }
        public string UserId { get; set; }

    }
}
