﻿namespace Mdm.Services.Models
{
    public class Delta
    {
        public string Name { get; set; }
        public string OriginalValue { get; set; }
        public string UpdatedValue { get; set; }
    }
}
