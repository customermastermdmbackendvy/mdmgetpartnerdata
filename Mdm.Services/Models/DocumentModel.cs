﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mdm.Services.Models
{
    public class DocumentModel
    {
        public int DocumentTypeId { get; set; }
        public string DocumentName { get; set; }
        public string S3objectKey { get; set; }
        public string AttachmentName { get; set; }
    }
}
