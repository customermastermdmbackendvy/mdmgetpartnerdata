﻿namespace Mdm.Services.Models
{
    public partial class GlobalCustomerModel
    {
        #region Properties
        public string MdmNumber { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }

        public string Street2 { get; set; }


        public string City { get; set; }

        public string Region { get; set; }

        public string PostalCode { get; set; }

        public string Country { get; set; }

        public string Telephone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string Taxnumber { get; set; }

        public string VatRegNo { get; set; }

        public string SicCode4 { get; set; }

        public string SicCode6 { get; set; }

        public string SicCode8 { get; set; }

        public string NaicsCode { get; set; }

        public string DunsNumber { get; set; }
        public string TaxJurisdiction { get; set; }

        public int? CategoryTypeId { get; set; }
        #endregion
    }
}
