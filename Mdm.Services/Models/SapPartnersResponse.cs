﻿
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mdm.Services.Models
{
    public class SapPartner
    {
        public string WorkflowOrCustomerNumber { get; set; }
        public string PartnerFunctionRole { get; set; }
        public string PartnerFunctionTypeId { get; set; }
        public string MdmNumber { get; set; }
        public string PartnerCustomerName { get; set; }
        public string PartnerCustomerAddress { get; set; }
        public bool IsDefaultPartner { get; set; }
        public bool IsEditable { get; set; }
        public string PartnerNumber { get; set; }
        public string PartnersOperation { get { return string.Empty; } }
        public bool UnPartner { get; set; }
        public string CustomerRole { get; set; }
        public string CustomerRoleTypeId { get; set; }

        [JsonIgnore]
        public int? SortOrder { get; set; }
    }
    public class SapPartnersResponse
    {
        public SapPartnersResponse()
        {
            SapPartners = new List<SapPartner>();
            AllowSapPartnerEdit = true;
        }

        public string WorkflowOrCustomerNumber { get; set; }
        public bool AllowSapPartnerEdit { get; set; }
        public List<SapPartner> SapPartners { get; set; }

    }
}
