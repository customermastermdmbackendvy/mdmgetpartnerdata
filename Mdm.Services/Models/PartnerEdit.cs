﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mdm.Services.Models
{
    public class PartnerEdit
    {
        public int ID { get; set; }
        public string PartnersOperation { get; set; }
        public string CustomerNumberOrWorkflowNumber { get; set; }
        public string PartnerFunctionTypeId { get; set; }
        public string PartnerFunctionRole { get; set; }
        public bool IsDefault { get; set; }
        public SearchSapPartnerResponse SearchedSapPartner { get; set; }

    }
}
