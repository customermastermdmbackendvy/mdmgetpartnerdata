﻿using System.Linq;
using System.Text;

namespace Mdm.Services.Models
{
    public class PartnerDataResult
    {
        public string AssignedMdm { get; set; }
        public string BillToCustomerNumber { get; set; }
        public string ShipToCustomerNumber { get; set; }
        public string PartnerFunc { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
    }
}
