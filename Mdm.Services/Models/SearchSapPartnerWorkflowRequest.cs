﻿

namespace Mdm.Services.Models
{
    public class SearchSapPartnerWorkflowRequest
    {
        public string WorkflowId { get; set; }
        public string UserId { get; set; }
    }
}
