﻿using Mdm.Services.Helpers;
using Mdm.Services.Interfaces;
using Newtonsoft.Json;

namespace Mdm.Services.Models
{
    public partial class SapOlympusCustomerModel : ICustomerStaticDataModel
    {

        #region Properties
        public SystemType SystemType { get; set; }
        public string Title { get; set; }

        public string Name1 { get; set; }


        public string Name2 { get; set; }


        public string Name3 { get; set; }


        public string Name4 { get; set; }


        public string Street { get; set; }


        public string Street2 { get; set; }


        public string City { get; set; }

        public string Region { get; set; }

        public string PostalCode { get; set; }

        public string Country { get; set; }

        public string Telephone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string Taxnumber { get; set; }

        public string VatRegNo { get; set; }

        public string SicCode4 { get; set; }

        public string SicCode6 { get; set; }

        public string SicCode8 { get; set; }

        public string NaicsCode { get; set; }

        public string DunsNumber { get; set; }

        [JsonIgnore]
        public string SpecialPriceType { get; set; }

        [JsonIgnore]
        public string DistPriceType { get; set; }

        [JsonIgnore]
        public string RoleType { get; set; }

        [JsonIgnore]
        public string SalesOrgType { get; set; }

        public string License { get; set; }

        public string LicenseExpDate { get; set; }

        public string SearchTerm1 { get; set; }

        public string SearchTerm2 { get; set; }

        [JsonIgnore]
        public string CustomerClassType { get; set; }

        [JsonIgnore]
        public string IndustryCodeType { get; set; }

        [JsonIgnore]
        public string CompanyCodeType { get; set; }

        [JsonIgnore]
        public string DistributionChannelType { get; set; }

        [JsonIgnore]
        public string DivisionType { get; set; }

        public string TransporationZone { get; set; }

        [JsonIgnore]
        public string IndustryType { get; set; }

        public string MarketingSegmentation { get; set; }

        public string TaxNumber2 { get; set; }

        [JsonIgnore]
        public string ReconAccountType { get; set; }

        public string SortKey { get; set; }

        public string PaymentHistoryRecord { get; set; }
        public string PaymentMethods { get; set; }

        public string AcctgClerk { get; set; }

        public string AccountStatement { get; set; }

        [JsonIgnore]
        public string SalesOfficeType { get; set; }

        [JsonIgnore]
        public string CustomerGroupType { get; set; }

        [JsonIgnore]
        public string PPCustProcType { get; set; }

        [JsonIgnore]
        public string CustomerPriceProcType { get; set; }

        [JsonIgnore]
        public string PriceListType { get; set; }

        [JsonIgnore]
        public string DeliveryPriorityType { get; set; }

        [JsonIgnore]
        public string ShippingConditionsType { get; set; }

        public string OrderCombination { get; set; }

        [JsonIgnore]
        public string IncoTerms1Type { get; set; }

        public string IncoTerms2 { get; set; }

        [JsonIgnore]
        public string AcctAssignmentGroupType { get; set; }

        public string TaxClassification { get; set; }

        [JsonIgnore]
        public string AccountType { get; set; }
        public string TaxJurisdiction { get; set; }

        [JsonIgnore]
        public string ShippingCustomerType { get; set; }

        [JsonIgnore]
        public string PaymentTermsType { get; set; }
        public string CreditLimit { get; set; }

        [JsonIgnore]
        public string RiskCategoryType { get; set; }

        [JsonIgnore]
        public string CreditRepGroupType { get; set; }

        public string CredInfoNumber { get; set; }

        public string LastExtReview { get; set; }

        public string Rating { get; set; }

        public string PaymentIndex { get; set; }

        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }

        public string ContactPhone { get; set; }

        public string ContactFax { get; set; }

        public string ContactEmail { get; set; }

        [JsonIgnore]
        public string Type { get; set; }

        [JsonIgnore]
        public string Message { get; set; }

        #region Static Type Id Properties
        public int RoleTypeId { get; set; }
        public int SalesOrgTypeId { get; set; }
        public int CustomerClassTypeId { get; set; }
        public int IndustryCodeTypeId { get; set; }
        public int CompanyCodeTypeId { get; set; }
        //public int IndustryTypeId { get; set; }
        public int MarketingSegmentationTypeId { get; set; }
        public int ReconAccountTypeId { get; set; }
        public int SalesOfficeTypeId { get; set; }
        public int CustomerGroupTypeId { get; set; }
        public int PPCustProcTypeId { get; set; }
        public int CustomerPriceProcTypeId { get; set; }
        public int PriceListTypeId { get; set; }
        public int DeliveryPriorityTypeId { get; set; }
        public int IncoTerms1TypeId { get; set; }
        public int AcctAssignmentGroupTypeId { get; set; }
        public int AccountTypeId { get; set; }
        public int ShippingCustomerTypeId { get; set; }
        public int PaymentTermsTypeId { get; set; }
        public int CreditRepGroupTypeId { get; set; }
        public int RiskCategoryTypeId { get; set; }
        public int ShippingConditionsTypeId { get; set; }

        public int DivisionTypeId { get; set; }
        public int DistributionChannelTypeId { get; set; }
        public int SpecialPricingTypeId { get; set; }

        [JsonProperty("DistLevelTypeId")]
        public int DistPriceTypeId { get; set; }

        [JsonIgnore]
        public string DAPAPriceType { get; set; }
        [JsonIgnore]
        public string DAPA2PriceType { get; set; }
        [JsonIgnore]
        public string FSSPriceType { get; set; }
        public int DAPAPricingTypeId { get; set; }
        public int DAPA2PricingTypeId { get; set; }
        public int FSSPricingTypeId { get; set; }
        #endregion

        #endregion


        [JsonIgnore]
        public string BillingBlock_AllSalesArea { get; set; }
        [JsonIgnore]
        public string BillingBlock_SalectedSalesArea { get; set; }
        [JsonIgnore]
        public string DeliveryBlock_AllSalesArea { get; set; }
        [JsonIgnore]
        public string DeliveryBlock_SalectedSalesArea { get; set; }
        [JsonIgnore]
        public string OrderBlock_AllSalesArea { get; set; }
        [JsonIgnore]
        public string OrderBlock_SalectedSalesArea { get; set; }
        [JsonIgnore]
        public string PostingBlock_AllCompanyCodes { get; set; }
        [JsonIgnore]
        public string PostingBlock_SelectedCompanyCode { get; set; }
        [JsonIgnore]
        public BlockUnblockData BlockUnblockData { get; set; }
    }
}
