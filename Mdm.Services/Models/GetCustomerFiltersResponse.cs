﻿
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mdm.Services.Models
{
    public class GetCustomerFiltersResponse
    {
        public string CustomerNumber { get; set; }
        public string SystemTypeId { get; set; }
        public List<string> SalesOrgs { get; set; }
        public List<string> DistChannels { get; set; }
        public List<string> Divisions { get; set; }

    }
}
