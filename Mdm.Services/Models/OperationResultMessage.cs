﻿


using Mdm.Services.Helpers;

namespace Mdm.Services.Models
{
    public class OperationResultMessage
    {
        public string Message { get; set; }
        public OperationalResultType OperationalResultType { get; set; }
    }
}
