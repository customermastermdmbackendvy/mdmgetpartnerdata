﻿
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mdm.Services.Models
{
    public class SearchSapPartnerResponse
    {
        public string WorkflowOrCustomerNumber { get; set; }
        public string MdmNumber { get; set; }
        public string PartnerCustomerName { get; set; }
        public string PartnerCustomerAddress { get; set; }
        public string PartnerCustomerRoleTypeId { get; set; }

    }
}
