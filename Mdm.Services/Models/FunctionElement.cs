﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mdm.Services.Models
{
    public class FunctionElement
    {
        public string FunctionName { get; set; }
        public Dictionary<string, object> CustomerElements { get; set; }
    }
}
