﻿
using Mdm.Services.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mdm.Services.Models
{
    
    public class SapPartnerWorkflowDataResponse
    {
        public SapPartnerWorkflowDataResponse()
        {
            SapPartners = new List<SapPartner>();
            AllowSapPartnerEdit = true;
            PartnerEdits = new List<PartnerEdit>();
        }
        public int SystemTypeId { get; set; }
        public int TaskId { get; set; }
        public int WorkflowTaskStateTypeId { get; set; }
        public int WorkflowStateTypeId { get; set; }
        public string WorkflowId { get; set; }
        public string UserId { get; set; }
        public string WorkflowTitle { get; set; }
        public string PurposeOfRequest { get; set; }
        public string WorkflowTaskNote { get; set; }
        public int? DistributionChannelTypeId { get; set; }
        public int? DivisionTypeId { get; set; }
        public int? SalesOrgTypeId { get; set; }
        public int? RoleTypeId { get; set; }
        public string WorkflowOrCustomerNumber { get; set; }
        public bool AllowSapPartnerEdit { get; set; }
        public List<SapPartner> SapPartners { get; set; }
        public List<PartnerEdit> PartnerEdits { get; set; }
        public List<DocumentModel> DocumentLocation { get; set; }
        public string RequestorName { get; set; }
        public DateTime DateOfCreation { get; set; }
        public List<TaskNoteData> TaskNoteData { get; set; }

    }

    public class TaskNoteData
    {
        public string TeamName { get; set; }
        public string TaskNote { get; set; }
        public WorkflowTaskStateType WorkflowTaskState { get; set; }
        public DateTime ModifiedDate { get; set; }
        public List<string> ApprovedRejectedUser { get; set; }
    }
}
