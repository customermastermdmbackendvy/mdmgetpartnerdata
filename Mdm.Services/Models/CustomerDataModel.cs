﻿using Mdm.Services.Helpers;
using System.Collections.Generic;

namespace Mdm.Services.Models
{
    public class CustomerDataModel
    {
        public Dictionary<string, object> CustomerElements { get; set; }
        public List<FunctionElement> FunctionElements { get; set; }
    }
}
