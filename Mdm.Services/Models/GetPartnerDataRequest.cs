﻿namespace Mdm.Services.Models
{
    public class GetPartnerDataRequest
    {
        public string UserId { get; set; }
        public string CustomerOrWorkflowId { get; set; }
        public int SystemTypeId { get; set; }
        public string WorkflowId
        {
            get 
            {
                if (!string.IsNullOrEmpty(CustomerOrWorkflowId))
                {
                    if (CustomerOrWorkflowId.StartsWith("wf", System.StringComparison.InvariantCultureIgnoreCase))
                        return CustomerOrWorkflowId;
                }
                return null;
            }
        }
        public string CustomerNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(CustomerOrWorkflowId))
                {
                    if (!CustomerOrWorkflowId.StartsWith("wf", System.StringComparison.InvariantCultureIgnoreCase))
                        return CustomerOrWorkflowId;
                }
                return null;
            }
        }
        public string MdmNumber { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Role { get; set; }
    }
}
