﻿namespace Mdm.Services.Models
{
    public class BlockUnblockData
    {
        public BlockItem OrderBlock { get; set; }
        public BlockItem DeliveryBlock { get; set; }
        public BlockItem BillingBlock { get; set; }
        public PostingBlock PostingBlock { get; set; }
        public string Purpose { get; set; }
    }
    public class BlockItem
    {
        public int AllSalesAreaId { get; set; }
        public int SelectedSalesAreaId { get; set; }
    }

    public class PostingBlock
    {
        public bool AllCompanyCodes { get; set; }
        public bool SelectedCompanyCode { get; set; }
    }
}
