﻿

using Amazon.Lambda.Core;
using AutoMapper;
using Mdm.Domain.Interfaces.Repositories;
using Mdm.Services.Helpers;
using Mdm.Services.Interfaces;
using Mdm.Services.Models;
using System.Collections.Generic;

namespace Mdm.Services
{
    public class BasePartnerDataService : IBasePartnerDataService
    {
        private readonly IBaseRepository _transactionalRepository;
        public IMapper Mapper { get; private set; }

        public OperationResult<List<PartnerDataResult>> OperationResult { get; set; }
        public OperationResult<SapPartnersResponse> SapPartnersOperationResult { get; set; }
        public OperationResult<SearchSapPartnerResponse> SearchSapPartnerOperationResult { get; set; }
        public OperationResult<SapPartnerWorkflowDataResponse> SapPartnerWorkflowDataOperationResult { get; set; }
        public OperationResult<GetCustomerFiltersResponse> GetCustomerFiltersResponseOperationResult { get; set; }

        public BasePartnerDataService(IMapper mapper, IBaseRepository transactionalRepository)
        {
            Mapper = mapper;
            _transactionalRepository = transactionalRepository;
        }

        public void ProcessOperationError(string message, string operationName)
        {
            OperationResult = new OperationResult<List<PartnerDataResult>>
            {
                OperationName = operationName,
                ResultData = new List<PartnerDataResult>()
            };

            ProcessOperationError(message);
        }

        public void ProcessSapPartnersOperationError(string message, string operationName)
        {
            SapPartnersOperationResult = new OperationResult<SapPartnersResponse>
            {
                OperationName = operationName,
                ResultData = new SapPartnersResponse()
            };

            ProcessSapPartnersOperationError(message);
        }

        public void ProcessSearchSapPartnerOperationError(string message, string operationName)
        {
            SearchSapPartnerOperationResult = new OperationResult<SearchSapPartnerResponse>
            {
                OperationName = operationName,
                ResultData = new SearchSapPartnerResponse()
            };

            ProcessSearchSapPartnerOperationError(message);
        }

        public void ProcessSapPartnerWorkflowDataOperationError(string message, string operationName)
        {
            SapPartnerWorkflowDataOperationResult = new OperationResult<SapPartnerWorkflowDataResponse>
            {
                OperationName = operationName,
                ResultData = new SapPartnerWorkflowDataResponse()
            };

            ProcessSapPartnerWorkflowDataOperationError(message);
        }
       
        protected void ProcessOperationError(string message)
        {
            OperationResult.OperationResultMessages.Add
            (
                new OperationResultMessage { Message = message, OperationalResultType = OperationalResultType.Error }
            );
            OperationResult.IsSuccess = false;
            LambdaLogger.Log(message);
            _transactionalRepository.Rollback();
        }

        protected void ProcessSapPartnersOperationError(string message)
        {
            SapPartnersOperationResult.OperationResultMessages.Add
            (
                new OperationResultMessage { Message = message, OperationalResultType = OperationalResultType.Error }
            );
            SapPartnersOperationResult.IsSuccess = false;
            LambdaLogger.Log(message);
            _transactionalRepository.Rollback();
        }

        protected void ProcessSearchSapPartnerOperationError(string message)
        {
            SearchSapPartnerOperationResult.OperationResultMessages.Add
            (
                new OperationResultMessage { Message = message, OperationalResultType = OperationalResultType.Error }
            );
            SearchSapPartnerOperationResult.IsSuccess = false;
            LambdaLogger.Log(message);
            _transactionalRepository.Rollback();
        }

        protected void ProcessSapPartnerWorkflowDataOperationError(string message)
        {
            SapPartnerWorkflowDataOperationResult.OperationResultMessages.Add
            (
                new OperationResultMessage { Message = message, OperationalResultType = OperationalResultType.Error }
            );
            SapPartnerWorkflowDataOperationResult.IsSuccess = false;
            LambdaLogger.Log(message);
            _transactionalRepository.Rollback();
        }

        protected void ProcessGetCustomerFiltersResponseOperationError(string message)
        {
            GetCustomerFiltersResponseOperationResult.OperationResultMessages.Add
            (
                new OperationResultMessage { Message = message, OperationalResultType = OperationalResultType.Error }
            );
            GetCustomerFiltersResponseOperationResult.IsSuccess = false;
            LambdaLogger.Log(message);
            _transactionalRepository.Rollback();
        }

        protected void ProcessSearchSapPartnerOperationValidation(string message)
        {
            if (SearchSapPartnerOperationResult == null && SapPartnerWorkflowDataOperationResult != null)
            {
                ProcessSapPartnerWorkflowDataOperationValidation(message);
            }
            else
            {
                SearchSapPartnerOperationResult.OperationResultMessages.Add
                (
                    new OperationResultMessage { Message = message, OperationalResultType = OperationalResultType.Validation }
                );
                SearchSapPartnerOperationResult.IsSuccess = false;
                LambdaLogger.Log(message);
                _transactionalRepository.Rollback();
            }
        }

        protected void ProcessSapPartnerWorkflowDataOperationValidation(string message)
        {
            SapPartnerWorkflowDataOperationResult.OperationResultMessages.Add
            (
                new OperationResultMessage { Message = message, OperationalResultType = OperationalResultType.Validation }
            );
            SapPartnerWorkflowDataOperationResult.IsSuccess = false;
            LambdaLogger.Log(message);
            _transactionalRepository.Rollback();
        }

        protected void ProcessGetCustomerFiltersResponseOperationValidation(string message)
        {
            GetCustomerFiltersResponseOperationResult.OperationResultMessages.Add
            (
                new OperationResultMessage { Message = message, OperationalResultType = OperationalResultType.Validation }
            );
            GetCustomerFiltersResponseOperationResult.IsSuccess = false;
            LambdaLogger.Log(message);
            _transactionalRepository.Rollback();
        }

        protected void InitOperationResult()
        {
            OperationResult = new OperationResult<List<PartnerDataResult>>
            {
                OperationName = "GetPartnerData",
                ResultData = new List<PartnerDataResult>()
            };
        }

        protected void InitSapPartnersOperationResult(string operationName)
        {
            SapPartnersOperationResult = new OperationResult<SapPartnersResponse>
            {
                OperationName = operationName,
                ResultData = new SapPartnersResponse()
            };
        }

        protected void InitSearchSapPartnerOperationResult(string operationName)
        {
            SearchSapPartnerOperationResult = new OperationResult<SearchSapPartnerResponse>
            {
                OperationName = operationName,
                ResultData = new SearchSapPartnerResponse()
            };
        }

        protected void InitSapPartnerWorkflowDataOperationResult(string operationName)
        {
            SapPartnerWorkflowDataOperationResult = new OperationResult<SapPartnerWorkflowDataResponse>
            {
                OperationName = operationName,
                ResultData = new SapPartnerWorkflowDataResponse()
            };
        }

        protected void InitGetCustomerFiltersResponseOperationResult()
        {
            GetCustomerFiltersResponseOperationResult = new OperationResult<GetCustomerFiltersResponse>
            {
                OperationName = "GetCustomerFiltersResponse",
                ResultData = new GetCustomerFiltersResponse()
            };
        }
    }
}
