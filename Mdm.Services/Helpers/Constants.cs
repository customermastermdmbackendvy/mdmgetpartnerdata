﻿namespace Mdm.Services.Helpers
{
    public static class PartnerConstants
    {
        public const string ParcelPartnerNumber = "7000070";
        public const string LTLPartnerNumber = "7000060";

        public const string SapSoldToRole = "0001";
        public const string ParcelPartnerName = "Small Parcel Freight Rates";
        public const string LTLPartnerName = "Czarlite LTL Freight Rates";
    }

    public enum SystemType
    {
        APOLLO = 1,
        OLYMPUS = 2,
        Pointman = 3,
        Made2Manage = 4,
        JDE = 5,
        Salesforce = 6
    }

    public enum PointmanRoleType
    {
        BillTo = 4,
        ShipTo = 2
    }
    public enum PartnerEditType
    {
        Add, Edit, Remove
    }
    public enum OperationalResultType
    {
        Success = 1,
        Validation = 2,
        Error = 3
    }

    public enum WorkflowStateType
    {
        New = 1,
        InProgress = 2,
        Completed = 3,
        Withdrawn = 4,
        Rejected = 5
    }


    public enum SapFunctionPartnerType
    {
        SP = 1,
        SH = 2,
        BP = 3,
        PY = 4,
        Z1 = 5,
        Z2 = 6
    }


    public enum JdePartnerFuncType
    {
        SOLDTO = 1,
        SHIPTO = 2,
        BILLTO = 3
    }

    public enum WorkflowTaskStateType
    {
        New = 1,
        InProgress = 2,
        Completed = 3,
        Approved = 4,
        Rejected = 5
    }

    public enum WorkflowTeamType
    {
        CustomerServiceSales = 1,
        GlobalTrade = 2,
        CustomerMaster = 3,
        Credit = 4,
        Contracts = 5,
        SalesOperations = 6,
        CustomerServiceOperations = 7,
        Pricing = 8,
        Tax = 9,
        CreditTeamWest = 10,
        CreditTeamNorthEast = 11,
        CreditTeamSouthEast = 12,
        CreditTeamMidSouth = 13,
        CreditTeamMidWest = 14,
        CreditTeamAfrica = 15,
        CreditTeamMexico = 16,
        CreditTeamCaribbean = 17,
        CreditTeamSouthAmerica = 18,
        CreditTeamOtherInternational = 19,
        Search = 20,
        OlympusGlobalTrade = 21,
        OlympusCustomerMaster = 22,
        OlympusCredit = 23,
        OlympusContracts = 24,
        OlympusPricing = 25,
        CustomerMasterJDE = 27,
        CreditJDE = 28,
        GlobalTradeJDE = 29,
        ContractJDE = 30,
        PricingJDE = 31,

    }

    public enum ApolloPartnerableRoleType
    {
        SoldTo = 1,
        ShipTo = 2,
        Payer = 3,
        BillTo = 4
    }

    public enum OlympusPartnerableRoleType
    {
        SoldTo = 1,
        ShipTo = 2,
        Payer = 3,
        BillTo = 4
    }

    public enum JdePartnerableRoleType
    {
        SoldTo = 1,
        BillTo = 2
    }
}

