﻿using System;
using System.Collections.Generic;

namespace Mdm.Services.Helpers
{
    public static partial class Extensions
    {
        public static string EmptyIfNull(this string @this)
        {
            return @this ?? string.Empty;
        }

        public static string GetValue(this Dictionary<string, object> @this, string elementKey, string defaultValue = "")
        {
            if (@this == null)
                return defaultValue;

            return @this.ContainsKey(elementKey) && @this[elementKey] != null ? @this[elementKey].ToString() : defaultValue;
        }
    }

    public class Utilities
    {
        public static string GetUniqueID(string prefix, int length)
        {
            return prefix + string.Format("{0:d" + length + "}", (DateTime.UtcNow.Ticks / 10) % 1000000000);
        }

        public static T GetEnumValue<T>(string str) where T : struct, IConvertible
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new Exception("T must be an Enumeration type.");
            }
            T val;
            return Enum.TryParse<T>(str, true, out val) ? val : default(T);
        }

        public static T GetEnumValue<T>(int intValue) where T : struct, IConvertible
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new Exception("T must be an Enumeration type.");
            }

            return (T)Enum.ToObject(enumType, intValue);
        }
    }
}
