
using Mdm.Domain.Entities;
using Mdm.Domain.Interfaces.Repositories;
using System.Collections.Generic;

namespace Mdm.WorkflowEngine.Helpers.Constants
{
    public static class StaticTypeData
    {
        public static List<StaticDataTypeDomain> ApolloRoleTypes { get; private set; }
        public static List<StaticDataTypeDomain> OlympusRoleTypes { get; private set; }
        public static List<StaticDataTypeDomain> JDERoleTypes { get; private set; }
        public static List<StaticDataTypeDomain> SalesOrgTypes { get; private set; }
      
   
        public static List<WorkflowTypeDomain> WorkflowTypes { get; private set; }
        public static List<WorkflowStateTypeDomain> WorkflowStateTypes { get; private set; }

     
        public static List<StaticDataTypeDomain> ApolloDistributionChannelTypes { get; private set; }
        public static List<StaticDataTypeDomain> ApolloDivisionTypes { get; private set; }
        public static List<StaticDataTypeDomain> ApolloPartnerFunctionTypes { get; private set; }

        public static List<StaticDataTypeDomain> OlympusSalesOrgTypes { get; private set; }
        public static List<StaticDataTypeDomain> OlympusDistributionChannelTypes { get; private set; }
        public static List<StaticDataTypeDomain> OlympusDivisionTypes { get; private set; }
        public static List<StaticDataTypeDomain> OlympusPartnerFunctionTypes { get; private set; }

        public static List<StaticDataTypeDomain> JdePartnerFunctionTypes { get; private set; }



        private static readonly object syncObj = new object();

        public static void CheckRefreshStaticData(IStaticDataRepository staticDataRepo)
        {
            if (ApolloRoleTypes == null ||
                OlympusRoleTypes == null ||
                JDERoleTypes == null ||
                 SalesOrgTypes == null ||
                 WorkflowTypes == null ||
                 WorkflowStateTypes == null ||        
                 ApolloDistributionChannelTypes == null ||
                 ApolloDivisionTypes == null ||
                 ApolloPartnerFunctionTypes == null ||
                 OlympusSalesOrgTypes == null ||
                 OlympusDistributionChannelTypes == null ||
                 OlympusDivisionTypes == null ||
                 OlympusPartnerFunctionTypes == null ||
                 JdePartnerFunctionTypes == null
                 )
            {
                LoadStaticData(staticDataRepo);
            }
        }
        public static void LoadStaticData(IStaticDataRepository staticDataRepo)
        {
            lock (syncObj)
            {
                BuildApolloRoleTypes(staticDataRepo);
                BuildOlympusRoleTypes(staticDataRepo);
                BuildJdeRoleTypes(staticDataRepo);
                BuildSalesOrgTypes(staticDataRepo);

                BuildWorkflowTypes(staticDataRepo);
                BuildWorkflowStateTypes(staticDataRepo);

                BuildApolloDistributionChannelTypes(staticDataRepo);
                BuildApolloDivisionTypes(staticDataRepo);

                BuildApolloPartnerFunctionTypes(staticDataRepo);

                BuildOlympusSalesOrgTypes(staticDataRepo);
                BuildOlympusDistributionChannelTypes(staticDataRepo);
                BuildOlympusDivisionTypes(staticDataRepo);
                BuildOlympusPartnerFunctionTypes(staticDataRepo);

                BuildJdePartnerFunctionTypes(staticDataRepo);
            }
        }

        private static void BuildApolloPartnerFunctionTypes(IStaticDataRepository staticDataRepo)
        {
            ApolloPartnerFunctionTypes = staticDataRepo.GetApolloPartnerFunctionTypes().Result;
        }

        private static void BuildApolloDivisionTypes(IStaticDataRepository staticDataRepo)
        {
            ApolloDivisionTypes = staticDataRepo.GetApolloDivisionTypes().Result;
        }

        private static void BuildApolloDistributionChannelTypes(IStaticDataRepository staticDataRepo)
        {
            ApolloDistributionChannelTypes = staticDataRepo.GetApolloDistributionChannelTypes().Result;
        }

      
        private static void BuildWorkflowTypes(IStaticDataRepository staticDataRepo)
        {
            WorkflowTypes = staticDataRepo.GetWorkflowTypeAsync().Result;
        }

        private static void BuildWorkflowStateTypes(IStaticDataRepository staticDataRepo)
        {
            WorkflowStateTypes = staticDataRepo.GetWorkflowStateTypes().Result;
        }

        private static void BuildApolloRoleTypes(IStaticDataRepository staticDataRepo)
        {
            ApolloRoleTypes = staticDataRepo.GetApolloRoleTypes().Result;
        }

        private static void BuildOlympusRoleTypes(IStaticDataRepository staticDataRepo)
        {
            OlympusRoleTypes = staticDataRepo.GetOlympusRoleTypes().Result;
        }

        private static void BuildJdeRoleTypes(IStaticDataRepository staticDataRepo)
        {
            JDERoleTypes = staticDataRepo.GetJdeRoleTypes().Result;
        }

        private static void BuildSalesOrgTypes(IStaticDataRepository staticDataRepo)
        {
            SalesOrgTypes = staticDataRepo.GetSalesOrgs().Result;
        }
        private static void BuildOlympusSalesOrgTypes(IStaticDataRepository staticDataRepo)
        {
            OlympusSalesOrgTypes = staticDataRepo.GetOlympusSalesOrgTypes().Result;
        }

        private static void BuildOlympusDistributionChannelTypes(IStaticDataRepository staticDataRepo)
        {
            OlympusDistributionChannelTypes = staticDataRepo.GetOlympusDistributionChannelTypes().Result;
        }

        private static void BuildOlympusDivisionTypes(IStaticDataRepository staticDataRepo)
        {
            OlympusDivisionTypes = staticDataRepo.GetOlympusDivisionTypes().Result;
        }

        private static void BuildOlympusPartnerFunctionTypes(IStaticDataRepository staticDataRepo)
        {
            OlympusPartnerFunctionTypes = staticDataRepo.GetOlympusPartnerFunctionTypes().Result;
        }

        private static void BuildJdePartnerFunctionTypes(IStaticDataRepository staticDataRepo)
        {
            JdePartnerFunctionTypes = staticDataRepo.GetJdePartnerFunctionTypes().Result;
        }
    }
}