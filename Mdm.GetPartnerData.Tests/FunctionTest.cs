
using Amazon.Lambda.TestUtilities;
using Mdm.GetPartnerData;
using Mdm.Services.Helpers;
using Mdm.Services.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.IO;
using Xunit;

namespace Mdm.TasksGlobalTrade
{
    public class FunctionTest
    {
        private const string EnvironmentRegion = "EnvironmentRegion";
        private const string RdsSecretName = "RdsSecretName";
        private const string Environment = "Environment";
        private const string GetErpUrl = "GetErpUrl";
        private const string RedshiftSecretName = "RedshiftSecretName";

        /// <summary>
        /// Force CI/CD
        /// </summary>
        public FunctionTest()
        {
            SetEnvironmentVariables();

        }
        private void SetEnvironmentVariables()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();

            System.Environment.SetEnvironmentVariable(EnvironmentRegion, configuration[EnvironmentRegion]);
            System.Environment.SetEnvironmentVariable(RdsSecretName, configuration[RdsSecretName]);
            System.Environment.SetEnvironmentVariable(Environment, configuration[Environment]);
            System.Environment.SetEnvironmentVariable(GetErpUrl, configuration[GetErpUrl]);
            System.Environment.SetEnvironmentVariable(RedshiftSecretName, configuration[RedshiftSecretName]);
        }

        #region GetPartnerData Tests for Ptmn
       // [Fact]
        public async void TestGetPartnerData()
        {
            var function = new MdmGetPartnerData();
            var context = new TestLambdaContext();
            //{"UserId":"steve.goldman","CustomerOrWorkflowId":"56565677","SystemTypeId":3}
            //{"UserId":"steve.goldman","CustomerOrWorkflowId":"wf000000766469311","SystemTypeId":3}
            var getPartnerDataRequest = new GetPartnerDataRequest
            {
                //WorkflowId = "wf000000885048745",
                CustomerOrWorkflowId = "10017095",//"wf000000782896674",//"wf000000571261543",
                SystemTypeId = 3,
                CustomerName= "DUPAGE",
                Address= "3825 HIGHLAND ",
                City= "DOWNERS GROVE",
                Role="BILLTO"
			};
            var result = await function.GetPartnerData(getPartnerDataRequest, context);
            Assert.True(result.IsSuccess);
        }
        #endregion

        #region Tests for SearchApolloPartner
        /// <summary>
        /// This lambda is for searching for partners to partner with on the add partner feature
        /// </summary>
       // [Fact]
        public async void TestSearchApolloPartnerByCustomerNuber()
        {
            var function = new MdmGetPartnerData();
            var context = new TestLambdaContext();
            var searchApolloPartnerRequest = new SapPartnerRequest
            {
                DistributionChannelTypeId = "1",
                DivisionTypeId = "1",
                SalesOrgTypeId = "2",
                UserId = "steve.goldman",
                WorkflowOrCustomerNumber = "wf000000555093153"//"0010041786"
            };
            var result = await function.SearchApolloPartner(searchApolloPartnerRequest, context);
            Assert.True(result.IsSuccess);
        }

        //[Fact]
        public async void TestSearchApolloPartnerByComplete()
        {
            var function = new MdmGetPartnerData();
            var context = new TestLambdaContext();
            var searchApolloPartnerRequest = new SapPartnerRequest
            {
                DistributionChannelTypeId = "1",
                DivisionTypeId = "1",
                SalesOrgTypeId = "2",
                UserId = "steve.goldman",
                WorkflowOrCustomerNumber = "wf000000168076515"
            };
            var result = await function.SearchApolloPartner(searchApolloPartnerRequest, context);
            Assert.True(result.IsSuccess);
        }

        //[Fact]
        public async void TestSearchApolloPartnerByInProgress()
        {
            var function = new MdmGetPartnerData();
            var context = new TestLambdaContext();
            var searchApolloPartnerRequest = new SapPartnerRequest
            {
                DistributionChannelTypeId = "1",
                DivisionTypeId = "1",
                SalesOrgTypeId = "2",
                UserId = "steve.goldman",
                WorkflowOrCustomerNumber = "wf000000549138193"
            };
            var result = await function.SearchApolloPartner(searchApolloPartnerRequest, context);
            Assert.True(result.IsSuccess);
        }

       // [Fact]
        public async void TestSearchApolloPartnerByRejected()
        {
            var function = new MdmGetPartnerData();
            var context = new TestLambdaContext();
            var searchApolloPartnerRequest = new SapPartnerRequest
            {
                DistributionChannelTypeId = "1",
                DivisionTypeId = "1",
                SalesOrgTypeId = "2",
                UserId = "steve.goldman",
                WorkflowOrCustomerNumber = "wf000000168076515"
            };
            var result = await function.SearchApolloPartner(searchApolloPartnerRequest, context);
            Assert.True(result.IsSuccess);
        }
        #endregion

        #region GetApolloPartners
        /// <summary>
        /// This lambda finds apollo partners for a given wf or customer number
        /// </summary>
       [Fact]
        public async void TestGetApolloPartners()
        {
            //MDMCM001175396 has workflow id = wf000000168076515
            var function = new MdmGetPartnerData();
            var context = new TestLambdaContext();
            //{"UserId":"steve.goldman","CustomerOrWorkflowId":"56565677","SystemTypeId":3}
            //{"UserId":"steve.goldman","CustomerOrWorkflowId":"wf000000766469311","SystemTypeId":3}
            var apolloPartnersRequest = new SapPartnersRequest
            {
                DistributionChannelTypeId = "1",
                DivisionTypeId = "1",
                SalesOrgTypeId = "1",
                UserId = "steve.goldman",
                WorkflowOrCustomerNumber = "20056632"
            };
            var serialized = JsonConvert.SerializeObject(apolloPartnersRequest);
            var result = await function.GetApolloPartners(apolloPartnersRequest, context);
            Assert.True(result.IsSuccess);
        }
        // {"UserId":"steve.goldman","WorkflowOrCustomerNumber":"wf000000366248447","DistributionChannelTypeId":"1","DivisionTypeId":"1","SalesOrgTypeId":"1"}
        //10012700, 13, 1, 2
        [Fact]
        public async void TestGetOlympusPartners()
        {
            //MDMCM001175396 has workflow id = wf000000168076515
            var function = new MdmGetPartnerData();
            var context = new TestLambdaContext();
            //{"UserId":"steve.goldman","CustomerOrWorkflowId":"56565677","SystemTypeId":3}
            //{"UserId":"steve.goldman","CustomerOrWorkflowId":"wf000000766469311","SystemTypeId":3}
            var apolloPartnersRequest = new SapPartnersRequest
            {
                DistributionChannelTypeId = "1",
                DivisionTypeId = "2",
                SalesOrgTypeId = "13",
                UserId = "steve.goldman",
                WorkflowOrCustomerNumber = "0010012700"
            };
            var serialized = JsonConvert.SerializeObject(apolloPartnersRequest);
            var result = await function.GetOlympusPartners(apolloPartnersRequest, context);
            Assert.True(result.IsSuccess);
        }
        // [Fact]
        public async void TestGetApolloPartnersCompletedWorkflow()
        {
            //MDMCM001175396 has workflow id = wf000000168076515
            var function = new MdmGetPartnerData();
            var context = new TestLambdaContext();
            //{"UserId":"steve.goldman","CustomerOrWorkflowId":"56565677","SystemTypeId":3}
            //{"UserId":"steve.goldman","CustomerOrWorkflowId":"wf000000766469311","SystemTypeId":3}
            var apolloPartnersRequest = new SapPartnersRequest
            {
                DistributionChannelTypeId = "1",
                DivisionTypeId = "1",
                SalesOrgTypeId = "2",
                UserId = "steve.goldman",
                WorkflowOrCustomerNumber = "wf000000168076515"
            };
            var result = await function.GetApolloPartners(apolloPartnersRequest, context);
            Assert.True(result.IsSuccess);
        }

      //[Fact]
        public async void TestGetApolloPartnersInCompletWorkflow()
        {
            //MDMCM001175396 has workflow id = wf000000168076515
            var function = new MdmGetPartnerData();
            var context = new TestLambdaContext();
            //{"UserId":"steve.goldman","CustomerOrWorkflowId":"56565677","SystemTypeId":3}
            //{"UserId":"steve.goldman","CustomerOrWorkflowId":"wf000000766469311","SystemTypeId":3}
            var apolloPartnersRequest = new SapPartnersRequest
            {
                DistributionChannelTypeId = "1",
                DivisionTypeId = "1",
                SalesOrgTypeId = "1",
                UserId = "steve.goldman",
                WorkflowOrCustomerNumber = "0010055269"
            };
            var result = await function.GetApolloPartners(apolloPartnersRequest, context);
            Assert.True(result.IsSuccess);
        }
        #endregion

        #region Filter Tests
      // [Fact]
        public async void TestFilter()
        {
            var function = new MdmGetPartnerData();
            var context = new TestLambdaContext();
            var request = new GetCustomerFiltersRequest
            {
                SystemTypeId = 2,
                UserId = "steve.goldman",
                CustomerNumber = "0010077527"
            };
            var result = await function.GetCustomerFilterData(request, context);
            Assert.True(result.IsSuccess);
        }

        #endregion

        #region Tests for Get Apollo Partner WorkflowData
        /// <summary>
        /// This lambda is for getting the apollo partner task data
        /// </summary>
        // [Fact]
        public async void TestGetApolloWorkflowDataSoldToIsCustomer()
        {
            var function = new MdmGetPartnerData();
            var context = new TestLambdaContext();
            var apolloPartnerWorkflowRequest = new SearchSapPartnerWorkflowRequest
            {
                UserId = "steve.goldman",
                WorkflowId = "wf000000973320281"//"wf000000135298849"//"wf000000023711846"
            };//WorkflowId: "wf000000081438141", UserId: "shibu.loyoladass"
            var result = await function.GetApolloPartnerWorkflowData(apolloPartnerWorkflowRequest, context);
            Assert.True(result.IsSuccess);
        }


     //[Fact]
        public async void TestGetApolloWorkflowDataSoldToIsWorkflow()
        {
            var function = new MdmGetPartnerData();
            var context = new TestLambdaContext();
            var apolloPartnerWorkflowRequest = new SearchSapPartnerWorkflowRequest
            {
                UserId = "steve.goldman",
                WorkflowId = "wf000000610415702"
            };
            var result = await function.GetApolloPartnerWorkflowData(apolloPartnerWorkflowRequest, context);
            Assert.True(result.IsSuccess);
        }
        #endregion
    }
}
