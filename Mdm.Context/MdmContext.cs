﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Mdm.Context.Entities;

namespace Mdm.Context
{
    public partial class MdmContext : DbContext
    {
        public MdmContext()
        {
        }

        public MdmContext(DbContextOptions<MdmContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ApolloAccountStatementType> ApolloAccountStatementType { get; set; }
        public virtual DbSet<ApolloAccountType> ApolloAccountType { get; set; }
        public virtual DbSet<ApolloAcctAssignmentGroupType> ApolloAcctAssignmentGroupType { get; set; }
        public virtual DbSet<ApolloBlockBillingType> ApolloBlockBillingType { get; set; }
        public virtual DbSet<ApolloBlockDeliveryType> ApolloBlockDeliveryType { get; set; }
        public virtual DbSet<ApolloBlockOrderType> ApolloBlockOrderType { get; set; }
        public virtual DbSet<ApolloCompanyCodeType> ApolloCompanyCodeType { get; set; }
        public virtual DbSet<ApolloCreditControlAreaType> ApolloCreditControlAreaType { get; set; }
        public virtual DbSet<ApolloCreditRepGroupType> ApolloCreditRepGroupType { get; set; }
        public virtual DbSet<ApolloCustomerClassType> ApolloCustomerClassType { get; set; }
        public virtual DbSet<ApolloCustomerGroupType> ApolloCustomerGroupType { get; set; }
        public virtual DbSet<ApolloCustomerPriceProcType> ApolloCustomerPriceProcType { get; set; }
        public virtual DbSet<ApolloDapa2priceType> ApolloDapa2priceType { get; set; }
        public virtual DbSet<ApolloDapapriceType> ApolloDapapriceType { get; set; }
        public virtual DbSet<ApolloDeliveryPriorityType> ApolloDeliveryPriorityType { get; set; }
        public virtual DbSet<ApolloDistLevelType> ApolloDistLevelType { get; set; }
        public virtual DbSet<ApolloDistributionChannelType> ApolloDistributionChannelType { get; set; }
        public virtual DbSet<ApolloDivisionType> ApolloDivisionType { get; set; }
        public virtual DbSet<ApolloFssPriceType> ApolloFssPriceType { get; set; }
        public virtual DbSet<ApolloIncoTermsType> ApolloIncoTermsType { get; set; }
        public virtual DbSet<ApolloIndustryCodeType> ApolloIndustryCodeType { get; set; }
        public virtual DbSet<ApolloIndustryType> ApolloIndustryType { get; set; }
        public virtual DbSet<ApolloMarketingSegmentationType> ApolloMarketingSegmentationType { get; set; }
        public virtual DbSet<ApolloPartnerFunctionType> ApolloPartnerFunctionType { get; set; }
        public virtual DbSet<ApolloPaymentTermsType> ApolloPaymentTermsType { get; set; }
        public virtual DbSet<ApolloPpcustProcType> ApolloPpcustProcType { get; set; }
        public virtual DbSet<ApolloPriceListType> ApolloPriceListType { get; set; }
        public virtual DbSet<ApolloReconAccountType> ApolloReconAccountType { get; set; }
        public virtual DbSet<ApolloRiskCategoryType> ApolloRiskCategoryType { get; set; }
        public virtual DbSet<ApolloSalesOfficeType> ApolloSalesOfficeType { get; set; }
        public virtual DbSet<ApolloShippingConditionsType> ApolloShippingConditionsType { get; set; }
        public virtual DbSet<ApolloShippingCustomerType> ApolloShippingCustomerType { get; set; }
        public virtual DbSet<ApolloSpecialPricingType> ApolloSpecialPricingType { get; set; }
        public virtual DbSet<ApolloTaxClassificationType> ApolloTaxClassificationType { get; set; }
        public virtual DbSet<CategoryType> CategoryType { get; set; }
        public virtual DbSet<CminternalMap> CminternalMap { get; set; }
        public virtual DbSet<CmuserProdBackup> CmuserProdBackup { get; set; }
        public virtual DbSet<DocumentType> DocumentType { get; set; }
        public virtual DbSet<Integrations> Integrations { get; set; }
        public virtual DbSet<Jde1099reportingType> Jde1099reportingType { get; set; }
        public virtual DbSet<JdeadjustmentScheduleType> JdeadjustmentScheduleType { get; set; }
        public virtual DbSet<JdebillingAddressTypeType> JdebillingAddressTypeType { get; set; }
        public virtual DbSet<JdecarrierType> JdecarrierType { get; set; }
        public virtual DbSet<JdecategoryCode03Type> JdecategoryCode03Type { get; set; }
        public virtual DbSet<JdecategoryCode22Type> JdecategoryCode22Type { get; set; }
        public virtual DbSet<JdecategoryCode28Type> JdecategoryCode28Type { get; set; }
        public virtual DbSet<JdecategoryCode29Type> JdecategoryCode29Type { get; set; }
        public virtual DbSet<JdecollectionManagerType> JdecollectionManagerType { get; set; }
        public virtual DbSet<JdecompanyType> JdecompanyType { get; set; }
        public virtual DbSet<JdecountryType> JdecountryType { get; set; }
        public virtual DbSet<JdecurrencyType> JdecurrencyType { get; set; }
        public virtual DbSet<JdedocumentsLanguageType> JdedocumentsLanguageType { get; set; }
        public virtual DbSet<JdegeographicalType> JdegeographicalType { get; set; }
        public virtual DbSet<JdegloffsetType> JdegloffsetType { get; set; }
        public virtual DbSet<Jdegpotype> Jdegpotype { get; set; }
        public virtual DbSet<JdeincoTermsType> JdeincoTermsType { get; set; }
        public virtual DbSet<JdeindustryClassType> JdeindustryClassType { get; set; }
        public virtual DbSet<JdeitemRestrictionsType> JdeitemRestrictionsType { get; set; }
        public virtual DbSet<JdelanguageType> JdelanguageType { get; set; }
        public virtual DbSet<JdemarketSegmentationType> JdemarketSegmentationType { get; set; }
        public virtual DbSet<JdePartnerFunctionType> JdePartnerFunctionType { get; set; }
        public virtual DbSet<JdepaymentInstructionType> JdepaymentInstructionType { get; set; }
        public virtual DbSet<JdepaymentTermsType> JdepaymentTermsType { get; set; }
        public virtual DbSet<JdepolicyNameType> JdepolicyNameType { get; set; }
        public virtual DbSet<JdepriceGroupType> JdepriceGroupType { get; set; }
        public virtual DbSet<JderegionType> JderegionType { get; set; }
        public virtual DbSet<JderelatedAddressNumType> JderelatedAddressNumType { get; set; }
        public virtual DbSet<JderoleType> JderoleType { get; set; }
        public virtual DbSet<JdesalesAreaType> JdesalesAreaType { get; set; }
        public virtual DbSet<JdesalesOrgType> JdesalesOrgType { get; set; }
        public virtual DbSet<JdesalesRepsType> JdesalesRepsType { get; set; }
        public virtual DbSet<JdesearchTypeType> JdesearchTypeType { get; set; }
        public virtual DbSet<JdesendMethodType> JdesendMethodType { get; set; }
        public virtual DbSet<JdestateType> JdestateType { get; set; }
        public virtual DbSet<JdetaxExplCodeType> JdetaxExplCodeType { get; set; }
        public virtual DbSet<JdetaxRateAreaType> JdetaxRateAreaType { get; set; }
        public virtual DbSet<JdetimeZoneType> JdetimeZoneType { get; set; }
        public virtual DbSet<MdmNumberMapping> MdmNumberMapping { get; set; }
        public virtual DbSet<MdmTeam> MdmTeam { get; set; }
        public virtual DbSet<MdmTeamUser> MdmTeamUser { get; set; }
        public virtual DbSet<MdmUser> MdmUser { get; set; }
        public virtual DbSet<OlympusAccountStatementType> OlympusAccountStatementType { get; set; }
        public virtual DbSet<OlympusAcctAssignmentGroupType> OlympusAcctAssignmentGroupType { get; set; }
        public virtual DbSet<OlympusCategoryType> OlympusCategoryType { get; set; }
        public virtual DbSet<OlympusCompanyCodeType> OlympusCompanyCodeType { get; set; }
        public virtual DbSet<OlympusCompanyCodeTypeValidation> OlympusCompanyCodeTypeValidation { get; set; }
        public virtual DbSet<OlympusCountryType> OlympusCountryType { get; set; }
        public virtual DbSet<OlympusCreditControlAreaType> OlympusCreditControlAreaType { get; set; }
        public virtual DbSet<OlympusCreditControlAreaTypeValidation> OlympusCreditControlAreaTypeValidation { get; set; }
        public virtual DbSet<OlympusCurrencyType> OlympusCurrencyType { get; set; }
        public virtual DbSet<OlympusCustPriceProcType> OlympusCustPriceProcType { get; set; }
        public virtual DbSet<OlympusCustomerClassType> OlympusCustomerClassType { get; set; }
        public virtual DbSet<OlympusCustomerCreditGroupType> OlympusCustomerCreditGroupType { get; set; }
        public virtual DbSet<OlympusCustomerGroupType> OlympusCustomerGroupType { get; set; }
        public virtual DbSet<OlympusDeliveringPlantType> OlympusDeliveringPlantType { get; set; }
        public virtual DbSet<OlympusDeliveringPlantTypeValidation> OlympusDeliveringPlantTypeValidation { get; set; }
        public virtual DbSet<OlympusDeliveryPriorityType> OlympusDeliveryPriorityType { get; set; }
        public virtual DbSet<OlympusDistributionChannelType> OlympusDistributionChannelType { get; set; }
        public virtual DbSet<OlympusDistributionChannelTypeValidation> OlympusDistributionChannelTypeValidation { get; set; }
        public virtual DbSet<OlympusDivisionType> OlympusDivisionType { get; set; }
        public virtual DbSet<OlympusDivisionTypeValidation> OlympusDivisionTypeValidation { get; set; }
        public virtual DbSet<OlympusIncoTermsType> OlympusIncoTermsType { get; set; }
        public virtual DbSet<OlympusIndustryCodeType> OlympusIndustryCodeType { get; set; }
        public virtual DbSet<OlympusIndustryType> OlympusIndustryType { get; set; }
        public virtual DbSet<OlympusLanguageType> OlympusLanguageType { get; set; }
        public virtual DbSet<OlympusMarketingSegmentationType> OlympusMarketingSegmentationType { get; set; }
        public virtual DbSet<OlympusMaxPartialDeliveriesType> OlympusMaxPartialDeliveriesType { get; set; }
        public virtual DbSet<OlympusPartialDeliveryPerItemType> OlympusPartialDeliveryPerItemType { get; set; }
        public virtual DbSet<OlympusPartnerFunctionType> OlympusPartnerFunctionType { get; set; }
        public virtual DbSet<OlympusPriceGroupType> OlympusPriceGroupType { get; set; }
        public virtual DbSet<OlympusPriorityType> OlympusPriorityType { get; set; }
        public virtual DbSet<OlympusReconAccountType> OlympusReconAccountType { get; set; }
        public virtual DbSet<OlympusRiskCategoryType> OlympusRiskCategoryType { get; set; }
        public virtual DbSet<OlympusRoleType> OlympusRoleType { get; set; }
        public virtual DbSet<OlympusSalesDistrictType> OlympusSalesDistrictType { get; set; }
        public virtual DbSet<OlympusSalesGroupType> OlympusSalesGroupType { get; set; }
        public virtual DbSet<OlympusSalesGroupTypeValidation> OlympusSalesGroupTypeValidation { get; set; }
        public virtual DbSet<OlympusSalesOfficeType> OlympusSalesOfficeType { get; set; }
        public virtual DbSet<OlympusSalesOfficeTypeValidation> OlympusSalesOfficeTypeValidation { get; set; }
        public virtual DbSet<OlympusSalesOrgType> OlympusSalesOrgType { get; set; }
        public virtual DbSet<OlympusShippingConditionsType> OlympusShippingConditionsType { get; set; }
        public virtual DbSet<OlympusSortKeyType> OlympusSortKeyType { get; set; }
        public virtual DbSet<OlympusTaxClassificationType> OlympusTaxClassificationType { get; set; }
        public virtual DbSet<OlympusTermsOfPaymentType> OlympusTermsOfPaymentType { get; set; }
        public virtual DbSet<OlympusTradingPartnerType> OlympusTradingPartnerType { get; set; }
        public virtual DbSet<OlympusTransportationZoneType> OlympusTransportationZoneType { get; set; }
        public virtual DbSet<OlympusTransportationZoneTypeValidation> OlympusTransportationZoneTypeValidation { get; set; }
        public virtual DbSet<OlympusUritype> OlympusUritype { get; set; }
        public virtual DbSet<PointmanMarketSegmentationType> PointmanMarketSegmentationType { get; set; }
        public virtual DbSet<PointmanPartnerFunctionType> PointmanPartnerFunctionType { get; set; }
        public virtual DbSet<PointmanRoleType> PointmanRoleType { get; set; }
        public virtual DbSet<RoleType> RoleType { get; set; }
        public virtual DbSet<SalesOrgType> SalesOrgType { get; set; }
        public virtual DbSet<SicNaicsMapping> SicNaicsMapping { get; set; }
        public virtual DbSet<StaticDataCacheType> StaticDataCacheType { get; set; }
        public virtual DbSet<SystemType> SystemType { get; set; }
        public virtual DbSet<Workflow> Workflow { get; set; }
        public virtual DbSet<WorkflowBlockCustomer> WorkflowBlockCustomer { get; set; }
        public virtual DbSet<WorkflowCreateCustomer> WorkflowCreateCustomer { get; set; }
        public virtual DbSet<WorkflowDocument> WorkflowDocument { get; set; }
        public virtual DbSet<WorkflowExtendToNewSalesOrg> WorkflowExtendToNewSalesOrg { get; set; }
        public virtual DbSet<WorkflowFunctionCustomer> WorkflowFunctionCustomer { get; set; }
        public virtual DbSet<WorkflowOther> WorkflowOther { get; set; }
        public virtual DbSet<WorkflowPartners> WorkflowPartners { get; set; }
        public virtual DbSet<WorkflowRegionMapping> WorkflowRegionMapping { get; set; }
        public virtual DbSet<WorkflowStateType> WorkflowStateType { get; set; }
        public virtual DbSet<WorkflowTask> WorkflowTask { get; set; }
        public virtual DbSet<WorkflowTaskStateType> WorkflowTaskStateType { get; set; }
        public virtual DbSet<WorkflowTaskType> WorkflowTaskType { get; set; }
        public virtual DbSet<WorkflowTaskTypeTeam> WorkflowTaskTypeTeam { get; set; }
        public virtual DbSet<WorkflowTaskUser> WorkflowTaskUser { get; set; }
        public virtual DbSet<WorkflowType> WorkflowType { get; set; }
        public virtual DbSet<WorkflowUpdates> WorkflowUpdates { get; set; }

       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApolloAccountStatementType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloAccountType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloAcctAssignmentGroupType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloBlockBillingType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloBlockDeliveryType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloBlockOrderType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloCompanyCodeType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloCreditControlAreaType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloCreditRepGroupType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloCustomerClassType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloCustomerGroupType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloCustomerPriceProcType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloDapa2priceType>(entity =>
            {
                entity.ToTable("ApolloDAPA2PriceType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ApolloDapa2priceTypecol)
                    .HasColumnName("ApolloDAPA2PriceTypecol")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloDapapriceType>(entity =>
            {
                entity.ToTable("ApolloDAPAPriceType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ApolloDapa2priceTypecol)
                    .HasColumnName("ApolloDAPA2PriceTypecol")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloDeliveryPriorityType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloDistLevelType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloDistributionChannelType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<ApolloDivisionType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<ApolloFssPriceType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloIncoTermsType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloIndustryCodeType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloIndustryType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloMarketingSegmentationType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault)
                    .IsRequired()
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("'b\\'0\\''");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloPartnerFunctionType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault)
                    .IsRequired()
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("'b\\'0\\''");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloPaymentTermsType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloPpcustProcType>(entity =>
            {
                entity.ToTable("ApolloPPCustProcType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloPriceListType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloReconAccountType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloRiskCategoryType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloSalesOfficeType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloShippingConditionsType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloShippingCustomerType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloSpecialPricingType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloTaxClassificationType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<CategoryType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<CminternalMap>(entity =>
            {
                entity.ToTable("CMInternalMap");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.RedshiftTableName).HasColumnType("varchar(30)");

                entity.Property(e => e.SourceField).HasColumnType("varchar(30)");

                entity.Property(e => e.TargetField).HasColumnType("varchar(30)");
            });

            modelBuilder.Entity<CmuserProdBackup>(entity =>
            {
                entity.ToTable("CMUserProdBackup");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Email2).HasColumnType("varchar(200)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<DocumentType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<Integrations>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .HasName("idx_Integrations_Identifier");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.Data).HasColumnType("longtext");

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.PrimaryId).HasColumnType("varchar(50)");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Jde1099reportingType>(entity =>
            {
                entity.ToTable("JDE1099ReportingType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdeadjustmentScheduleType>(entity =>
            {
                entity.ToTable("JDEAdjustmentScheduleType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdebillingAddressTypeType>(entity =>
            {
                entity.ToTable("JDEBillingAddressTypeType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdecarrierType>(entity =>
            {
                entity.ToTable("JDECarrierType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdecategoryCode03Type>(entity =>
            {
                entity.ToTable("JDECategoryCode03Type");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdecategoryCode22Type>(entity =>
            {
                entity.ToTable("JDECategoryCode22Type");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdecategoryCode28Type>(entity =>
            {
                entity.ToTable("JDECategoryCode28Type");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdecategoryCode29Type>(entity =>
            {
                entity.ToTable("JDECategoryCode29Type");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdecollectionManagerType>(entity =>
            {
                entity.ToTable("JDECollectionManagerType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdecompanyType>(entity =>
            {
                entity.ToTable("JDECompanyType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdecountryType>(entity =>
            {
                entity.ToTable("JDECountryType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdecurrencyType>(entity =>
            {
                entity.ToTable("JDECurrencyType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdedocumentsLanguageType>(entity =>
            {
                entity.ToTable("JDEDocumentsLanguageType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdegeographicalType>(entity =>
            {
                entity.ToTable("JDEGeographicalType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdegloffsetType>(entity =>
            {
                entity.ToTable("JDEGLOffsetType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<Jdegpotype>(entity =>
            {
                entity.ToTable("JDEGPOType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdeincoTermsType>(entity =>
            {
                entity.ToTable("JDEIncoTermsType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdeindustryClassType>(entity =>
            {
                entity.ToTable("JDEIndustryClassType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdeitemRestrictionsType>(entity =>
            {
                entity.ToTable("JDEItemRestrictionsType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdelanguageType>(entity =>
            {
                entity.ToTable("JDELanguageType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdemarketSegmentationType>(entity =>
            {
                entity.ToTable("JDEMarketSegmentationType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault)
                    .IsRequired()
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("'b\\'0\\''");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdePartnerFunctionType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault)
                    .IsRequired()
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("'b\\'0\\''");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdepaymentInstructionType>(entity =>
            {
                entity.ToTable("JDEPaymentInstructionType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdepaymentTermsType>(entity =>
            {
                entity.ToTable("JDEPaymentTermsType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdepolicyNameType>(entity =>
            {
                entity.ToTable("JDEPolicyNameType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdepriceGroupType>(entity =>
            {
                entity.ToTable("JDEPriceGroupType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JderegionType>(entity =>
            {
                entity.ToTable("JDERegionType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JderelatedAddressNumType>(entity =>
            {
                entity.ToTable("JDERelatedAddressNumType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JderoleType>(entity =>
            {
                entity.ToTable("JDERoleType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdesalesAreaType>(entity =>
            {
                entity.ToTable("JDESalesAreaType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdesalesOrgType>(entity =>
            {
                entity.ToTable("JDESalesOrgType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdesalesRepsType>(entity =>
            {
                entity.ToTable("JDESalesRepsType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdesearchTypeType>(entity =>
            {
                entity.ToTable("JDESearchTypeType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdesendMethodType>(entity =>
            {
                entity.ToTable("JDESendMethodType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdestateType>(entity =>
            {
                entity.ToTable("JDEStateType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdetaxExplCodeType>(entity =>
            {
                entity.ToTable("JDETaxExplCodeType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdetaxRateAreaType>(entity =>
            {
                entity.ToTable("JDETaxRateAreaType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<JdetimeZoneType>(entity =>
            {
                entity.ToTable("JDETimeZoneType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<MdmNumberMapping>(entity =>
            {
                entity.HasIndex(e => e.NewMdmNumber)
                    .HasName("idx_MdmNumberMapping_NewMdmNumber");

                entity.HasIndex(e => e.OldMdmNumber)
                    .HasName("idx_MdmNumberMapping_OldMdmNumber");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedDate).HasColumnType("timestamp");

                entity.Property(e => e.ExpiredDate).HasColumnType("timestamp");

                entity.Property(e => e.NewMdmNumber)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.OldMdmNumber)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<MdmTeam>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Email).HasColumnType("varchar(150)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<MdmTeamUser>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.MdmTeamId).HasColumnType("int(11)");

                entity.Property(e => e.MdmUserId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<MdmUser>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Email2).HasColumnType("varchar(200)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<OlympusAccountStatementType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusAcctAssignmentGroupType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusCategoryType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusCompanyCodeType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusCompanyCodeTypeValidation>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CompanyCodeTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<OlympusCountryType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusCreditControlAreaType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusCreditControlAreaTypeValidation>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreditControlAreaTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<OlympusCurrencyType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusCustPriceProcType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusCustomerClassType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusCustomerCreditGroupType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusCustomerGroupType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusDeliveringPlantType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusDeliveringPlantTypeValidation>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DeliveringPlantTypeId).HasColumnType("int(11)");

                entity.Property(e => e.DistributionChannelTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<OlympusDeliveryPriorityType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusDistributionChannelType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusDistributionChannelTypeValidation>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DistributionChannelTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<OlympusDivisionType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusDivisionTypeValidation>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DistributionChannelTypeId).HasColumnType("int(11)");

                entity.Property(e => e.DivisionTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<OlympusIncoTermsType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusIndustryCodeType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusIndustryType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusLanguageType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusMarketingSegmentationType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault)
                    .IsRequired()
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("'b\\'0\\''");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusMaxPartialDeliveriesType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusPartialDeliveryPerItemType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusPartnerFunctionType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault)
                    .IsRequired()
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("'b\\'0\\''");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusPriceGroupType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusPriorityType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusReconAccountType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusRiskCategoryType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusRoleType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<OlympusSalesDistrictType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusSalesGroupType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusSalesGroupTypeValidation>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.SalesGroupTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOfficeTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<OlympusSalesOfficeType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusSalesOfficeTypeValidation>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DistributionChannelTypeId).HasColumnType("int(11)");

                entity.Property(e => e.DivisionTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOfficeTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<OlympusSalesOrgType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusShippingConditionsType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusSortKeyType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusTaxClassificationType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusTermsOfPaymentType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("longtext");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusTradingPartnerType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusTransportationZoneType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OlympusTransportationZoneTypeValidation>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CountryTypeValue)
                    .IsRequired()
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.TransportationZoneTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<OlympusUritype>(entity =>
            {
                entity.ToTable("OlympusURIType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(255)");

                entity.Property(e => e.IsDefault).HasColumnType("tinyint(4)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value).HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<PointmanMarketSegmentationType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault)
                    .IsRequired()
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("'b\\'0\\''");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<PointmanPartnerFunctionType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.IsDefault)
                    .IsRequired()
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("'b\\'0\\''");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<PointmanRoleType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<RoleType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.SortOrder).HasColumnType("int(11)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<SalesOrgType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<SicNaicsMapping>(entity =>
            {
                entity.HasKey(e => e.SicCode)
                    .HasName("PRIMARY");

                entity.Property(e => e.SicCode).HasColumnType("varchar(12)");

                entity.Property(e => e.NaicsCode)
                    .IsRequired()
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.NaicsCodeDescription)
                    .IsRequired()
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.SicCodeDescription)
                    .IsRequired()
                    .HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<StaticDataCacheType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.StaticDataKey).HasColumnType("varchar(500)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<SystemType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<Workflow>(entity =>
            {
                entity.HasIndex(e => e.WorkflowId)
                    .HasName("idx_Workflow_WorkflowId");

                entity.HasIndex(e => e.WorkflowRequestorId)
                    .HasName("idx_Workflow_WorkflowRequestorId");

                entity.HasIndex(e => e.WorkflowStateTypeId)
                    .HasName("idx_Workflow_WorkflowStateTypeId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WorkflowRequestorId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowStateTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowBlockCustomer>(entity =>
            {
                entity.HasIndex(e => e.WorkflowId)
                    .HasName("idx_WorkflowBlockCustomer_WorkflowId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CompanyCodeTypeId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.CustomerName).HasColumnType("varchar(200)");

                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasColumnType("longtext");

                entity.Property(e => e.DistributionChannelTypeId).HasColumnType("int(11)");

                entity.Property(e => e.DivisionTypeId).HasColumnType("int(11)");

                entity.Property(e => e.MdmCustomerId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.RoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SystemRecordId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WorkflowTitle)
                    .IsRequired()
                    .HasColumnType("varchar(40)");
            });

            modelBuilder.Entity<WorkflowCreateCustomer>(entity =>
            {
                entity.HasIndex(e => e.WorkflowId)
                    .HasName("idx_WorkflowCreateCustomer_WorkflowId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.Data).HasColumnType("longtext");

                entity.Property(e => e.MdmCustomerId).HasColumnType("varchar(50)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.SystemRecordId).HasColumnType("varchar(50)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WorkflowTitle)
                    .IsRequired()
                    .HasColumnType("varchar(40)");
            });

            modelBuilder.Entity<WorkflowDocument>(entity =>
            {
                entity.HasIndex(e => e.WorkflowId)
                    .HasName("idx_WorkflowDocument_WorkflowId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AttachmentName).HasColumnType("varchar(30)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.DocumentName)
                    .IsRequired()
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.DocumentTypeId).HasColumnType("int(11)");

                entity.Property(e => e.IsActive).HasColumnType("bit(1)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.S3objectKey)
                    .HasColumnName("S3ObjectKey")
                    .HasColumnType("longtext");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowExtendToNewSalesOrg>(entity =>
            {
                entity.HasIndex(e => e.WorkflowId)
                    .HasName("idx_WorkflowExtendToNewSalesOrg_WorkflowId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CompanyCodeTypeId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.CustomerName).HasColumnType("varchar(200)");

                entity.Property(e => e.Deltas)
                    .IsRequired()
                    .HasColumnType("longtext");

                entity.Property(e => e.DistributionChannelTypeId).HasColumnType("int(11)");

                entity.Property(e => e.DivisionTypeId).HasColumnType("int(11)");

                entity.Property(e => e.MdmCustomerId).HasColumnType("varchar(50)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.RoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SourceSalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SystemRecordId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.TargetSalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WorkflowTitle)
                    .IsRequired()
                    .HasColumnType("varchar(40)");
            });

            modelBuilder.Entity<WorkflowFunctionCustomer>(entity =>
            {
                entity.HasIndex(e => e.WorkflowId)
                    .HasName("idx_WorkflowFunctionCustomer_WorkflowId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasColumnType("longtext");

                entity.Property(e => e.FunctionName)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowOther>(entity =>
            {
                entity.HasIndex(e => e.WorkflowId)
                    .HasName("idx_WorkflowOther_WorkflowId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.PurposeOfRequest).HasColumnType("varchar(1000)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WorkflowTitle)
                    .IsRequired()
                    .HasColumnType("varchar(40)");
            });

            modelBuilder.Entity<WorkflowPartners>(entity =>
            {
                entity.HasIndex(e => e.PartnerOneWorkflowId)
                    .HasName("idx_WorkflowPartnerOne_WorkflowId");

                entity.HasIndex(e => e.PartnerTwoWorkflowId)
                    .HasName("idx_WorkflowPartnerTwo_WorkflowId");

                entity.HasIndex(e => e.WorkflowId)
                    .HasName("idx_WorkflowPartner_PartnerWorkflowId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.IsAddPartnerRequest).HasColumnType("bit(1)");

                entity.Property(e => e.IsDefaultPartner).HasColumnType("bit(1)");

                entity.Property(e => e.IsDeleteRequest).HasColumnType("bit(1)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.PartnerOneCustomerNumber).HasColumnType("varchar(50)");

                entity.Property(e => e.PartnerOneDistributionChannelTypeId).HasColumnType("int(11)");

                entity.Property(e => e.PartnerOneDivisionTypeId).HasColumnType("int(11)");

                entity.Property(e => e.PartnerOneRoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.PartnerOneSalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.PartnerOneWorkflowId).HasColumnType("varchar(50)");

                entity.Property(e => e.PartnerTwoCustomerNumber).HasColumnType("varchar(50)");

                entity.Property(e => e.PartnerTwoWorkflowId).HasColumnType("varchar(50)");

                entity.Property(e => e.PartnerTypeId).HasColumnType("int(11)");

                entity.Property(e => e.PurposeOfRequest).HasColumnType("varchar(1000)");

                entity.Property(e => e.RoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId).HasColumnType("varchar(50)");

                entity.Property(e => e.WorkflowTitle).HasColumnType("varchar(40)");
            });

            modelBuilder.Entity<WorkflowRegionMapping>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ContinentCode).HasColumnType("text");

                entity.Property(e => e.CountryCode).HasColumnType("text");

                entity.Property(e => e.Region).HasColumnType("text");

                entity.Property(e => e.State).HasColumnType("text");
            });

            modelBuilder.Entity<WorkflowStateType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<WorkflowTask>(entity =>
            {
                entity.HasIndex(e => e.TeamId)
                    .HasName("idx_WorkflowTask_TeamId");

                entity.HasIndex(e => e.WorkflowId)
                    .HasName("idx_WorkflowTask_WorkflowId");

                entity.HasIndex(e => e.WorkflowTaskStateTypeId)
                    .HasName("idx_WorkflowTask_WorkflowTaskStateTypeId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AssignedUserId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.TeamId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WorkflowTaskNote).HasColumnType("varchar(1000)");

                entity.Property(e => e.WorkflowTaskStateTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowTaskTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowTaskStateType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<WorkflowTaskType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.StatusChangeRuleName).HasColumnType("varchar(100)");

                entity.Property(e => e.TaskCompletionRuleName).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.WorkflowOrder).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowTaskTypeTeam>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AllowBusinessRuleName).HasColumnType("varchar(100)");

                entity.Property(e => e.MdmTeamId).HasColumnType("int(11)");

                entity.Property(e => e.TaskTeamRuleName).HasColumnType("varchar(100)");

                entity.Property(e => e.WorkflowTaskTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowTaskUser>(entity =>
            {
                entity.HasIndex(e => e.UserId)
                    .HasName("idx_WorkflowTaskUser_UserId");

                entity.HasIndex(e => e.WorkflowTaskId)
                    .HasName("idx_WorkflowTaskUser_WorkflowTaskId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowTaskId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CanAutoComplete).HasColumnType("bit(1)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.StatusChangeRuleName).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.WorkflowStartRuleName).HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<WorkflowUpdates>(entity =>
            {
                entity.HasIndex(e => e.WorkflowId)
                    .HasName("idx_WorkflowUpdates_WorkflowId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CompanyCodeTypeId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.Deltas)
                    .IsRequired()
                    .HasColumnType("longtext");

                entity.Property(e => e.DistributionChannelTypeId).HasColumnType("int(11)");

                entity.Property(e => e.DivisionTypeId).HasColumnType("int(11)");

                entity.Property(e => e.MdmCustomerId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.RoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SystemRecordId).HasColumnType("varchar(50)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WorkflowTitle)
                    .IsRequired()
                    .HasColumnType("varchar(40)");
            });
        }
    }
}
