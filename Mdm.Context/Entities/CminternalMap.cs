﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class CminternalMap
    {
        public int Id { get; set; }
        public string SourceField { get; set; }
        public string TargetField { get; set; }
        public string RedshiftTableName { get; set; }
    }
}
