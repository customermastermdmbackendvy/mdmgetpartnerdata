﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class OlympusDeliveringPlantTypeValidation
    {
        public int Id { get; set; }
        public int SalesOrgTypeId { get; set; }
        public int DistributionChannelTypeId { get; set; }
        public int DeliveringPlantTypeId { get; set; }
    }
}
