﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowCreateCustomer
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public string WorkflowTitle { get; set; }
        public int SystemTypeId { get; set; }
        public string MdmCustomerId { get; set; }
        public string SystemRecordId { get; set; }
        public string Data { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
