﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowRegionMapping
    {
        public string State { get; set; }
        public string Region { get; set; }
        public string CountryCode { get; set; }
        public string ContinentCode { get; set; }
        public int Id { get; set; }
    }
}
