﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class MdmNumberMapping
    {
        public int Id { get; set; }
        public string NewMdmNumber { get; set; }
        public string OldMdmNumber { get; set; }
        public DateTime ExpiredDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
