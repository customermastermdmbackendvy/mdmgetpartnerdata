﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class SystemFieldMapping
    {
        public int Id { get; set; }
        public int SystemId { get; set; }
        public int SystemFieldTypeId { get; set; }
        public string TechnicalFieldName { get; set; }
        public string FriendlyFieldName { get; set; }
        public int? MaxLength { get; set; }
        public int? DecimalPlaces { get; set; }
        public bool? IsRequired { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
