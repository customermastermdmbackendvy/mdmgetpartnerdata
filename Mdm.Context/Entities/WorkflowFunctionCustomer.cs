﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowFunctionCustomer
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public string FunctionName { get; set; }
        public string Data { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
