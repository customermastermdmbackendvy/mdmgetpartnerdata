﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowPartners
    {
        public int Id { get; set; }
        public int SystemTypeId { get; set; }
        public string WorkflowId { get; set; }
        public string WorkflowTitle { get; set; }
        public string PurposeOfRequest { get; set; }
        public string PartnerOneWorkflowId { get; set; }
        public string PartnerOneCustomerNumber { get; set; }
        public int? PartnerOneDistributionChannelTypeId { get; set; }
        public int? PartnerOneDivisionTypeId { get; set; }
        public int? PartnerOneRoleTypeId { get; set; }
        public int? PartnerOneSalesOrgTypeId { get; set; }
        public string PartnerTwoWorkflowId { get; set; }
        public string PartnerTwoCustomerNumber { get; set; }
        public int RoleTypeId { get; set; }
        public int PartnerTypeId { get; set; }

        public bool IsAddPartnerRequest { get; set; }
        public bool IsDefaultPartner { get; set; }
        public bool IsDeleteRequest { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
