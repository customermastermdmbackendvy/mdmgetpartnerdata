﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowApolloBlock
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public string Title { get; set; }
        public int SystemTypeId { get; set; }
        public int RoleTypeId { get; set; }
        public string SystemAccountNumber { get; set; }
        public int SalesOrgTypeId { get; set; }
        public int CompanyCodeTypeId { get; set; }
        public bool? BlockPosting { get; set; }
        public int? BlockOrderTypeId { get; set; }
        public int? BlockDeliveryTypeId { get; set; }
        public int? BlockBillingTypeId { get; set; }
        public bool? BlockAllSalesOrg { get; set; }
        public string BlockReason { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
