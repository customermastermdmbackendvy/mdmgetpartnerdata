﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class OlympusDistributionChannelTypeValidation
    {
        public int Id { get; set; }
        public int SalesOrgTypeId { get; set; }
        public int DistributionChannelTypeId { get; set; }
    }
}
