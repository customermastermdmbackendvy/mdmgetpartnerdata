﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowApolloCustomerMaster
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public string License { get; set; }
        public DateTime? LicenseExpDate { get; set; }
        public string SearchTerm1 { get; set; }
        public string SearchTerm2 { get; set; }
        public int CustomerClassTypeId { get; set; }
        public int IndustryCodeTypeId { get; set; }
        public string TransporationZone { get; set; }
        public int IndustryTypeId { get; set; }
        public int CustomerGroupTypeId { get; set; }
        public string TaxNumber2 { get; set; }
        public int ReconAccountTypeId { get; set; }
        public string SortKey { get; set; }
        public string PaymentHistoryRecord { get; set; }
        public string PaymentMethods { get; set; }
        public string AcctgClerk { get; set; }
        public string AccountStatement { get; set; }
        public int SalesOfficeTypeId { get; set; }
        public int PpcustProcTypeId { get; set; }
        public int CustomerPriceProcTypeId { get; set; }
        public int PriceListTypeId { get; set; }
        public int AccountTypeId { get; set; }
        public int DeliveryPriorityTypeId { get; set; }
        public int ShippingConditionsTypeId { get; set; }
        public bool OrderCombination { get; set; }
        public int Incoterms1TypeId { get; set; }
        public string Incoterms2 { get; set; }
        public int AcctAssignmentGroupTypeId { get; set; }
        public string TaxClassification { get; set; }
        public int? PartnerFunctionTypeId { get; set; }
        public string PartnerFunctionNumber { get; set; }
        public int ShippingCustomerTypeId { get; set; }
        public string AdditionalNotes { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
