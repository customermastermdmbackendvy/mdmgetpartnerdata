﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class Integrations
    {
        public int Id { get; set; }
        public string Identifier { get; set; }
        public string Type { get; set; }
        public string PrimaryId { get; set; }
        public string Data { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
