﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowOther
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public string WorkflowTitle { get; set; }
        public int SystemTypeId { get; set; }
        public string PurposeOfRequest { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
