﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowApolloCredit
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public int? PaymentTermsTypeId { get; set; }
        public decimal? CreditLimit { get; set; }
        public int? RiskCategoryTypeId { get; set; }
        public int? CreditRepGroupTypeId { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactTelephone { get; set; }
        public string ContactFax { get; set; }
        public string ContactEmail { get; set; }
        public string CredInfoNumber { get; set; }
        public string PaymentIndex { get; set; }
        public string LastExtReview { get; set; }
        public string Rating { get; set; }
        public string AdditionalNotes { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
