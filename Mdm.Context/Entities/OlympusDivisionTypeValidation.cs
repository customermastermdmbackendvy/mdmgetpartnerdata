﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class OlympusDivisionTypeValidation
    {
        public int Id { get; set; }
        public int SalesOrgTypeId { get; set; }
        public int DistributionChannelTypeId { get; set; }
        public int DivisionTypeId { get; set; }
    }
}
