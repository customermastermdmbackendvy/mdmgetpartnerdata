﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class OlympusSalesGroupTypeValidation
    {
        public int Id { get; set; }
        public int SalesOfficeTypeId { get; set; }
        public int SalesGroupTypeId { get; set; }
    }
}
