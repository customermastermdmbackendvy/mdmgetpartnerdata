﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class MdmTeamUser
    {
        public int Id { get; set; }
        public int MdmTeamId { get; set; }
        public int MdmUserId { get; set; }
    }
}
