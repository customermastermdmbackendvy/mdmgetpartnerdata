﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowTask
    {
        public int Id { get; set; }
        public int WorkflowTaskTypeId { get; set; }
        public string WorkflowId { get; set; }
        public int TeamId { get; set; }
        public int? AssignedUserId { get; set; }
        public int WorkflowTaskStateTypeId { get; set; }
        public string WorkflowTaskNote { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
