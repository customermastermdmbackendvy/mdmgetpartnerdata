﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowTaskUser
    {
        public int Id { get; set; }
        public int WorkflowTaskId { get; set; }
        public int UserId { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
