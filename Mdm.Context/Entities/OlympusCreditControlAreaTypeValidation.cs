﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class OlympusCreditControlAreaTypeValidation
    {
        public int Id { get; set; }
        public int SalesOrgTypeId { get; set; }
        public int CreditControlAreaTypeId { get; set; }
    }
}
