﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class OlympusMarketingSegmentationType
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public int? SortOrder { get; set; }
        public bool? IsDefault { get; set; }
    }
}
