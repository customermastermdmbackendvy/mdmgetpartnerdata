﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class OlympusCompanyCodeTypeValidation
    {
        public int Id { get; set; }
        public int SalesOrgTypeId { get; set; }
        public int CompanyCodeTypeId { get; set; }
    }
}
