﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class MdmTeam
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
    }
}
