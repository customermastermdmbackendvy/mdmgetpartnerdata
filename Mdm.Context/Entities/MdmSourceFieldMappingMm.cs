﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class MdmSourceFieldMappingMm
    {
        public int Id { get; set; }
        public int? SystemId { get; set; }
        public string OperationName { get; set; }
        public string SourceKeyField { get; set; }
        public string SourceFieldName { get; set; }
        public string BusinessFieldName { get; set; }
        public string RedshiftFieldName { get; set; }
        public string RedShiftTableName { get; set; }
        public sbyte? RedshiftKeyField { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
