﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowApolloPricing
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public int? SpecialPricingTypeId { get; set; }
        public int? DistLevelTypeId { get; set; }
        public string AdditionalNotes { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
