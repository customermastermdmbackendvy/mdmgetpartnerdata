﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class OlympusTransportationZoneTypeValidation
    {
        public int Id { get; set; }
        public string CountryTypeValue { get; set; }
        public int TransportationZoneTypeId { get; set; }
    }
}
