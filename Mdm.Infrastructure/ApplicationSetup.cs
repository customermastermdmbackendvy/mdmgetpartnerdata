﻿using Amazon.Lambda.Core;
using AutoMapper;
using Mdm.Context.Entities;
using Mdm.Domain.Entities;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Domain.Interfaces.Repositories;
using Mdm.Infrastructure.Managers;
using Mdm.Infrastructure.Repositories;
using Mdm.Services;
using Mdm.Services.Interfaces;
using Mdm.Services.Models;
using Mdm.WorkflowEngine.Helpers.Constants;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Mdm.Infrastructure
{
    public class ApplicationSetup
    {
        private ServiceCollection _services;
        private IMapper _serviceMapper;

        private IMapper _repoMapper;
        public IServiceProvider ServiceProvider { get; private set; }

        private static readonly Lazy<ApplicationSetup> Instancelock = new Lazy<ApplicationSetup>(() => new ApplicationSetup());

        #region Private Constructor
        private ApplicationSetup()
        {
            LambdaLogger.Log("Instatiating Application Setup.");
        }
        #endregion

        #region Public Methods
        public static ApplicationSetup Instance
        {
            get
            {
                return Instancelock.Value;
            }
        }

        public ApplicationSetup BuildStaticData()
        {
            var staticDataRepo = ServiceProvider.GetRequiredService<IStaticDataRepository>();
            StaticTypeData.LoadStaticData(staticDataRepo);
            return this;
        }

        public ApplicationSetup BuildMappers()
        {
            _repoMapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Workflow, WorkflowDomain>();
                cfg.CreateMap<WorkflowType, WorkflowTypeDomain>();
                cfg.CreateMap<WorkflowUpdates, WorkflowUpdatesDomain>();
                cfg.CreateMap<WorkflowFunctionCustomer, WorkflowFunctionCustomerDomain>();
                cfg.CreateMap<WorkflowCreateCustomer, WorkflowCreateCustomerDomain>();
                cfg.CreateMap<WorkflowExtendToNewSalesOrg, WorkflowExtendToNewSalesOrgDomain>();
                cfg.CreateMap<WorkflowTask, WorkflowTaskDomain>();
                cfg.CreateMap<CategoryType, CategoryTypeDomain>();
                cfg.CreateMap<MdmTeam, MdmTeamDomain>();
                cfg.CreateMap<WorkflowDocument, WorkflowDocumentDomain>();
                cfg.CreateMap<WorkflowBlockCustomer, WorkflowBlockCustomerDomain>();
                cfg.CreateMap<ApolloTaxClassificationType, ApolloTaxClassificationTypeDomain>();
                cfg.CreateMap<ApolloAccountStatementType, ApolloAccountStatementTypeDomain>();
                cfg.CreateMap<MdmUserDomain, MdmUser>();
                cfg.CreateMap<MdmUser, MdmUserDomain>();
                cfg.CreateMap<RoleType, RoleTypeDomain>();
                cfg.CreateMap<SalesOrgType, StaticDataTypeDomain>();
                cfg.CreateMap<ApolloCustomerClassType, ApolloCustomerClassTypeDomain>();
                cfg.CreateMap<ApolloIndustryCodeType, ApolloIndustryCodeTypeDomain>();
                cfg.CreateMap<ApolloCompanyCodeType, ApolloCompanyCodeTypeDomain>();
                cfg.CreateMap<ApolloIndustryType, ApolloIndustryTypeDomain>();
                cfg.CreateMap<ApolloMarketingSegmentationType, ApolloMarketingSegmentationTypeDomain>();
                cfg.CreateMap<ApolloReconAccountType, ApolloReconAccountTypeDomain>();
                cfg.CreateMap<ApolloSalesOfficeType, ApolloSalesOfficeTypeDomain>();
                cfg.CreateMap<ApolloCustomerGroupType, ApolloCustomerGroupTypeDomain>();
                cfg.CreateMap<ApolloPpcustProcType, ApolloPpcustProcTypeDomain>();
                cfg.CreateMap<ApolloCustomerPriceProcType, ApolloCustomerPriceProcTypeDomain>();
                cfg.CreateMap<ApolloPriceListType, ApolloPriceListTypeDomain>();
                cfg.CreateMap<ApolloDeliveryPriorityType, ApolloDeliveryPriorityTypeDomain>();
                cfg.CreateMap<ApolloIncoTermsType, ApolloIncoTermsTypeDomain>();
                cfg.CreateMap<ApolloAcctAssignmentGroupType, ApolloAcctAssignmentGroupTypeDomain>();
                cfg.CreateMap<ApolloAccountType, ApolloAccountTypeDomain>();
                cfg.CreateMap<ApolloShippingCustomerType, ApolloShippingCustomerTypeDomain>();
                cfg.CreateMap<ApolloPaymentTermsType, ApolloPaymentTermsTypeDomain>();
                cfg.CreateMap<WorkflowStateType, WorkflowStateTypeDomain>();
                cfg.CreateMap<ApolloRiskCategoryType, ApolloRiskCategoryTypeDomain>();
                cfg.CreateMap<ApolloShippingConditionsType, ApolloShippingConditionsTypeDomain>();
                cfg.CreateMap<ApolloCreditRepGroupType, ApolloCreditRepGroupTypeDomain>();
                cfg.CreateMap<ApolloDistributionChannelType, StaticDataTypeDomain>();
                cfg.CreateMap<ApolloDivisionType, StaticDataTypeDomain>();
                cfg.CreateMap<ApolloDistLevelType, ApolloDistLevelTypeDomain>();
                cfg.CreateMap<ApolloSpecialPricingType, ApolloSpecialPricingTypeDomain>();
                cfg.CreateMap<ApolloPartnerFunctionType, StaticDataTypeDomain>();
                cfg.CreateMap<ApolloDapapriceType, ApolloDAPAPriceTypeDomain>();
                cfg.CreateMap<ApolloDapa2priceType, ApolloDAPA2PriceTypeDomain>();
                cfg.CreateMap<ApolloFssPriceType, ApolloFssPriceTypeDomain>();
                cfg.CreateMap<ApolloBlockBillingType, StaticDataTypeDomain>();
                cfg.CreateMap<ApolloBlockDeliveryType, StaticDataTypeDomain>();
                cfg.CreateMap<ApolloBlockOrderType, StaticDataTypeDomain>();
                cfg.CreateMap<ApolloPartnerFunctionType, StaticDataTypeDomain>();

                cfg.CreateMap<OlympusDistributionChannelType, StaticDataTypeDomain>();
                cfg.CreateMap<OlympusDivisionType, StaticDataTypeDomain>();
                cfg.CreateMap<OlympusPartnerFunctionType, StaticDataTypeDomain>();
                cfg.CreateMap<OlympusSalesOrgType, StaticDataTypeDomain>();

                cfg.CreateMap<JdePartnerFunctionType, StaticDataTypeDomain>();

                cfg.CreateMap<RoleType, StaticDataTypeDomain>();
                cfg.CreateMap<OlympusRoleType, StaticDataTypeDomain>();
                cfg.CreateMap<JderoleType, StaticDataTypeDomain>();

                cfg.CreateMap<WorkflowPartners, WorkflowPartnersDomain>();
            }).CreateMapper();

            _serviceMapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SapApolloCustomerDomain, SapApolloCustomerModel>();
                cfg.CreateMap<RedshiftPartnerDataDomain, PartnerDataResult>();
                cfg.CreateMap<SapOlympusCustomerDomain, SapOlympusCustomerModel>();
                cfg.CreateMap<WorkflowDocumentDomain, DocumentModel>();
            }).CreateMapper();

            return this;
        }

        public ApplicationSetup SetupDependencies()
        {
            _services = new ServiceCollection();
            ConfigureServices();
            ServiceProvider = _services.BuildServiceProvider();
            return this;
        }
        #endregion

        #region Helpers
        private void ConfigureServices()
        {
            _services.AddScoped<ISecretsManager, SecretsManager>();
            _services.AddSingleton<IConnectionManager, ConnectionManager>();
            _services.AddSingleton<IApiConnectionManager, ApiConnectionManager>();
            _services.AddScoped<IHttpManager, HttpManager>();
            _services.AddScoped<IPartnerDataRepository, PartnerDataRepository>(cr => new PartnerDataRepository(cr.GetRequiredService<IConnectionManager>(), _repoMapper));
            _services.AddScoped<IWorkflowRepository, WorkflowRepository>(cr => new WorkflowRepository(cr.GetRequiredService<IConnectionManager>(), _repoMapper));
            _services.AddScoped<IUserRepository, UserRepository>(cr => new UserRepository(cr.GetRequiredService<IConnectionManager>(), _repoMapper));
            _services.AddScoped<IStaticDataRepository, StaticDataRepository>(cr => new StaticDataRepository(cr.GetRequiredService<IConnectionManager>(), _repoMapper));

            _services.AddScoped<IPartnerDataService, PartnerDataService>(cs => new PartnerDataService(
                cs.GetRequiredService<IPartnerDataRepository>(),
                cs.GetRequiredService<IWorkflowRepository>(),
                cs.GetRequiredService<IUserRepository>(),
                cs.GetRequiredService<IStaticDataRepository>(),
                cs.GetRequiredService<IHttpManager>(),
                 _serviceMapper));
        }
        #endregion
    }
}
