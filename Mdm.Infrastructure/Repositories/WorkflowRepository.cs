﻿using AutoMapper;
using Mdm.Context.Entities;
using Mdm.Domain.Entities;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mdm.Infrastructure.Repositories
{
    public class WorkflowRepository : BaseRepository, IWorkflowRepository
    {
        public WorkflowRepository(IConnectionManager connectionManager, IMapper mapper) : base(connectionManager, mapper)
        {
        }

        public async Task<WorkflowTaskDomain> GetWorkflowTaskByTeamAsync(int taskId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowTasks = mdmContext.WorkflowTask.AsQueryable();
                return Mapper.Map<WorkflowTask, WorkflowTaskDomain>(await workflowTasks.Where(x => x.Id == taskId).FirstOrDefaultAsync());
            }
        }

        public async Task<List<WorkflowTaskDomain>> GetWorkflowTaskAsync(string WorkflowId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowTasks = mdmContext.WorkflowTask.AsQueryable();
                return Mapper.Map<List<WorkflowTask>, List<WorkflowTaskDomain>>(await workflowTasks.Where(x => x.WorkflowId == WorkflowId).ToListAsync());
            }
        }

        public async Task<IEnumerable<WorkflowDomain>> GetWorkflowsAsync(IEnumerable<string> workflowIds)
        {
            if (workflowIds == null || !workflowIds.Any())
            {
                return new List<WorkflowDomain>();
            }
            using (var mdmContext = GetContext())
            {
                var workflows = mdmContext.Workflow.AsQueryable();

                // Filter Workflow Ids
                workflows = workflows.Where(x => workflowIds.Contains(x.WorkflowId));
                return Mapper.Map<IEnumerable<Workflow>, IEnumerable<WorkflowDomain>>(await workflows.ToListAsync());
            }
        }
        public async Task<WorkflowTypeDomain> GetWorkflowTypeAsync(int workflowTypeId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowTypes = mdmContext.WorkflowType.AsQueryable();
                return Mapper.Map<WorkflowType, WorkflowTypeDomain>(await workflowTypes.Where(x => x.Id == workflowTypeId).FirstOrDefaultAsync());
            }
        }

        public async Task<List<MdmTeamDomain>> GetAllTeamsAsync()
        {
            using (var mdmContext = GetContext())
            {
                return Mapper.Map<List<MdmTeam>, List<MdmTeamDomain>>(await mdmContext.MdmTeam.ToListAsync());
            }
        }

        public async Task<List<WorkflowDocumentDomain>> GetWorkflowDocumentByWorkflowID(string workFlowId)
        {
            if (workFlowId == null)
            {
                return null;
            }
            using (var mdmContext = GetContext())
            {
                var workflowDocuments = await mdmContext.WorkflowDocument.Where(x => x.WorkflowId == workFlowId && x.IsActive == true).ToListAsync();
                return Mapper.Map<List<WorkflowDocument>, List<WorkflowDocumentDomain>>(workflowDocuments);
            }
        }
    }
}
