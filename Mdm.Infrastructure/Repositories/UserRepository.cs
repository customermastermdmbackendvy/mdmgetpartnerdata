﻿using AutoMapper;
using Mdm.Domain.Entities;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mdm.Infrastructure.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(IConnectionManager connectionManager, IMapper mapper) : base(connectionManager, mapper)
        {
        }

        public async Task<MdmUserDomain> GetmdmUserByRequestorId(int RequestorId)
        {
            using (var mdmContext = GetContext())
            {
                var mdmUser = await mdmContext.MdmUser.Where(x => x.Id == RequestorId).FirstOrDefaultAsync();
                return Mapper.Map<MdmUserDomain>(mdmUser);
            }
        }
        public async Task<List<MdmUserDomain>> GetMdmUser(int? teamId, int? UserId)
        {
            using (var mdmContext = GetContext())
            {
                if (teamId == null && UserId.HasValue)
                {
                    var mdmUser = await mdmContext.MdmUser.Where(x => x.Id == UserId).ToListAsync();
                    return Mapper.Map<List<MdmUserDomain>>(mdmUser);
                }
                else
                {
                    var mdmTeamUser = mdmContext.MdmTeamUser.Where(x => x.MdmTeamId == teamId);
                    var mdmUser = await mdmContext.MdmUser.Where(x => mdmTeamUser.Any(y => y.MdmUserId == x.Id)).ToListAsync();
                    return Mapper.Map<List<MdmUserDomain>>(mdmUser);
                }

            }
        }
    }
}
