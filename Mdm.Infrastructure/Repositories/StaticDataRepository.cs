﻿using AutoMapper;
using Mdm.Domain.Entities;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mdm.Infrastructure.Repositories
{
    public class StaticDataRepository : BaseRepository, IStaticDataRepository
    {
        public StaticDataRepository(IConnectionManager connectionManager, IMapper mapper) : base(connectionManager, mapper)
        {
        }

        public async Task<List<WorkflowTypeDomain>> GetWorkflowTypeAsync()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.WorkflowType.ToListAsync();
                return Mapper.Map<List<WorkflowTypeDomain>>(staticData);
            }
        }
        public async Task<List<RoleTypeDomain>> GetApolloRoles()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.RoleType.ToListAsync();
                return Mapper.Map<List<RoleTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetSalesOrgs()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.SalesOrgType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetOlympusSalesOrgTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.OlympusSalesOrgType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetOlympusDistributionChannelTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.OlympusDistributionChannelType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetOlympusDivisionTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.OlympusDivisionType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetOlympusPartnerFunctionTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.OlympusPartnerFunctionType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetJdePartnerFunctionTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.JdePartnerFunctionType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloCustomerClassTypeDomain>> GetApolloCustomerClassTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloCustomerClassType.ToListAsync();
                return Mapper.Map<List<ApolloCustomerClassTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloIndustryCodeTypeDomain>> GetApolloIndustryCodeTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloIndustryCodeType.ToListAsync();
                return Mapper.Map<List<ApolloIndustryCodeTypeDomain>>(staticData);
            }
        }
        public async Task<List<ApolloCompanyCodeTypeDomain>> GetApolloCompanyCodeTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloCompanyCodeType.ToListAsync();
                return Mapper.Map<List<ApolloCompanyCodeTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloMarketingSegmentationTypeDomain>> GetApolloMarketingSegmentationTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloMarketingSegmentationType.ToListAsync();
                return Mapper.Map<List<ApolloMarketingSegmentationTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloReconAccountTypeDomain>> GetApolloReconAccountTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloReconAccountType.ToListAsync();
                return Mapper.Map<List<ApolloReconAccountTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloSalesOfficeTypeDomain>> GetApolloSalesOfficeTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloSalesOfficeType.ToListAsync();
                return Mapper.Map<List<ApolloSalesOfficeTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloCustomerGroupTypeDomain>> GetApolloCustomerGroupTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloCustomerGroupType.ToListAsync();
                return Mapper.Map<List<ApolloCustomerGroupTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloPpcustProcTypeDomain>> GetApolloPpcustProcTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloPpcustProcType.ToListAsync();
                return Mapper.Map<List<ApolloPpcustProcTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloCustomerPriceProcTypeDomain>> GetApolloCustomerPriceProcTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloCustomerPriceProcType.ToListAsync();
                return Mapper.Map<List<ApolloCustomerPriceProcTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloPriceListTypeDomain>> GetApolloPriceListTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloPriceListType.ToListAsync();
                return Mapper.Map<List<ApolloPriceListTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloDeliveryPriorityTypeDomain>> GetApolloDeliveryPriorityTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloDeliveryPriorityType.ToListAsync();
                return Mapper.Map<List<ApolloDeliveryPriorityTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloIncoTermsTypeDomain>> GetApolloIncoTermsTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloIncoTermsType.ToListAsync();
                return Mapper.Map<List<ApolloIncoTermsTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloAcctAssignmentGroupTypeDomain>> GetApolloAcctAssignmentGroupTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloAcctAssignmentGroupType.ToListAsync();
                return Mapper.Map<List<ApolloAcctAssignmentGroupTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloAccountTypeDomain>> GetApolloAccountTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloAccountType.ToListAsync();
                return Mapper.Map<List<ApolloAccountTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloShippingCustomerTypeDomain>> GetApolloShippingCustomerTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloShippingCustomerType.ToListAsync();
                return Mapper.Map<List<ApolloShippingCustomerTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloPaymentTermsTypeDomain>> GetApolloPaymentTermsTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloPaymentTermsType.ToListAsync();
                return Mapper.Map<List<ApolloPaymentTermsTypeDomain>>(staticData);
            }
        }

        public async Task<List<WorkflowStateTypeDomain>> GetWorkflowStateTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.WorkflowStateType.ToListAsync();
                return Mapper.Map<List<WorkflowStateTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloRiskCategoryTypeDomain>> GetApolloRiskCategoryTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloRiskCategoryType.ToListAsync();
                return Mapper.Map<List<ApolloRiskCategoryTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloShippingConditionsTypeDomain>> GetApolloShippingConditionsTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloShippingConditionsType.ToListAsync();
                return Mapper.Map<List<ApolloShippingConditionsTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloCreditRepGroupTypeDomain>> GetApolloCreditRepGroupTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloCreditRepGroupType.ToListAsync();
                return Mapper.Map<List<ApolloCreditRepGroupTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetApolloDistributionChannelTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloDistributionChannelType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetApolloDivisionTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloDivisionType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetApolloPartnerFunctionTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloPartnerFunctionType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloDistLevelTypeDomain>> GetApolloDistLevelTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloDistLevelType.ToListAsync();
                return Mapper.Map<List<ApolloDistLevelTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloSpecialPricingTypeDomain>> GetApolloSpecialPricingTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloSpecialPricingType.ToListAsync();
                return Mapper.Map<List<ApolloSpecialPricingTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloDAPAPriceTypeDomain>> GetApolloDAPAPriceTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloDapapriceType.ToListAsync();
                return Mapper.Map<List<ApolloDAPAPriceTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloDAPA2PriceTypeDomain>> GetApolloDapa2priceTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloDapa2priceType.ToListAsync();
                return Mapper.Map<List<ApolloDAPA2PriceTypeDomain>>(staticData);
            }
        }

        public async Task<List<ApolloFssPriceTypeDomain>> GetApolloFssPriceTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloFssPriceType.ToListAsync();
                return Mapper.Map<List<ApolloFssPriceTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetApolloBlockBillingTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloBlockBillingType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetApolloBlockDeliveryTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloBlockDeliveryType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetApolloBlockOrderTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.ApolloBlockOrderType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetApolloRoleTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.RoleType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetOlympusRoleTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.OlympusRoleType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }

        public async Task<List<StaticDataTypeDomain>> GetJdeRoleTypes()
        {
            using (var mdmContext = GetContext())
            {
                var staticData = await mdmContext.JderoleType.ToListAsync();
                return Mapper.Map<List<StaticDataTypeDomain>>(staticData);
            }
        }
    }
}
