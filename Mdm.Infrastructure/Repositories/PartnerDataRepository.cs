﻿using Amazon.Lambda.Core;
using AutoMapper;
using Dapper;
using Mdm.Context.Entities;
using Mdm.Domain.Entities;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Mdm.Infrastructure.Repositories
{
    public class PartnerDataRepository : BaseRepository, IPartnerDataRepository
    {
        private IConnectionManager _connectionManager;

        public PartnerDataRepository(IConnectionManager connectionManager, IMapper mapper) : base(connectionManager, mapper)
        {
            _connectionManager = connectionManager;
        }

        public async Task<List<WorkflowPartnersDomain>> GetWorkflowPartnersByWorkflowId(string workflowId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowPartners = await mdmContext.WorkflowPartners.Where(x => x.WorkflowId == workflowId).ToListAsync();
                return Mapper.Map<List<WorkflowPartners>, List<WorkflowPartnersDomain>>(workflowPartners);
            }
        }

        public async Task<List<WorkflowPartnersDomain>> GetWorkflowPartnersByPartnerOneWorkflowId(string partnerOneWorkflowId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowPartners = await mdmContext.WorkflowPartners.Where(x => x.PartnerOneWorkflowId == partnerOneWorkflowId).ToListAsync();
                return Mapper.Map<List<WorkflowPartners>, List<WorkflowPartnersDomain>>(workflowPartners);
            }
        }

        public async Task<List<WorkflowPartnersDomain>> GetWorkflowPartnersByPartnerOneCustomerId(string partnerOneCustomerId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowPartners = await mdmContext.WorkflowPartners.Where(x => x.PartnerOneCustomerNumber == partnerOneCustomerId).ToListAsync();
                return Mapper.Map<List<WorkflowPartners>, List<WorkflowPartnersDomain>>(workflowPartners);
            }
        }

        public async Task<WorkflowUpdatesDomain> GetUpdatesWorkflow(string workflowId, int systemTypeId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowUpdates = await mdmContext.WorkflowUpdates.Where(x => x.WorkflowId == workflowId && x.SystemTypeId == systemTypeId).FirstOrDefaultAsync();
                return Mapper.Map<WorkflowUpdatesDomain>(workflowUpdates);
            }
        }

        public async Task<WorkflowExtendToNewSalesOrgDomain> GetExtendToSalesOrgWorkflow(string workflowId, int systemTypeId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowExtendToSalesOrg = await mdmContext.WorkflowExtendToNewSalesOrg.Where(x => x.WorkflowId == workflowId && x.SystemTypeId == systemTypeId).FirstOrDefaultAsync();
                return Mapper.Map<WorkflowExtendToNewSalesOrgDomain>(workflowExtendToSalesOrg);
            }
        }

        public async Task<WorkflowCreateCustomerDomain> GetWorkflowCreateCustomer(string workflowId, int systemTypeId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowCreateCUstomer = await mdmContext.WorkflowCreateCustomer.Where(x => x.WorkflowId == workflowId && x.SystemTypeId == systemTypeId).FirstOrDefaultAsync();
                return Mapper.Map<WorkflowCreateCustomerDomain>(workflowCreateCUstomer);
            }
        }

        public async Task<WorkflowBlockCustomerDomain> GetWorkflowBlockCustomer(string workflowId, int systemTypeId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowBlockCustomer = await mdmContext.WorkflowBlockCustomer.Where(x => x.WorkflowId == workflowId && x.SystemTypeId == systemTypeId).FirstOrDefaultAsync();
                return Mapper.Map<WorkflowBlockCustomerDomain>(workflowBlockCustomer);
            }
        }

        public async Task<MdmMappingDomain> RetrieveMdmMappingDomainByOldMdmNumber(string oldMdmNumber)
        {
            DbConnection existingConnection = null;
            try
            {
                string selectQuery = @"select NewMdmNumber,OldMdmNumber,ExpiredDate,CreatedDate from MdmNumberMapping where OldMdmNumber=@oldMdmNumber; ";
                using (var mdmContext = GetContext())
                {
                    existingConnection = mdmContext.Database.GetDbConnection();
                    return await existingConnection.QueryFirstOrDefaultAsync<MdmMappingDomain>(selectQuery, new { oldMdmNumber }, commandType: CommandType.Text);
                }
            }
            catch (Exception)
            {
                if (existingConnection != null)
                    existingConnection.Close();
                return null;
            }
        }

        public async Task<List<WorkflowFunctionCustomerDomain>> RetrieveWorkflowFunctionCustomerData(string workflowId)
        {
            using (var mdmDbContext = GetContext())
            {
                var customers = mdmDbContext.WorkflowFunctionCustomer.Where(x => x.WorkflowId == workflowId);
                return Mapper.Map<List<WorkflowFunctionCustomer>, List<WorkflowFunctionCustomerDomain>>(await customers.ToListAsync());
            }
        }

        public async Task<bool> ReadRedShiftForColdStart()
        {
            try
            {
                using (var connection = new NpgsqlConnection(_connectionManager.RedshiftConnectionString))
                {
                    await connection.OpenAsync();

                    string query = "select top 1 source_id from mdm_customer_master ";
                    using (var cmd = new NpgsqlCommand(query, connection))
                    {
                        var dr = await cmd.ExecuteReaderAsync();
                    }
                    connection.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error reading redshift customer master data-" + ex.Message);
                return false;
            }
        }

        public async Task<List<RedshiftCustomerMasterDomain>> ReadRedShift(string mdmNumber)
        {
            try
            {
                var redshiftCustomerMasterDomain = new List<RedshiftCustomerMasterDomain>();
                using (var connection = new NpgsqlConnection(_connectionManager.RedshiftConnectionString))
                {
                    await connection.OpenAsync();

                    string query = "select * from mdm_customer_master where globalmdm = @mdmNumber";
                    using (var cmd = new NpgsqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("mdmNumber", mdmNumber);
                        var dr = await cmd.ExecuteReaderAsync();
                        redshiftCustomerMasterDomain = ToCustomList<RedshiftCustomerMasterDomain>(dr);
                    }
                    connection.Close();
                }
                return redshiftCustomerMasterDomain;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error reading redshift customer master data-" + ex.Message);
                return null;
            }
        }

        public async Task<List<RedshiftCustomerMasterDomain>> ReadRedShiftByCustomerNumberSource(string customerNumber, string source)
        {
            try
            {
                var redshiftCustomerMasterDomain = new List<RedshiftCustomerMasterDomain>();
                using (var connection = new NpgsqlConnection(_connectionManager.RedshiftConnectionString))
                {
                    await connection.OpenAsync();

                    string query = "select * from mdm_customer_master where customer_number = @customerNumber and source_id=@source";
                    using (var cmd = new NpgsqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("customerNumber", customerNumber);
                        cmd.Parameters.AddWithValue("source", source);
                        var dr = await cmd.ExecuteReaderAsync();
                        redshiftCustomerMasterDomain = ToCustomList<RedshiftCustomerMasterDomain>(dr);
                    }
                    connection.Close();
                }
                return redshiftCustomerMasterDomain;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error reading redshift customer master data-" + ex.Message);
                return null;
            }
        }

        public async Task<RedshiftCustomerMasterDomain> ReadMdmRepoCustomerByCustomerDetail(string customerNumber, string source, string salesOrg, string division, string distributionChannelType)
        {
            try
            {
                var redshiftCustomerMasterDomain = new List<RedshiftCustomerMasterDomain>();
                using (var connection = new NpgsqlConnection(_connectionManager.RedshiftConnectionString))
                {
                    await connection.OpenAsync();

                    string query = "select * from mdm_customer_master where customer_number = @customerNumber and source_id=@source and sales_org = @salesOrg and division = @division and distribution_channel = @distributionChannelType";
                    using (var cmd = new NpgsqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("customerNumber", customerNumber);
                        cmd.Parameters.AddWithValue("source", source);
                        cmd.Parameters.AddWithValue("salesOrg", salesOrg);
                        cmd.Parameters.AddWithValue("division", division);
                        cmd.Parameters.AddWithValue("distributionChannelType", distributionChannelType);
                        var dr = await cmd.ExecuteReaderAsync();
                        redshiftCustomerMasterDomain = ToCustomList<RedshiftCustomerMasterDomain>(dr);
                    }
                    connection.Close();
                }
                return redshiftCustomerMasterDomain.Any() ? redshiftCustomerMasterDomain[0]: null;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error reading redshift customer master data-" + ex.Message);
                return null;
            }
        }

        public async Task<RedshiftCustomerMasterDomain> ReadRedShiftByCustomerNumberAndSource(string customerNumber, string source)
        {
            try
            {
                var redshiftCustomerMasterDomain = new List<RedshiftCustomerMasterDomain>();
                using (var connection = new NpgsqlConnection(_connectionManager.RedshiftConnectionString))
                {
                    await connection.OpenAsync();

                    string query = "select * from mdm_customer_master where customer_number = @customerNumber and source_id=@source";
                    using (var cmd = new NpgsqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("customerNumber", customerNumber);
                        cmd.Parameters.AddWithValue("source", source);
                        var dr = await cmd.ExecuteReaderAsync();
                        redshiftCustomerMasterDomain = ToCustomList<RedshiftCustomerMasterDomain>(dr);
                    }
                    connection.Close();
                }
                return redshiftCustomerMasterDomain.Any() ? redshiftCustomerMasterDomain[0] : null; 
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error reading redshift customer master data-" + ex.Message);
                return null;
            }
        }

        public async Task<string> GetMdmNumberfromCustomerNumber(string customerNumber)
        {
            try
            {
                var redshiftCustomerMasterDomain = new List<RedshiftCustomerMasterDomain>();
                using (var connection = new NpgsqlConnection(_connectionManager.RedshiftConnectionString))
                {
                    await connection.OpenAsync();

                    string query = "select * from mdm_customer_master where customer_number = @customerNumber";
                    using (var cmd = new NpgsqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("customerNumber", customerNumber);
                        var dr = await cmd.ExecuteReaderAsync();
                        redshiftCustomerMasterDomain = ToCustomList<RedshiftCustomerMasterDomain>(dr);
                    }
                    connection.Close();
                }
                return redshiftCustomerMasterDomain?.FirstOrDefault()?.globalmdm;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error reading redshift customer master data-" + ex.Message);
                return null;
            }
        }

        public async Task<List<RedshiftPartnerDataDomain>> ReadRedShiftPointmanBillTo(string customerNumber,String customerName="",string address="",string city="",string role="")
        {
            try
            {
                //        address1, city, state, country postal1
                var redshiftPartnerDataDomain = new List<RedshiftPartnerDataDomain>();
                using (var connection = new NpgsqlConnection(_connectionManager.RedshiftConnectionString))
                {
                    await connection.OpenAsync();

                    string query = "select distinct P.assignedmdm, P.customer_number as BillToCustomerNumber, " +
                        "P.Address_number as ShipToCustomerNumber,P.partner_func as PartnerFunc, A.name as CustomerName " +
                        ",A.address1 as Address1, A.city as City, A.state as State, A.country as Country, A.postal1 as Postal " +
                        "from mdm_cm_partner_func P " +
                        "inner join mdm_cm_aggregate A on P.assignedmdm = A.assignedmdm " +
                        "where P.customer_number = @customerNumber and P.source_id = 'PTMN' and P.partner_func <> 'SOLDTO' ";
                    if(customerName!="")
                    {
                        query = query + "and LOWER(A.name) like '%" + customerName.ToLower()+"%' ";
                    }
					if (address != "")
					{
						query = query + "and LOWER(A.address1) like '%" + address.ToLower() + "%' ";
					}
					if (city != "")
					{
						query = query + "and LOWER(A.city) like '%" + city.ToLower() + "%' ";
					}
					if (role != "")
					{
						query = query + "and LOWER(P.partner_func) like '%" + role.ToLower() + "%' ";
					}

					string orderby= query+"order by BillToCustomerNumber, ShipToCustomerNumber";
                    using (var cmd = new NpgsqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("customerNumber", customerNumber);
                        var dr = await cmd.ExecuteReaderAsync();
                        redshiftPartnerDataDomain = ToCustomList<RedshiftPartnerDataDomain>(dr);
                    }
                    connection.Close();
                }
                return redshiftPartnerDataDomain;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error reading redshift partner data-" + ex.Message);
                return null;
            }
        }

        public async Task<List<RedshiftPartnerDataDomain>> ReadRedShiftPointmanShipTo(string mdmNumber, string customerNumber)
        {
            try
            {
                var redshiftPartnerDataDomain = new List<RedshiftPartnerDataDomain>();
                using (var connection = new NpgsqlConnection(_connectionManager.RedshiftConnectionString))
                {
                    await connection.OpenAsync();

                    string query = "select distinct P.assignedmdm, P.customer_number as BillToCustomerNumber, " +
                        "P.Address_number as ShipToCustomerNumber,P.partner_func as PartnerFunc, A.name as CustomerName " +
                        "from mdm_cm_partner_func P " +
                        "inner join mdm_cm_aggregate A on P.assignedmdm = A.assignedmdm " +
                        "where P.Address_number = @customerNumber and P.source_id = 'PTMN' and P.partner_func <> 'SOLDTO' order by BillToCustomerNumber, ShipToCustomerNumber";
                    using (var cmd = new NpgsqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("customerNumber", customerNumber);
                        var dr = await cmd.ExecuteReaderAsync();
                        redshiftPartnerDataDomain = ToCustomList<RedshiftPartnerDataDomain>(dr);
                    }
                    connection.Close();
                }
                return redshiftPartnerDataDomain;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error reading redshift partner data-" + ex.Message);
                return null;
            }
        }

        public async Task<List<RedshiftAggregateDomain>> ReadAggregateDataFromRedShift(string mdmNumber)
        {
            try
            {
                var redshiftAggregateDomainList = new List<RedshiftAggregateDomain>();
                using (var connection = new NpgsqlConnection(_connectionManager.RedshiftConnectionString))
                {
                    await connection.OpenAsync();

                    string query = "select * from mdm_cm_aggregate where assignedmdm = @mdmNumber";
                    using (var cmd = new NpgsqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("mdmNumber", mdmNumber);
                        var dr = await cmd.ExecuteReaderAsync();
                        redshiftAggregateDomainList = ToCustomList<RedshiftAggregateDomain>(dr);
                    }
                    connection.Close();
                }
                return redshiftAggregateDomainList;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error reading redshift aggregate data-" + ex.Message);
                return null;
            }
        }

        public async Task<List<RedshiftSapPartner>> ReadSapPartners(string source, string customerNumber, string salesOrg, string division, string distributionChannelType)
        {
            var partnerKey = source + "|" + customerNumber.TrimStart(new char[] { '0' }) + "|" + salesOrg + "|" + distributionChannelType + "|" + division + "%";
            var query = @"     
                select distinct 
                a.customer_number as CustomerNumber,
                b.customer_number as PartnerNumber,
                b.assignedmdm as MdmNumber,
                partner_func as PartnerFunctionRole,
                name1 as PartnerCustomerName,
                street as Street,
                street2 as Street2,
                city as City,
                region as Region,
                country as Country,
                postal_code as  Postal,
                isdefaultpartner as IsDefaultPartner,
                role as CustomerRole
                from 
                mdm_cm_partner_func a inner join mdm_customer_master b
                on a.source_id=b.source_id and a.address_number=b.customer_number
                where 
                a.customer_number = @customerNumber and b.sales_org = @salesOrg and b.division = @division and b.distribution_channel = @distributionChannelType 
                and partner_key like(@partnerKey)
                ";

            try
            {
                var redshiftApolloPartners = new List<RedshiftSapPartner>();
                using (var connection = new NpgsqlConnection(_connectionManager.RedshiftConnectionString))
                {
                    await connection.OpenAsync();
                    using (var cmd = new NpgsqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("customerNumber", customerNumber);
                        cmd.Parameters.AddWithValue("salesOrg", salesOrg);
                        cmd.Parameters.AddWithValue("division", division);
                        cmd.Parameters.AddWithValue("distributionChannelType", distributionChannelType);
                        cmd.Parameters.AddWithValue("partnerKey", partnerKey);
                        var dr = await cmd.ExecuteReaderAsync();
                        redshiftApolloPartners = ToCustomList<RedshiftSapPartner>(dr);
                    }
                    connection.Close();
                    connection.Dispose();
                }
                return redshiftApolloPartners;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error reading redshift Partners Data -" + ex.Message);
                return null;
            }
        }

        public async Task<List<RedshiftSapPartner>> ReadJdePartners(string source, string customerNumber)
        {
            var partnerKey = source + "|" + customerNumber.TrimStart(new char[] { '0' }) + "%";
            var query = @"     
                select distinct 
                a.customer_number as CustomerNumber,
                b.customer_number as PartnerNumber,
                b.assignedmdm as MdmNumber,
                partner_func as PartnerFunctionRole,
                name1 as PartnerCustomerName,
                street as Street,
                street2 as Street2,
                city as City,
                region as Region,
                country as Country,
                postal_code as  Postal,
                isdefaultpartner as IsDefaultPartner,
                role as CustomerRole
                from 
                mdm_cm_partner_func a inner join mdm_customer_master b
                on a.source_id=b.source_id and a.address_number=b.customer_number
                where a.customer_number = @customerNumber
                ";

            try
            {
                var redshiftApolloPartners = new List<RedshiftSapPartner>();
                using (var connection = new NpgsqlConnection(_connectionManager.RedshiftConnectionString))
                {
                    await connection.OpenAsync();
                    using (var cmd = new NpgsqlCommand(query, connection))
                    {
                        cmd.Parameters.AddWithValue("customerNumber", customerNumber);
                        var dr = await cmd.ExecuteReaderAsync();
                        redshiftApolloPartners = ToCustomList<RedshiftSapPartner>(dr);
                    }
                    connection.Close();
                    connection.Dispose();
                }
                return redshiftApolloPartners;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error reading redshift Apollo Partners Data -" + ex.Message);
                return null;
            }
        }

        private List<T> ToCustomList<T>(IDataReader dr)
        {
            List<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (!prop.CustomAttributes.Where(x => x.AttributeType == typeof(Newtonsoft.Json.JsonIgnoreAttribute)).Any())
                    {
                        if (!object.Equals(dr[prop.Name], DBNull.Value))
                        {
                            var propType = prop.PropertyType;

                            if (propType.IsGenericType && propType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                            {
                                prop.SetValue(obj, Convert.ChangeType(dr[prop.Name], Nullable.GetUnderlyingType(prop.PropertyType)), null);
                            }
                            else
                            {
                                prop.SetValue(obj, dr[prop.Name], null);
                            }
                        }
                    }
                }
                list.Add(obj);
            }
            return list;
        }

        public async Task<WorkflowBlockCustomerDomain> GetBlockUnblockWorkflow(string workflowId, int systemTypeId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowData = await mdmContext.WorkflowBlockCustomer.Where(x => x.WorkflowId == workflowId && x.SystemTypeId == systemTypeId).FirstOrDefaultAsync();
                return Mapper.Map<WorkflowBlockCustomerDomain>(workflowData);
            }
        }

        public async Task<WorkflowDomain> GetWorkflow(string workflowId)
        {
            using (var mdmContext = GetContext())
            {
                var workflow = await mdmContext.Workflow.Where(x => x.WorkflowId == workflowId).FirstOrDefaultAsync();
                return Mapper.Map<WorkflowDomain>(workflow);
            }
        }

        public async Task<WorkflowTypeDomain> GetWorkflowTypeAsync(int workflowTypeId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowTypes = mdmContext.WorkflowType.AsQueryable();
                return Mapper.Map<WorkflowType, WorkflowTypeDomain>(await workflowTypes.Where(x => x.Id == workflowTypeId).FirstOrDefaultAsync());
            }
        }
    }
}
