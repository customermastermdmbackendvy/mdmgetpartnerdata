﻿namespace Mdm.Infrastructure.Managers
{
    public class ErpApisAuth
    {
        public string SapApolloBasicAuthData { get; set; }
        public string SapOlympusBasicAuthData { get; set; }
    }
}
