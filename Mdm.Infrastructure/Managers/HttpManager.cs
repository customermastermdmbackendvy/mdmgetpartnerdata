﻿
using Amazon.Lambda.Core;
using Flurl;
using Flurl.Http;
using Mdm.Domain.Entities;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Infrastructure.Helpers.Constants;
using Mdm.Services.Models;
using System;
using System.Threading.Tasks;

namespace Mdm.Infrastructure.Managers
{
    public class HttpManager : IHttpManager
    {
        private readonly string _getErpUrl;
        public HttpManager()
        {
            _getErpUrl = Environment.GetEnvironmentVariable(EnvironmentVariables.GetErpUrl);
            // _getErpUrl = "https://462wczp940.execute-api.us-east-2.amazonaws.com/dev";
        }

        public async Task<SapOlympusCustomerDomain> GetSapOlympusCustomer(ErpPartnerDataRequest erpPartnerDataRequest)
        {
            return await GetErpCustomer<SapOlympusCustomerDomain>(erpPartnerDataRequest);
        }

        public async Task<SapApolloCustomerDomain> GetSapApolloCustomer(ErpPartnerDataRequest erpPartnerDataRequest)
        {
            return await GetErpCustomer<SapApolloCustomerDomain>(erpPartnerDataRequest);
        }

        #region Private helper
        private async Task<T> GetErpCustomer<T>(ErpPartnerDataRequest erpPartnerDataRequest) where T : class
        {
            try
            {
                var result = await new Url(_getErpUrl)
                    .WithTimeout(30)
                    .PostJsonAsync(erpPartnerDataRequest).ReceiveJson<OperationResult<T>>();
                return result.ResultData;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error with retrieving data from ERP.  Details: " + ex.Message);
                return null;
            }
        }
        #endregion
    }
}
