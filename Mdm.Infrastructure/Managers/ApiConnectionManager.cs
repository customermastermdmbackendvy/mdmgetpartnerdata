﻿using Amazon.Lambda.Core;
using Mdm.Domain.Interfaces.Managers;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Mdm.Infrastructure.Managers
{
    public class ApiConnectionManager : IApiConnectionManager
    {
        private readonly ISecretsManager _secretsManager;
        public ApiConnectionManager(ISecretsManager secretsManager)
        {
            _secretsManager = secretsManager;
            var erpBasicAuthData = GetBasicAuth().Result;
            SapApolloBasicAuthData = erpBasicAuthData.SapApolloBasicAuthData;
            SapOlympusBasicAuthData = erpBasicAuthData.SapOlympusBasicAuthData;
        }

        public string SapApolloBasicAuthData { get; private set; }
        public string SapOlympusBasicAuthData { get; private set; }
        private async Task<ErpApisAuth> GetBasicAuth()
        {
            var rawErpApisSecret = await _secretsManager.GetErpApisSecret();
            if (string.IsNullOrEmpty(rawErpApisSecret))
            {
                LambdaLogger.Log("Cannot find secret from secret manager.  Check credentials or secrets settings.");
                return null;
            }

            var erpApisAuth = JsonConvert.DeserializeObject<ErpApisAuth>(rawErpApisSecret, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            if (erpApisAuth == null)
            {
                LambdaLogger.Log("Cannot parse secret connection data.");
                return null;
            }

            return erpApisAuth;
        }
    }
}
