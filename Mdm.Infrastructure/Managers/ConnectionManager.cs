﻿using Amazon.Lambda.Core;
using Mdm.Domain.Interfaces.Managers;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Mdm.Infrastructure.Managers
{
    public class ConnectionManager : IConnectionManager
    {
        private readonly ISecretsManager _secretsManager;
        private const string ConnectionStringTemplate = "server={0};port={1};user={2};password={3};database={4}";
        private const string RedshiftConnectionStringTemplate = "Server={0};User Id={1};Password={2};Database={3};Port={4};SearchPath={5};";
        public ConnectionManager(ISecretsManager secretsManager)
        {
            _secretsManager = secretsManager;
            ConnectionString = GetConnectionString().Result;
            RedshiftConnectionString = GetRedshiftConnectionString().Result;
        }

        public string ConnectionString { get; private set; }
        private DataBaseConnection DataBaseConnection { get; set; }
        public string RedshiftConnectionString { get; set; }
        public async Task<string> GetConnectionString()
        {
            var rawConnectionSecret = await _secretsManager.GetRdsSecret();
            if (string.IsNullOrEmpty(rawConnectionSecret))
            {
                LambdaLogger.Log("Cannot find secret from secret manager.  Check credentials or secrets settings.");
                return null;
            }

            DataBaseConnection = JsonConvert.DeserializeObject<DataBaseConnection>(rawConnectionSecret, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            if (DataBaseConnection == null)
            {
                LambdaLogger.Log("Cannot parse secret connection data.");
                return null;
            }

            ConnectionString = string.Format(ConnectionStringTemplate, DataBaseConnection.Host, DataBaseConnection.Port, DataBaseConnection.UserName, DataBaseConnection.Password, DataBaseConnection.DataBase);
            return ConnectionString;
        }
        public async Task<string> GetRedshiftConnectionString()
        {
            var rawConnectionSecret = await _secretsManager.GetRedshiftSecret();
            if (string.IsNullOrEmpty(rawConnectionSecret))
            {
                LambdaLogger.Log("Cannot find secret from secret manager.  Check credentials or secrets settings.");
                return null;
            }

            DataBaseConnection = JsonConvert.DeserializeObject<DataBaseConnection>(rawConnectionSecret, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            if (DataBaseConnection == null)
            {
                LambdaLogger.Log("Cannot parse secret connection data.");
                return null;
            }

            RedshiftConnectionString = string.Format(RedshiftConnectionStringTemplate, DataBaseConnection.Host, DataBaseConnection.UserName, DataBaseConnection.Password, DataBaseConnection.DataBase, DataBaseConnection.Port, DataBaseConnection.Schema);
            return RedshiftConnectionString;
        }
    }
}
