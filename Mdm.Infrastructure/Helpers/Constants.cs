﻿

namespace Mdm.Infrastructure.Helpers.Constants
{
    public static class EnvironmentVariables
    {
        public const string AspnetCoreEnvironment = "ASPNETCORE_ENVIRONMENT";
        public const string Environment = "Environment";
        public const string EnvironmentRegion = "EnvironmentRegion";
        public const string RdsSecretName = "RdsSecretName";
        public const string GetErpUrl = "GetErpUrl";
        public const string RedshiftSecretName = "RedshiftSecretName";
    }

    public static class Environments
    {
        public const string Production = "Production";
    }
}
